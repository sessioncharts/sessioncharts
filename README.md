# SessionCharts

SessionCharts is a songbook app for creating songbooks with lyric sheets, chord charts, melodies, tabs and setlists.

# Features

- Chord charts with metric information (beats, measures, sections, arrangement)
- Your choice of letters, Roman numerals, or Nashville Numbers for chords
- Lyric sheets with chords and bar lines shown over each lyric line
- Melodies for songs, or for tunes without lyrics, using numbered musical notation (jianpu)
- Tablature
- Define song sections (verse, chorus and bridge, etc.) and arrange songs without having to redundantly input information from prior sections
- Automatic transposition into other keys
- Capo directives
- Manage and view setlists
- Simple plain text formats for songs and setlists (No vendor lockin)
- Lyric sheets in Chordpro format can also be used

# Further Information

See the [user manual](https://drive.google.com/file/d/131ijZRZQ7P7I89h8CSFesbCrNkquK-IJ/view?usp=sharing) for further information.

