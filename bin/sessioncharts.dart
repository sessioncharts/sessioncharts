#!/usr/bin/env dart
// Copyright 2019 Thomas F. Gordon

import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:args/args.dart';
import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/constants.dart';
import 'package:sessioncharts/src/parser/sessioncharts_parser.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/src/pdf/song.dart' as spdf;
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart'as pw;

ArgResults? argResults;

const helpText = """ 
Usage: sessioncharts [OPTION]... [FILE]...
-h --help      show this help text
-v --version   prints the version of this program
-n --nashville show chords using the Nashville Number System
-r --roman     show chords using Roman numerals

The SessionCharts command line utility formats SessionChart and
ChordPro files and writes them to PDF files in the current directory.
Existing files are not overwritten. To replace an existing file,
delete or move it first.
""";

void main(List<String> arguments) async {
  exitCode = 0; // presume success
  final ap = ArgParser()
    ..addFlag('help', negatable: false, abbr: 'h')
    ..addFlag('version', negatable: false, abbr: 'v')
    ..addFlag('nashville', negatable: false, abbr: 'n')
    ..addFlag('roman', negatable: false, abbr: 'r')
    ..addFlag('import', negatable: false, abbr: 'i')
    ..addFlag('export', negatable: false, abbr: 'e');

  argResults = ap.parse(arguments);
  if (argResults != null) {
    final paths = argResults!.rest;

    if (argResults!['version'] as bool) {
      print("SessionCharts $version");
    }

    if (argResults!['help'] as bool) {
      print(helpText);
    }

    var style = Style.letters;
    if (argResults!['nashville'] as bool) style = Style.nashville;
    if (argResults!['roman'] as bool) style = Style.roman;

    for (var path in paths) {
      final f = File(path);
      final s = await f.readAsString();
      try {
        Result r = song.end().parse(s);
        if (r.value is Song) {
          final song = r.value;
          final pdf = pw.Document();
          pdf.addPage(pw.Page(
              pageFormat: PdfPageFormat.a4,
              build: (pw.Context context) {
                return spdf.songWidget(pw.ThemeData.base(), song, style: style); 
              })); // Page
          final basename = p.basename(p.withoutExtension(path));
          final file = File("$basename.pdf");
          if (await file.exists()) 
             stderr.writeln("$basename.pdf: file exists and was not overwritten");
          else 
             await file.writeAsBytes(await pdf.save());
        } else {
          print(r.value);
        }
      } catch (e) {
        stderr.writeln("$path: $e");
      }
    }
  }
}
