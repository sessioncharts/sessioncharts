title: All My Trials
subtitle: trad. Spiritual
key: D major
meter: 4/4
copyright: public domain
recording: https://open.spotify.com/track/3lqxFhPsfNl2DSmqEogdH2?si=c3f1f3e7f15c4b20

section: V

chords {
  D | D C | C | 
  D | F#m | G | Em |
}

melody {
    | 5  6_ 6_  7  1' | 2'  3'  4' - |~4'  -  -  4' | 
    | 5'  2'  2'= 2'_. ~2' - | 2'  2'  2'  3' | ~3'_ 2'_ 1'  -  - | ~1'  -  -  - | 
}

lyric {
	[]Hush little baby, []don't you []cry []
	You []know your mama [] was born to []die [] 
}

section: C

chords {
  D | Bm | Em | Em | 
  A | A | D | D
}

melody {
 	7  -  -  - | ~7  -  -  1' | 7  6  6  - |~6  -  -  - | 
 	6  -  -  - | ~6  -  -  7 | 6  5  5  - |~5  -  -  - |
}

lyric  {
	[]All my [] []trials, Lord, []
	[]soon [] be []over []
}


section: V

lyric {
	[]The river of Jordan is []muddy and []cold []
	Well it []chills the body [] but not the []soul []
}

section: C

section: V

lyric {
	[]I've got a little book with []pages []three []
	And []every page [] spells liber[]ty []
}

section: B

chords {
	D | D |
	F#m | F#m | G | G
}

melody {
	0  5  5  2 | 5_ 5_~5-- | 
	0  2  2  - | 0  2  2  2 |3~3_ 2_ 1  - |~1  -  -  - |
}

lyric {
	[] Too late, my []brothers 
	[] Too late, [] but never []mind []
}


section: C

section: V

lyric {
	If []living were a thing that []money could []buy []
	You []know the rich would []live and the poor would []die []
}

section: C

section: V

lyric {
	There []grows a tree in []para[]dise []
	And the []pilgrims call it [] the tree of []life []
}

section: C

section: B

section: C

section: C




