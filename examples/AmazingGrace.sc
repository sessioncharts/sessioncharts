title: Amazing Grace
composer: John Newton 
key: F Major
meter: 3/4
copyright: public domain
recording: https://open.spotify.com/track/7BZXbEXRTM9Jbt8J3YJwfG?si=28ec1338fa1b4427

section: A

chords {
  I | I | IV | I
  I | I | V | V7
  I | I | IV | I
  I | V7 | I | I
}

lyric {
 A[]mazing []grace, how []sweet the []sound,
 That []saved a []wretch like []me. []
 I []once was []lost but []now I'm []found,
 Was []blind, but []now I []see. []
}

section: A

lyric {
  T'was []grace that []taught my []heart to []fear.
  And []grace, my []fears re[]lieved. []
  How []precious []did that []grace ap[]pear
  The []hour I []first be[]lieved. []
}

section: A

section: A

lyric {
  When []we've been []there ten []thousand []years
  bright []shining []as the []sun []
  We've []no less []days, to []sing God's []praise
  than []when we'd []first be[]gun.  []
}

section: A

