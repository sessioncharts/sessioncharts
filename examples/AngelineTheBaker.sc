title: Angeline The Baker
subtitle: Traditional
key: D Major
meter: 4/4
copyright: public domain
recording: https://open.spotify.com/track/0EL6wJmFpN3h1Dnpj2Ds1q?si=d3812917c9244aeb

section: V

chords {
	| D | D | D | G |
	| D | D | D G | D |
}

melody {
	5  6  1'~1'_.5= | 6  1'  -  - | 5  6  1'  5 | 6  -  -  - | 
	5  6  1'~1'_. 2'= | 3'  2'  1'~1'_. 2'= | 3'  2'  1'  6 | 5  -  -  - |
}


lyric {
	[]Angeline the []baker, []lives on the village []green 
	The []way I always []loved her, beats []all you’ve []ever []seen
}

section: C

chords {
	| D | D | D | G |
	| D | D | D G | D |
}

melody {
	5'  3'  2'~2'_. 1'= | 2'  3'  -  - | 5'  3'  2'  1' | 6  -  -  - |
	5'  3'  2'~2'_1'=2'= | 3'  2'  1'~1'_ 1'=2'= | 3'  2'  1'  6 | 5  -  -  - |
}

lyric {
	[]Angeline the []baker, []Angeline I []know
	[]Should have married []Angeline, []twenty []years a[]go
}

section: V

lyric {
	Her []father was a []baker, his []name was Uncle []Sam
	I []never can for[]get her, no []matter []where I []am
}

section: C

section: V

lyric {  
	[]Angeline is []handsome, []Angeline is []tall,
	They []say she sprained her []ankle a-[]dancing []at the []ball. 
}

section: C

section: V

lyric {
	[]She can't do []hard work, be[]cause she is not []stout
	She []bakes her biscuits []every day and []pours the []coffee []out
}

section: C

section: V

lyric {
	[]Angeline the []baker, []age of forty-[]three
	[]Fed her sugar []candy, but she []still won't []marry []me
}

section: C

section: V

lyric {
	[]The last time that I []saw her was []at the county []fair.
	Her []father run me []almost home and []told me []to stay []there. 
} 

section: C


