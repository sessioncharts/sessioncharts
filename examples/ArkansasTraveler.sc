title: Arkansas Traveler
subtitle: Oldtime Traditional
rhythm: Reel
meter: 4/4
key: D Major
copyright: public domain
recording: https://open.spotify.com/track/6w7vluxLQVrDdhZUEwOmSI?si=cdb044191266498f

section: A

chords {
  D G | D 
  A | A
  D G | D 
  D G | A D
}

melody {
  6,_7,_ |: 1_3_2_1_ 6, 6, | 5, 5, 1- 
  2 2 3 3 | 1_3_2_1_ 6, 5, 
  1_3_2_1_ 6, 6, | 5, 5, 1- 
  1'_7_1'_5_ 6_1'_5_4_ [1 3_1_2_3_ 1 6,_7,_ [2 3_1_2_3_ 1 3'_4'_ ||
}

section: B

chords {
  D G | D A 
  D A | D A 
  D G | D A 
  D G | A D
}

melody {
  5'_4'_3'_5'_ 4'_3'_2'_4'_ | 3'_2'_1'_3'_ 2'_1'_7_5_ 
  1'_7_1'_3'_ 2'_1'_2'_4'_ | 3'_2'_1'_3'_ 2' 3'_4'_ 
  5'_4'_3'_5'_ 4'_3'_2'_4'_ | 3'_2'_1'_3'_ 2'_1'_7_5_ 
  1'_7_1'_5_ 6_1'_5_4_ [1 3_1_2_3_ 1 3'_4'_ [2 3_1_2_3_ 1 |]
}

