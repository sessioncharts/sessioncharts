title {As I Roved Out}
artist {Planxty}
key: A dorian
notes: We play this is B dorian, with a capo on the second fret
meter: 4/4
copyright: public domain
recording: https://open.spotify.com/track/64odIIJcNZqdu886JCNr3A?si=ba8452db10954aa4 

section: I
comment: whistle solo

chords {
 | IIm I | IIm I |
 | IIm I | IIm I |
 | IIm VIm | I |
 | IIm I | IIm I IIm- |
}

section: V

chords {
 | IIm I | IIm I |
 | IIm I | IIm I |
 | IIm VIm | I |
 | IIm I | IIm- I IIm |
}

lyric {
And []who are you, me []pretty fair maid, and []who are you, me []honey?
And []who are you, me []pretty fair maid, and []who are you, me []honey?
She []answered me quite []modestly, "I []am me mother's darling."
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
And []will you come to []me mother's house, when the []moon is shining []clearly?
And []will you come to []me mother's house, when the []moon is shining []clearly?
I'll []open the door and I'll []let you in, and []devil 'o one would hear us.
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
So I []went to her house in the []middle of the night, when the []moon was shining []clearly
So I []went to her house in the []middle of the night, when the []moon was shining []clearly
She []opened the door and she []let me in, and []devil the one did hear us.
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
She []took me horse by the []bridle and the bit, and she []led him to the []stable
She []took me horse by the []bridle and the bit, and she []led him to the []stable
Saying "There's []plenty of oats for a []soldier's horse, to []eat it if he's able."
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
And she []took me by the []lily-white hand, and she []led me to the []table 
Oh she []took me by the []lily-white hand, and she []led me to the []table 
Saying "There's []plenty of wine for a []soldier boy, so []drink it if you're able."
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
Then []I got up and []I made the bed, and I []made it nice and []daisy
Then []I got up and []I made the bed, and I []made it nice and []daisy
Then I []got up and I []laid her down, saying "[]Lassie, are you able?"
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
And []there we lay till the []break of the day, and []devil the one did []hear us 
And []there we lay till the []break of the day, and []devil the one did []hear us 
Then []I arose and put []on me clothes, saying "[]Lassie, I must leave you."
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}

section: V

lyric {
And []when will you re[]turn again, and []when will we get []married?
And []when will you re[]turn again, and []when will we get []married?
When []broken shells make []Christmas bells, []we might well get married.
With me []too-ry-ay, []fol-de-diddle-day, []di-re fol-de-diddle []dai-rie []oh.
}







