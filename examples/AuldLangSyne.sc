title: Auld Lang Syne
composer: Robert Burns
key: F# Major
meter: 4/4
capo: 4
copyright: public domain 
recording: https://open.spotify.com/track/0GuKV0aqoY22YGSUuK8yWf?si=13fff8efb11f42b7

section: V

chords {
  I | IIIm | VIm | IV
  I | V | IV V | I
}

lyric {
  Should []old acquaintance []be forgot, and []never brought to []mind?
  Should []old acquaintance []be forgot, for []auld []lang []syne? 
}

section: C

chords {
  I | IIIm | VIm | IV
  I | V | IV V | I
}

lyric {
  For []auld lang []syne, my dear, for []auld lang []syne
  We'll []take a cup of []kindness yet, for []auld []lang []syne
}

section: V

lyric {
  And []surely you will []buy your cup, and []surely I'll buy []mine
  And we'll []take a cup of []kindness yet, for []auld []lang []syne.
}

section: C

section: V

lyric {
  We []two have run a[]bout the hills, and []picked the daisies []fine
  But we've []wandered many a []weary foot, since []auld []lang []syne.
}

section: C

section: V

lyric {
  We []two have paddled []in the stream, from []morning sun till []night
  But the []seas between us []roared and swelled, since []auld []lang []syne.
}

section: C

section: V

lyric {
  And []here's a hand, my []trusty friend, and []give me a hand of []thine.
  And we'll []take a cup of []kindness yet, for []auld []lang []syne.
}

section: C


