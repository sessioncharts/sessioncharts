title: Baby Please Don't Go
subtitle: Traditional Blues
meter: 4/4
key: G Major
copyright: public domain
recording: https://open.spotify.com/track/1vjHFgIdNuLXojpkzysoV8?si=01a49b264cdf4b32

section: A

chords {
  I | I | I | I
  IV7 | IIIb7 | I | V7
}

lyric {
  Baby []please don't go, [] baby []please don't go []
  Baby []please dont' go back to []New Orleans, you know I []love you so [] 
}

section: A

lyric {
  Turn your []lamp down low, [] turn your []lamp down low []
  Turn your []lamp down low, I beg you []all night long, Baby []please don't go []
}

section: A

lyric {
  She got me []way down here, [] She got me []way down here []
  She got me []way down here by the []rolling fog, she treats me []like a dog []
}

section: A

lyric {
  Before I []be your dog, [] before I []be your dog [] 
  Before I []be your dog, I get you []way out here, I'll make you walk the []dog, baby please don't go[]
}

section: A

lyric {
  You know your []man done gone [] you know your []man done gone []
  You know your []man done gone, down the []county line, before the []sheriff come []
}


