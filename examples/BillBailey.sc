title: Bill Bailey 
composer: Hughie Cannon
key: G major
meter: 4/4
tempo: 100 moderato
year: 1902
copyright: public domain
recording: https://open.spotify.com/track/4WSrev8fcCtAUqjYPcERDT
album: Big Bill Broonzy sings Folk Songs; Smithstonian Folkways; 1989

section: V

chords {
	| G | G | G / / C#dim | G |
	| G | G G#dim | D7/A D7 / / | D7 | 
	| D7 | D7 | D7 | D7 | 
	| D7 | D7 D9#5 | G | G |
}

melody {
	| 5, 6, - 1 | 3. #2_ 3 5 | 5, 6, - 1 | 3 - - - | 
        | 5, 6, - 1 | 3 - 5 - | 5 7, - - | 0 0 0 0 | 
        | 5, 7, - 2 | 4 3 4 5 | 5, 7, - 2 | 4 - - - |
	| 5, 7, - 2 | 5 - 6 - | 6 3 - - | 0 0 0 0 |
}

lyric {
	[]Won't you come []home, Bill Bailey, []won't you []come []home?
	[]She moans the []whole []day []long. [] []
	[]I'll do your []cooking darling, []I'll pay the []rent.
	[]I know I've []done []you []wrong. []
}

section: C

chords {
	| G | G | G / / C#dim | G  
 	| G | G Bdim | Am/C E7/B | Am 
	| C | C C#dim | G/D | E7  
	| A7 | D9 D7 [1 G | A7 D7 [2 G | G |]
}

melody {
	| 5, 6, - 1 | 3. #2_ 3 5 | 5, 6, - 1 | 3 - - 5, |
 	| 5, 5, 1 3 | 5 - #6 | 6 - - - | 6 - - 1 | 
	| 1 1 - 1 | 2 - 1 - | 5 5 - 6 | 3 - - 3 |
	| 3 2 #1 2 | 4 - 3 - [1 1 - - - | 1 0 0 0 [2 1 - - - | 1 0 0 0 |]
}

lyric {
	Re[]member that []rainy evening, []I drove []you []out, 
	with []nothing but a []fine []tooth []comb? [] []
	I []know I'm to []blame, []well, []ain't that a []shame? 
	Bill []Bailey won't you []please []come []home? [][] []home? []
}

section: V

section: C


