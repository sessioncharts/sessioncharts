title: Bird in the Bush
subtitle: Irish Trad
key: G Major
meter: 2/2
rhythm: Reel
copyright: Public Domain
recording: https://open.spotify.com/track/7AiZIihl4bqnKuJRvQALRd?si=c63ef79727fe46f5

section: A

chords {
  1 | 5
  2m | 2m 5 
  1 | 5 
  2m | 5 1
}

melody {
  5 6_3_ 5_3_ (3_2_3_) | 5_3_2_3_ 1 2_1_ 
  6,_2_ (2_2_2_) 3_1_ (1_1_1_) | 3_1_3_5_ 1'. 6_
  5 6_3_ 5_3_ (3_2_3_) | 5_3_2_3_ 1 1_6,_ 
  5,_6,_1_2_ 3 3_4_ | 5_3_2_4_ 3_1_ 1 
}

section: B

chords {
  6m | 6m
  5 | 5 
  6m | 6m 
  6m | 5 1
}

melody {
  (3_4_5_) 6_7_ 1' 7_1'_ | 2'_7_5_7_ 1'_7_6_5_ 
  (3_4_5_) 6_7_ 1'_7_1'_3'_ | 2'_1'_2'_3'_ 1' 1'_2'_
  3'_1'_ (1'_7_1'_) 2'_1'_6_7_ | 1' 1'_6_ 5_3_2_3_ 
  5,_6,_1_2_ 3 3_4_ | 5_3_2_4_ 3_1_ 1
}
