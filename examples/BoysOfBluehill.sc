title: Boys of Bluehill
subtitle: Irish Trad
copyright: public domain
meter: 2/2
rhythm: Hornpipe
key: D Major
recording:  https://open.spotify.com/track/6Pb443EpxcvHAlBzDSoCO8?si=01a3844545694785

section: A

chords {
   Bm D | G A 
   D Em | D G 
   Bm D | G A 
   D A7 | D 
}

melody {
  3_5_ || 6_5_ 3_5_ 1 3_5_ | 6_5_ (6_7_1'_) 2' 1'_2'_ 
  3'_5'_ 5'_3'_ 2'_4'_ 3'_2'_ | 1'_3'_ 2'_1'_ 6_ 1'_6_ 
  6_5_ 3_5_ 1 3_5_ | 6_5_ (6_7_1'_) 2' 1'_2'_ 
  3'_5' 5'_3'_ 2'_4'_ 3'_2'_ | 1' 3' 1' 
}

section: B

chords {
  D | Em  G 
  D Em | D G 
  Bm D | G A 
  D A7 | D 
}

melody {
  3'_4' || 5'_3'_ 1'_3'_ 5' 5'_3'_ | 4'_3'_ 4'_5'_ 6' 5'_4'_ 
  3'_5'_ 5'_3'_ 2'_4'_3'_2'_ | 1'_3'_2'_1'_ 6 1'_6_ 
  6_5_3_5_ 1 3_5_ | 6_5_ (6_7_1'_) 2' 1'_2'_ 
  3'_5'_5'_3'_ 2'_4'_3'_2'_ | 1' 3' 1' 
}

