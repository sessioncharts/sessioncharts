title: Careless Love
subtitle: traditional jazz
meter: 4/4
key: C Major
copyright: Public Domain
recording: https://open.spotify.com/track/5FmFpzZQbu7iEYYMiJxEfV?si=9022ea0072d24b5e

section: V

chords {
  C | G7 | C G7 | C 
  C | C | G7 | G7
  C | C7 | F | Fm 
  C | G7 | C | G7
}

lyric {
  [C]Love, oh [G7]love, oh careless [C]love [G7][C]
  You [C]fly through my [C]head like [G7]wine. [G7]
  You [C]wreaked the [C7]life of [F]many a poor [Fm]gal.
  And you [C]let me [G7]spoil this life of [C]mine. [G7]
}

section: V

lyric {
  [C]Love, oh [G7]love, oh careless [C]love [G7][C]
  [C]In your [C]clutches of de[G7]sire. [G7]
  You [C]made me [C7]break a [F]many true [Fm]vow.
  Then you [C]set my very [G7]soul on [C]fire. [G7]
}

section: V

lyric {
  [C]Love, oh [G7]love, oh careless [C]love [G7][C]
  [C]All my [C]happiness has [G7]left. [G7]
  Cause you [C]fill my [C7]heart with those [F]worried old [Fm]blues
  Now I'm [C]walkin and [G7]talkin to my[C]self. [G7]
}

section: V

lyric {
  [C]Love, oh [G7]love, oh careless [C]love [G7][C]
  [C]Trusted you [C]now its too [G7]late. [G7]
  You [C]made me [C7]throw my [F]only friend [Fm]down.
  That's [C]why I [G7]sing this song of [C]hate. [G7]
}

section: V

lyric {
  [C]Love, oh [G7]love, oh careless [C]love [G7][C]
  [C]Night and [C]day I weep and [G7]moan. [G7]
  You [C]brought the [C7]wrong man in[F]to this life of [Fm]mine.
  For my [C]sins till [G7]judgment I'll a[C]tone. [G7]
}


