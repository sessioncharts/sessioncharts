title: Cluck Old Hen
key: A Dorian
meter: 4/4
copyright: public domain
recording: https://open.spotify.com/track/6T3sv3t18ship4wjgtXKFs?si=c3e94f3f0a6d476b

section: A

chords {
  | A G | A D | A G | E A |
}

melody {
 e a'_e_ g a'_g_ | e a'_ e_ d #f | e a'_ e_ g_ #f_ g_ #f_ | e_ d_ c a- |
}

section: B

chords {
  | Am C | Am G | Am | E Am |
}

melody {
  a a c - | a a g,- | a a_ b_ c_ b_ c_ d_ | e_ d_ c a-  |
}


