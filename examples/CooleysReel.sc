title: Cooley's Reel
subtitle: Irish Trad
key: E Dorian
meter: 4/4
rhythm: Reel
recording: https://open.spotify.com/track/1GaikiETSvXrFjqqo9T7JT?si=1b5f1b5114d54508

section: A

chords {
  Em | Em | 
  D | D  
  Em | Em | 
  D | D Em
}

melody {
  3_ | 2_6_6_5_ 6 2_6_ | 6 5_6_ 1'_6_5_4_ 
  (3_2_1_) 5_1_ 6_1_5_4_ | (3_2_1_) 3_5_ 1'_5_3_1_
  2_6_6_5_ 6 2_6_ | 6 5_6_ 1'_2'_3'_4'_ 
  5'_3'_2'_7_ 1'_6_5_3_ | 1_2_3_1_ 2
}

section: B

chords {
  Em | Em 
  D | D 
  Em | Em 
  E | D Em
}

melody {
  4'_3'_ | 2'_6_ (6_7_6_) 2'_3'_4'_3'_ | 2'_6_ (6_7_6_) 4'_2'_1'_6_ 
  5 3_5_ 1_5_3_5_ | (5_6_5_)3_5_ 1'_2'_3'_4'_ 
  2'_6_ (6_7_6_) 2'_3'_4'_3'_ | 2'_6_ (6_7_6_) 1'_2'_3'_4'_ 
  5'_3'_2'_7_ 1'_6_5_3_ | 1_2_3_1_ 2 
}

