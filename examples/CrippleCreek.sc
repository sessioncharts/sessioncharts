title: Cripple Creek
subtitle: Traditional
copyright: Public Domain
meter: 4/4
key: A Major
recording: https://open.spotify.com/track/1tV5yXHqZ9KTLxJ2RD7fmd?si=c89a480dc98843ea

section: A

chords {
  | A | D A | A | E A 
  | A | D A | A | E A |
}

melody {
  | a' a' e_#c_e_#c_ | d_e_#f_d_ e #f_#g_ | a'_#g_a'_#f_ e_#c_e_#c_ | c_#c_e_#c_ a #f_#g_ 
  | a' a' e_#c_e_#c_ | d_e_#f_d_ e e | (b_c_#c_) e_#c_ b_a_#f,_#f,_ | e, (#f,g,#g,) a a |
}

section: B
 
chords {
  |: A | A | A | E A :| 
}

melody {
  |: c_#c_e_#c_ b_#c_a_b_ | c_#c_e_#c_ e, a_b_ | c_#c_e_#c_ b_a_#f,_#f,_ [1 e, (#f_g_#g_) a a [2 e, (#f_g_#g_) a- :|
}
