title: Saint James Infirmary
composer: Jim Primose
subtitle: Traditional Jazz
key: Em
meter: 4/4
copyright: Public Domain
recording: https://open.spotify.com/track/6a3t7n21ZyUDJCdjAZe8db?si=924070e0ec774f99

section: V

chords {
  Em Am | Em 
  Em Am | B7
  Em Am | Em 
  Em B7 | Em
}

lyric {
  I went [Em]down to the [Am]St. James In[Em]firmary
  [Em]I saw my [Am]baby [B7]there. 
  She was [Em]stretched out on a [Am]long white [Em]table,
  so [Em]cold, so [B7]sweet, so [Em]fair.
}

section: C

chords {
  Em Am | Em 
  Em Am | B7
  Em Am | Em 
  Em B7 | Em
}

lyric {
  Let her [Em]go, let her [Am]go, god [Em]bless her
  [Em] wherever [Am]she may [B7]be, 
  she can [Em]look this [Am]wide world [Em]over,
  but she'll [Em]never find a [B7]sweet man like [Em]me.
}
 
section: V

lyric {
  When I [Em]die, please bury [Am]me, in straight-laced [Em]shoes.
  I want a [Em]box-backed coat and a [Am]Stetson [B7]hat.
  Put a [Em]20 dollar [Am]goldpiece on my [Em]watch chain,
  So the [Em]boys will know that I [B7]died standing [Em]pat.
}

section: C

section: V

lyric {
  Get six [Em]gamblers to [Am]carry my [Em]coffin
  Six chorus [Em]girls to [Am]sing my [B7]song
  Put a [Em]jazz band on my [Am]hearse [Em]wagon,
  To raise [Em]hell, as I [B7]roll a[Em]long.
}

section: C


