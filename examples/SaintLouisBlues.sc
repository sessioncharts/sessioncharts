title: Saint Louis Blues
composer:  W.C. Handy
subtitle: Traditional Jazz
key: E Major
meter: 4/4
copyright: Public Domain
recording: https://open.spotify.com/track/50zXyjVdFb3xAr3hnyYYn1?si=55dc4171095a4901

section: A

chords {
  I | IV7 | I | I7
  IV7 | IV7 | I | I
  V7 | IV7 | I | V7
}

lyric {
  []I hate to see [] the evening' sun go []down []
  []I hate to see [] the evening' sun go []down []
  []It makes me think [] I'm on my last go [] 'round []
}

section: A

lyric {
  []Feeling' tomorrow [] like I feel to[]day []
  []Feeling' tomorrow [] like I feel to[]day []
  []I'll pack my grip [] and make my geta[]way []
}

section: B

chords {
  Im | Im | V7 | V7
  V7 | V7 | Im | Im
  Im | Im | V7 | V7
  V7 | V7 | Im7 II7 | V7
}

lyric{
  Saint Louis []woman [] with her diamond []rings []
  Pulls that []man around [] by her apron []strings []
  Wasn't for []powder [] and this store-bought []hair []
  The man I []love [] wouldn't go no[]where, []no[]where
}

section: A

lyric {
  I got them []Saint Louis Blues; just as []blue as I can []be []
  That []man got a heart like a []rock cast in the []sea []
  Or []else he wouldn't have gone [] so far from []me []
}


