title: When The Saints Go Marching In
subtitle: Traditional Jazz
key: G Major
meter: 4/4
copyright: Public Domain
recording:  https://open.spotify.com/track/15ldGLkV2U5hm37QAJURMP?si=77b19004fd3546c4

section: A

chords {
  I | I | I | I
  I | I | V7 | V7
  I | I7 | IV | IV#dim
  IIIm7 VI7 | IIm7 V7 | I | V7
}

lyric {
  Oh when the []saints [] go marching []in []
  Oh when the []saints go []marching []in []
  Yes I []want to []be in that []number []
  Oh when the []saints []go []march[]ing []in []
}


