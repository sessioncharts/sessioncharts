title: Wise Maid
subtitle: Irish Trad
key: D Major
meter: 2/2
rhythm: Reel
copyright: Public Domain
recording: https://open.spotify.com/track/1tACl4y9mtb3wbwiinR3ic?si=cdfbf0ab0ab344d4

section: A

chords {
  I | I  
  IV I | IIm V
  I | I 
  IV | V I
}

melody {
  3 3_4_ 3_2_1_2 | 3_5_5_6_ 5_3_2_1_ 
  1' (2'_3'_4'_) 3'_1'_2'_7_ | 1'_6_5_3_ 6_2_(2_3_2_) 
  3 3_4_ 3_2_1_2 | 3_5_5_6_ 5_3_2_1_ 
  1_ (2'_3'_4'_) 3'_1'_2'_7_ | 1'_6_5_4_ 3_1_ (1_2_1_) 
}

section: B

chords {
  I | I 
  I V | IV V |  
  IV I | I  
  IV IIm | V I
}

melody {
 1' 5_4_ 3_1_3_5_ | 1'_3'_5'_3'_ 4'_3'_2'_4'_ 
 3'_5_1'_3'_ 2'_5_7_2'_ | 1'_3'_2'_1'_ 7_5_ (5_6_5_) 
 6_2_4_6_ 5_1_3_5_ | 1'_3'_5'_3'_ 4'_3'_2'_1'_ 
 (6_7_1'_) 2'_7_ 1'_6_5_4_ | 3_4_2_4_ 3_1_ 1
}


