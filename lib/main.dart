// Copyright (c) 2020 Thomas F. Gordon

// Before launching an Android emulator, execute:
// sudo chmod a+rw /dev/kvm

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/home_view.dart';
import 'package:flutter/rendering.dart';
import 'package:sessioncharts/src/gui/theme.dart';
import 'package:sessioncharts/src/gui/settings.dart';
import 'package:sessioncharts/src/gui/licenses.dart';
import 'package:sessioncharts/src/storage/database.dart';
import 'package:sessioncharts/src/storage/examples.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

var themeManager = ThemeManager();

void main() async {
  licenses();
  debugPaintSizeEnabled = false;
  WidgetsFlutterBinding.ensureInitialized();
  if (kIsWeb)
     await Hive.initFlutter(); 
  else {
     final appDocDir = await getApplicationDocumentsDirectory();
     await Hive.initFlutter(appDocDir.path);
  }

  await Settings.init();

  if (settings != null && settings!.getInt('lastSongIndex') == null)
    settings!.setInt('lastSongIndex', 0);
  if (settings != null && settings!.getBool('isInitialized') != true) {
    // load the examples the first time starting the app.
    await loadExamples(); // loadExamples makes the Song Index as well
  } else {
    await makeSongIndex();
  }
  runApp(MyApp());
  await Hive.close();
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  _MyAppState();

  @override
  void initState() {
    super.initState();
    themeManager.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "SessionCharts",
      theme: themeManager.theme,
      home: HomeView(),
    );
  }
}
