// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

const version = "v1.2.1";
// const helpUrl = "https://docs.google.com/document/u/1/d/e/2PACX-1vR-8Tf6VElmbPxaYoswOXNwrDhu8L0k9RD0lW4_I5pVycDxdFZyy9E6jLcwar6aSnoYGVcIUSW_RaeA/pub";
const helpUrl =
   "https://drive.google.com/file/d/131ijZRZQ7P7I89h8CSFesbCrNkquK-IJ/view?usp=drive_link";

Set<String> chordProFilenameExtensions = {
  "cho",
  "crd",
  "chopro",
  "chord",
  "pro",
};

Set<String> songFilenameExtensions = {
  "sc",
};

Set<String> setlistFilenameExtensions = {
  "json",
  "yaml",
  "yml",
};
