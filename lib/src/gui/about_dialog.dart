// Copyright (c) 2020-2021 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/constants.dart';

String _txt = """
Version $version
copyright (c) 2019-2022 Thomas F. Gordon

Welcome to SessionCharts!

SessionCharts is a songbook app for viewing lyric sheets, 
chord charts, melodies, tabs and setlists.

Some setlists and public domain songs are included with the app
to give you a better idea of its features and to help you to get started.
To add your own songs and setlists, you will need to enter them with
a text editor using the simple formats described in the user manual and then 
import them.  The user manual can be viewed from within the app using the 
"Help" command. It is also available online at https://www.sessioncharts.com.

You may want to store the text files for your songs and setlists in the cloud, 
so that you can easily import the latest versions onto your various devices or 
to share them with others.

If you want to delete the examples before importing your own songs and 
setlists, use the "Reset" command. The examples can be reloaded at any 
time using the "Examples" command.

web: https://gitlab.com/sessioncharts/sessioncharts 
""";

void aboutDialog(BuildContext context) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text("About SessionCharts"),
        content: SingleChildScrollView(child: Text(_txt)),
        actions: <Widget>[
          TextButton(
            child: Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
