// Copyright (c) 2020-21 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';
import 'package:flutter/material.dart';
import 'tone_pair.dart';
import 'smufl.dart';
import 'theme.dart';
import 'drawings.dart';
import 'settings.dart';

TextSpan chordKindSpan(BuildContext context, ChordKind kind) {
  final bt2 = Theme.of(context).textTheme.bodyMedium;
  // final ms = musicTextStyle(context);
  switch (kind) {
    case ChordKind.maj:
      return TextSpan(style: bt2, text: "");
    case ChordKind.maj6:
      return TextSpan(style: bt2, text: "6");
    case ChordKind.maj7:
      return TextSpan(style: bt2, text: "\u{0394}"); // "maj7");
    case ChordKind.maj9:
      return TextSpan(style: bt2, text: "\u{0394}9");
    case ChordKind.dom7:
      return TextSpan(style: bt2, text: "7");
    case ChordKind.add9:
      return TextSpan(style: bt2, text: "9");
    case ChordKind.sevenSharp9:
      return TextSpan(style: bt2, text: "7\u{266F}9");
    case ChordKind.nineSharp5:
      return TextSpan(style: bt2, text: "9\u{266F}5");
    case ChordKind.min:
      return TextSpan(style: bt2, text: "m");
    case ChordKind.min6:
      return TextSpan(style: bt2, text: "m6");
    case ChordKind.min7:
      return TextSpan(style: bt2, text: "m7");
    case ChordKind.min9:
      return TextSpan(style: bt2, text: "m9");
    case ChordKind.dim:
      return TextSpan(style: bt2, text: "o"); // dim
    case ChordKind.halfDim:
      return TextSpan(style: bt2, text: "\u{00F8}"); // m7\u{266D}5");
    case ChordKind.aug:
      return TextSpan(style: bt2, text: "+"); // "aug"
    case ChordKind.sus:
      return TextSpan(style: bt2, text: "sus");
    case ChordKind.sus2:
      return TextSpan(style: bt2, text: "sus2");
    case ChordKind.sus9:
      return TextSpan(style: bt2, text: "sus9");
    case ChordKind.sixNine:
      return TextSpan(style: bt2, text: "6/9");
    case ChordKind.eleventh:
      return TextSpan(style: bt2, text: "11");
    case ChordKind.thirteenth:
      return TextSpan(style: bt2, text: "13");
    default:
      return TextSpan(style: bt2, text: ""); // major
  }
}

String articulationToMusicChar(Articulation art) {
  switch (art) {
    case Articulation.ring:
      return diamond; // articTenutoAbove;
    case Articulation.push:
      return ">"; // articAccentAbove;
    case Articulation.choke:
      return "∧"; //  articMarcatoStaccatoAbove;
    default:
      return "";
  }
}

Widget chordEventToWidget(
    BuildContext context, ChordEvent chordEvent, MusicKey key, Style style,
    {bool showDuration = false}) {
  // final ms = musicTextStyle(context);
  final bt2 = Theme.of(context).textTheme.bodyMedium;
  final chord = chordEvent.chord;
  final rootPair =
      toneToTonePair(chord?.tone ?? PitchTone(PitchClass.n), key, style);
  final fontWidth = Theme.of(context).textTheme.bodyMedium?.fontSize;

  var children = <InlineSpan>[]; // accidental, kind and bass

  // Accidental
  if (rootPair.accidental != "") {
    children.add(TextSpan(style: bt2, text: "${rootPair.accidental}"));
  }

  // Kind

  children.add(chordKindSpan(context, chord?.kind ?? ChordKind.na));

  // Bass
  if (chord != null && chord.bass != null) {
    final bassPair = toneToTonePair(chord.bass, key, style);
    children.add(TextSpan(style: bt2, text: "/", children: [
      TextSpan(style: bt2, text: "${bassPair.root}"),
      TextSpan(style: bt2, text: "${bassPair.accidental}")
    ]));
  }

  // add a dot to the right if the rhythm is a dotted note
  if (showDuration == true && showDot(chordEvent.duration)) {
    children.add(TextSpan(style: bt2, text: "\u{2022}"));
  }

  final chordWidget = RichText(
      text: TextSpan(
          style: bt2?.copyWith(fontWeight: FontWeight.bold),
          text: "${rootPair.root}",
          children: children),
      softWrap: false);

  var column = <Widget>[];

  // If the articulation is not normal, align the chord and articulation
  // in a the column.
  if (chord != null && chord.articulation != Articulation.normal) {
    column.add(SizedBox(
        height: fontWidth! / 2,
        child: RichText(
            text: TextSpan(children: [
          // TextSpan(text: " ", style: bt2),
          WidgetSpan(
            child: Transform.translate(
              offset: Offset(0, -fontWidth / 4.5),
              child: Text(
                articulationToMusicChar(chord.articulation),
                style: bt2,
                textScaleFactor: 0.7,
              ),
            ),
          )
        ]))));
  }

  column.add(chordWidget);

  if (showDuration)
    column.add(horizontalBarWidget(context, chordEvent.duration));

  // add white space below the chord to line the chord up with the barlines
  column.add(SizedBox(height: fontWidth! / 10));

  return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: column),
        // To Do: add dots for dotted durations
        // add white space after the chord
        SizedBox(width: fontWidth / 2) // extra right space
      ]);
}

// Format a measure of chords in a chord chart.
// The rhythm is left implicit, if the durations of *all* chords
// the measure are implicit.  Slashes are used if the duration of
// a chord is an even number of beats.  Traditional rhythmic notation
// is used only if subdivisions of a beat are needed.
List<Widget> chordMeasureToWidgets(BuildContext context, Measure measure,
    MusicKey key, Meter meter, Style style) {
  bool allDurationsImplict() {
    bool implicit(NoteLength nl) {
      return nl == NoteLength.implicit;
    }

    for (var e in measure.events) {
      if (e is ChordEvent && !implicit(e.duration)) {
        return false;
      }
    }
    return true;
  }

  List<Widget> l = [];

  if (allDurationsImplict()) {
    for (var e in measure.events) {
      if (e is ChordEvent) {
        l.add(chordEventToWidget(context, e, key, style));
      }
    }
    return l;
  }

  // Otherwise denote rhythm using slashes, if possible,
  // or traditional notation, if not.

  final fontSize = Theme.of(context).textTheme.bodyMedium?.fontSize;
  final slash = Container(
      // raise the slash a bit to align its baseline with the text
      padding: EdgeInsets.only(bottom: fontSize! / 10),
      // Add an en-dash character, instead of a slash, to use
      // the same rhythmic notation as in melodies.
      // This also avoids confusion with the slash used for inversions
      child: Text("\u{2013}".padRight(3)));

  // Return the chord followed by n slashes
  List<Widget> slashes(ChordEvent e, int n) {
    List<Widget> l = [chordEventToWidget(context, e, key, style)];
    for (var i = 0; i < n; i++) {
      l.add(slash);
    }
    return l;
  }

  for (var ce in measure.events) {
    if (ce is ChordEvent) {
      switch (ce.duration) {
        case NoteLength.whole:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 3));
          else // meter.unit == 8
            l.addAll(slashes(ce, 7));
          break;
        case NoteLength.half:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 1));
          else // meter.unit == 8
            l.addAll(slashes(ce, 3));
          break;
        case NoteLength.dottedHalf:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 2));
          else // meter.unit == 8
            l.addAll(slashes(ce, 5));
          break;
        case NoteLength.quarter:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 0));
          else // meter.unit == 8
            l.addAll(slashes(ce, 1));
          break;
        case NoteLength.dottedQuarter:
          if (meter.unit == 4)
            l.add(chordEventToWidget(context, ce, key, style,
                showDuration: true));
          else // meter.unit == 8
            l.addAll(slashes(ce, 2));
          break;
        case NoteLength.eighth:
          if (meter.unit == 4)
            l.add(chordEventToWidget(context, ce, key, style,
                showDuration: true));
          else // meter.unit === 8
            l.addAll(slashes(ce, 0));
          break;
        case NoteLength.implicit:
          l.addAll(slashes(ce, 0));
          break;
        case NoteLength.oneBeat:
          l.addAll(slashes(ce, 0));
          break;
        case NoteLength.twoBeats:
          l.addAll(slashes(ce, 1));
          break;
        case NoteLength.threeBeats:
          l.addAll(slashes(ce, 2));
          break;
        case NoteLength.fourBeats:
          l.addAll(slashes(ce, 3));
          break;
        case NoteLength.fiveBeats:
          l.addAll(slashes(ce, 4));
          break;
        case NoteLength.sixBeats:
          l.addAll(slashes(ce, 5));
          break;
        case NoteLength.sevenBeats:
          l.addAll(slashes(ce, 6));
          break;
        case NoteLength.eightBeats:
          l.addAll(slashes(ce, 7));
          break;
        case NoteLength.nineBeats:
          l.addAll(slashes(ce, 8));
          break;
        case NoteLength.tenBeats:
          l.addAll(slashes(ce, 9));
          break;
        case NoteLength.elevenBeats:
          l.addAll(slashes(ce, 10));
          break;
        case NoteLength.twelveBeats:
          l.addAll(slashes(ce, 11));
          break;
        default: // in case the duration is null
          l.add(
              chordEventToWidget(context, ce, key, style, showDuration: true));
      }
    }
  }
  return l;
}

Widget barlineToWidget(BuildContext context, BarLine bl) {
  final bt2 = Theme.of(context).textTheme.bodyMedium;
  final ms = musicTextStyle(context);

  Widget f(String symbol, [String label = ""]) {
    if (label.isEmpty)
      return Text("$symbol".padRight(5), style: ms);
    else
      return RichText(
          text: TextSpan(text: symbol, style: ms, children: [
        TextSpan(
            text: " " + label + " ",
            style: bt2?.copyWith(fontSize: bt2.fontSize! * 0.65)),
        TextSpan(text: "".padRight(5))
      ]));
  }

  switch (bl.kind) {
    case BarLineSymbol.standard:
      return f(barlineSingle);
    case BarLineSymbol.double:
      return f(barlineDouble);
    case BarLineSymbol.beginRepeat:
      return f(repeatLeft);
    case BarLineSymbol.endRepeat:
      return f(repeatRight);
    case BarLineSymbol.endBeginRepeat:
      return f(repeatRightLeft);
    case BarLineSymbol.variantRepeat:
      return f(barlineSingle, bl.label);
    case BarLineSymbol.end:
      return f(barlineFinal);
    default:
      return f(barlineSingle);
  }
}

// chordChart:  Each row represents a line in the chord chart,
// with a item for each barline and each measure in the line.
// Barlines and measures in each line are lined-up.
Widget chordChart(BuildContext context, List<MetricElement>? elements,
    MusicKey key, Meter meter, Style style) {
  // final fontSize = Theme.of(context).textTheme.bodyMedium.fontSize;
  List<Widget> rows = [];
  List<Widget> row = [];
  var compact = settings?.getBool('compact') ?? false;

  if (elements != null)
    for (var e in elements) {
      if (e is BarLine) {
        row.add(barlineToWidget(context, e));
      } else if (e is Measure) {
        for (var e2 in chordMeasureToWidgets(context, e, key, meter, style)) {
          row.add(e2);
        }
      } else if (e is NewLine) {
        if (!compact) {
          // Start a new row, unless the presentation is to be compact.
          // Add extra height to increase space between rows;
          rows.add(
              Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: row));
          row = []; // reset for next line
        } else {
          row.add(Text(" $downLeftArrow "));
        }
      }
    }
  if (compact) {
    // If the presentation is compact, all the chords and barlines are in one row
    rows.add(Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: row));
  }

  return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: rows);
}
