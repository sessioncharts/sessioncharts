// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// Widgets and auxilliary functions for displaying durations

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';

// show a dot only if the duration is a dotted
// note, except for dotted half notes, which
// are displayed using slashes
bool showDot(NoteLength nl) {
  switch (nl) {
    case NoteLength.dottedQuarter:
      return true;
    case NoteLength.dottedEighth:
      return true;
    case NoteLength.dottedSixteenth:
      return true;
    case NoteLength.dottedThirtySecond:
      return true;
    case NoteLength.dottedSixtyFourth:
      return true;
    default:
      return false;
  }
}

// show durations using horizontal bars
// as in numbered music notation,
// if the duration is less than a quarter note
bool showHorizontalBars(NoteLength nl) {
  switch (nl) {
    case NoteLength.eighth:
      return true;
    case NoteLength.dottedEighth:
      return true;
    case NoteLength.sixteenth:
      return true;
    case NoteLength.dottedSixteenth:
      return true;
    case NoteLength.thirtySecond:
      return true;
    case NoteLength.dottedThirtySecond:
      return true;
    case NoteLength.sixtyFourth:
      return true;
    case NoteLength.dottedSixtyFourth:
      return true;
    default:
      return false;
  }
}

// CustomPainters for drawing needed figures

class OneHorizontalBar extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 15; // stroke width
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    Offset start = Offset(0, 0); // top-left corner
    Offset end = Offset(size.width, 0); // top-right corner

    canvas.drawLine(start, end, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class OneCircle extends CustomPainter {
  bool drawUp = false;
  OneCircle(this.drawUp);

  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 10; // stroke width
    final radius = sw;
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final x = size.width / 2.0;
    final y = drawUp ? size.height : 0.0;
    Offset center = Offset(x, y);
    canvas.drawCircle(center, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class TwoHorizontalBars extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 15; // stroke width
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final gap = sw;
    final delta = sw + gap;

    Offset start1 = Offset(0, 0);
    Offset end1 = Offset(size.width, 0);

    Offset start2 = Offset(0, delta);
    Offset end2 = Offset(size.width, delta);

    canvas.drawLine(start1, end1, paint);
    canvas.drawLine(start2, end2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class TwoCircles extends CustomPainter {
  bool drawUp = false;
  TwoCircles(this.drawUp);

  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 10; // stroke width
    final gap = sw + 2;
    final delta = sw + gap;
    final radius = sw;
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final x = size.width / 2.0;
    final y1 = drawUp ? size.height : 0.0;
    final y2 = drawUp ? size.height - delta : delta;
    Offset center1 = Offset(x, y1);
    Offset center2 = Offset(x, y2);
    canvas.drawCircle(center1, radius, paint);
    canvas.drawCircle(center2, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class ThreeHorizontalBars extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 15; // stroke width
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final gap = sw;
    final delta = sw + gap;

    Offset start1 = Offset(0, 0);
    Offset end1 = Offset(size.width, 0);

    Offset start2 = Offset(0, delta);
    Offset end2 = Offset(size.width, delta);

    Offset start3 = Offset(0, 2 * delta);
    Offset end3 = Offset(size.width, 2 * delta);

    canvas.drawLine(start1, end1, paint);
    canvas.drawLine(start2, end2, paint);
    canvas.drawLine(start3, end3, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class ThreeCircles extends CustomPainter {
  bool drawUp = false;
  ThreeCircles(this.drawUp);

  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 10; // stroke width
    final gap = sw + 2;
    final delta = sw + gap;
    final radius = sw;

    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final x = size.width / 2.0;
    final y1 = drawUp ? size.height : 0.0;
    final y2 = drawUp ? size.height - delta : delta;
    final y3 = drawUp ? size.height - (2 * delta) : (2 * delta);
    Offset center1 = Offset(x, y1);
    Offset center2 = Offset(x, y2);
    Offset center3 = Offset(x, y3);

    canvas.drawCircle(center1, radius, paint);
    canvas.drawCircle(center2, radius, paint);
    canvas.drawCircle(center3, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class FourHorizontalBars extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final sw = size.width / 15; // stroke width
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = sw;

    final gap = sw;
    final delta = sw + gap;

    Offset start1 = Offset(0, 0);
    Offset end1 = Offset(size.width, 0);

    Offset start2 = Offset(0, delta);
    Offset end2 = Offset(size.width, delta);

    Offset start3 = Offset(0, 2 * delta);
    Offset end3 = Offset(size.width, 2 * delta);

    Offset start4 = Offset(0, 3 * delta);
    Offset end4 = Offset(size.width, 3 * delta);

    canvas.drawLine(start1, end1, paint);
    canvas.drawLine(start2, end2, paint);
    canvas.drawLine(start3, end3, paint);
    canvas.drawLine(start4, end4, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

// returns an empty SizedBox if the note length is a quarter note or longer
Widget horizontalBarWidget(BuildContext context, NoteLength nl) {
  final style = Theme.of(context).textTheme.bodyMedium;
  final w = (style?.fontSize ?? 12.0) * 0.8;
  final h = w / 7.0; // height per bar

  Widget f(int bars) {
    if (bars > 0 && bars < 5) {
      CustomPainter painter = OneHorizontalBar();
      switch (bars) {
        case 1:
          painter = OneHorizontalBar();
          break;
        case 2:
          painter = TwoHorizontalBars();
          break;
        case 3:
          painter = ThreeHorizontalBars();
          break;
        case 4:
          painter = FourHorizontalBars();
          break;
      }
      return CustomPaint(size: Size(w, h * bars), painter: painter);
    } else {
      // return an empty box
      return SizedBox(width: w, height: 0);
    }
  }

  switch (nl) {
    case NoteLength.eighth:
      return f(1);
    case NoteLength.dottedEighth:
      return f(1);
    case NoteLength.sixteenth:
      return f(2);
    case NoteLength.dottedSixteenth:
      return f(2);
    case NoteLength.thirtySecond:
      return f(3);
    case NoteLength.dottedThirtySecond:
      return f(3);
    case NoteLength.sixtyFourth:
      return f(4);
    case NoteLength.dottedSixtyFourth:
      return f(4);
    default:
      return SizedBox(height: h); // an empty box of the same size
  }
}

// returns an empty SizedBox if the octave is in the middle
Widget octaveWidget(BuildContext context, Octave octave) {
  final style = Theme.of(context).textTheme.bodyMedium;
  final w = (style?.fontSize ?? 12.0) * 0.8;
  final h = w / 5;

  Widget f(int octaves, bool up) {
    if (octaves > 0 && octaves < 4) {
      CustomPainter painter = OneCircle(up);
      switch (octaves) {
        case 1:
          painter = OneCircle(up);
          break;
        case 2:
          painter = TwoCircles(up);
          break;
        case 3:
          painter = ThreeCircles(up);
          break;
      }
      return CustomPaint(size: Size(w, h * 3), painter: painter);
    } else {
      // return an empty box of the same size
      return SizedBox(width: w, height: h * 3);
    }
  }

  switch (octave) {
    case Octave.upOne:
      return f(1, true);
    case Octave.downOne:
      return f(1, false);
    case Octave.upTwo:
      return f(2, true);
    case Octave.downTwo:
      return f(2, false);
    case Octave.upThree:
      return f(3, true);
    case Octave.downThree:
      return f(3, false);
    default:
      return emptyOctaveWidget(context);
  }
}

Widget emptyOctaveWidget(BuildContext context) {
  final style = Theme.of(context).textTheme.bodyMedium;
  final w = (style?.fontSize ?? 12.0) * 0.8;
  final h = w / 5;

  return SizedBox(width: w, height: h * 3);
}
