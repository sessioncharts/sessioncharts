// Copyright (c) 2020-2022 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/song_view.dart';
import 'package:sessioncharts/src/parser/sessioncharts_parser.dart' as lp;
import 'package:sessioncharts/src/parser/chordpro_parser.dart' as cp;
import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/src/gui/key_change.dart';
import 'package:sessioncharts/src/gui/song_chooser.dart';
import 'package:sessioncharts/src/gui/setlist_chooser.dart';
import 'package:sessioncharts/src/gui/song_info.dart';
import 'package:sessioncharts/src/gui/about_dialog.dart';
// import 'package:sessioncharts/src/gui/help_viewer.dart';
import 'package:sessioncharts/src/gui/settings.dart';
import 'package:sessioncharts/src/gui/url.dart';
import 'package:sessioncharts/src/constants.dart';
import 'package:sessioncharts/src/storage/database.dart';
import 'package:file_picker/file_picker.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';
import 'package:sessioncharts/src/gui/reset_dialog.dart';
import 'package:sessioncharts/src/gui/load_examples_dialog.dart';
import 'dart:io';
import 'package:sessioncharts/src/pdf/download.dart';

const userManualURL =
    "https://docs.google.com/document/u/1/d/e/2PACX-1vR-8Tf6VElmbPxaYoswOXNwrDhu8L0k9RD0lW4_I5pVycDxdFZyy9E6jLcwar6aSnoYGVcIUSW_RaeA/pub";

class HomeView extends StatefulWidget {
  HomeView({Key? key}) : super(key: key);

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  Song song = Song.empty();
  var _style = Style.nashville;
  var _showLyrics = true;
  var _showChords = true;
  var _showMelody = true;
  var _showTab = true;
  var _compact = false;
  var _originalKey = MusicKey(KeyClass.c, Mode.major);
  final allSongs = "All Songs";
  var _title = "";
  bool _hideBars = false; // hide the app and navigation bars if true
  List<Widget>? _actions;

  HomeViewState() {
    this._actions = [
      IconButton(
        icon: Icon(Icons.navigate_before),
        onPressed: () {
          if (currentSong > 0) {
            loadSong(currentSong - 1);
          }
        },
      ),
      IconButton(
        icon: Icon(Icons.navigate_next),
        onPressed: () {
          if (currentSong < songIndex.length - 1) {
            loadSong(currentSong + 1);
          }
        },
      ),
      IconButton(
        icon: Icon(Icons.info_outline),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute<Null>(
            builder: (BuildContext context) {
              return SongInfo(this);
            },
            fullscreenDialog: true,
          ));
        },
      ),
      keyMenu(),
    ];
    selectSetlist(settings?.getInt('lastSetlistIndex') ?? 0,
        song: settings?.getInt('lastSongIndex') ?? 0);
  }

  @override
  void initState() {
    super.initState();

    // show the About Dialog with its welome message and
    // instructions the first time the app is launched.
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (settings != null && settings!.getBool('isInitialized') != true) {
        settings!.setBool('isInitialized', true);
        aboutDialog(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onDoubleTap: () {
          setState(() {
            _hideBars = !_hideBars;
          });
        },
        child: Scaffold(
            appBar: _hideBars
                ? null
                : AppBar(
                    leading: mainMenu(),
                    centerTitle: true,
                    title: Text(_title),
                    // toolbarHeight: 40,
                    primary: true,
                    actions: _actions,
                  ),
            body: SongView(
                song: song,
                style: _style,
                compact: _compact,
                chords: _showChords,
                lyrics: _showLyrics,
                melody: _showMelody,
                tab: _showTab),
            bottomNavigationBar: _hideBars
                ? null
                : BottomAppBar(
                    color: Colors.white,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        // Container(
                        //     padding: EdgeInsets.only(left: 20),
                        //     child: FlatButton(
                        //       child: Text('Compact'),
                        //       textColor: _compact ? Colors.green : Colors.red,
                        //       onPressed: () {
                        //         setState(() => _compact = !_compact);
                        //       },
                        //     )),
                        TextButton(
                          child: Text('Chords'),
                          style: _showChords
                              ? TextButton.styleFrom(
                                  foregroundColor: Colors.green)
                              : TextButton.styleFrom(
                                  foregroundColor: Colors.red),
                          onPressed: () {
                            setState(() => _showChords = !_showChords);
                          },
                        ),
                        Container(
                            padding: EdgeInsets.only(right: 20),
                            child: TextButton(
                              child: Text('Lyrics'),
                              style: _showLyrics
                                  ? TextButton.styleFrom(
                                      foregroundColor: Colors.green)
                                  : TextButton.styleFrom(
                                      foregroundColor: Colors.red),
                              onPressed: () {
                                setState(() => _showLyrics = !_showLyrics);
                              },
                            )),
                        TextButton(
                          child: Text('Melody'),
                          style: _showMelody
                              ? TextButton.styleFrom(
                                  foregroundColor: Colors.green)
                              : TextButton.styleFrom(
                                  foregroundColor: Colors.red),
                          onPressed: () {
                            setState(() => _showMelody = !_showMelody);
                          },
                        ),
                        TextButton(
                          child: Text('Tab'),
                          style: _showTab
                              ? TextButton.styleFrom(
                                  foregroundColor: Colors.green)
                              : TextButton.styleFrom(
                                  foregroundColor: Colors.red),
                          onPressed: () {
                            setState(() => _showTab = !_showTab);
                          },
                        ),
                      ],
                    ),
                  )));
  }

  void _errorDialog(String title, String msg) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new TextButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // loadSongWithFilename: load the song with the given filename in the database
  void loadSongWithFilename(String filename, [int? index]) async {
    var s = await getSongFile(filename);

    if (s == "") {
      // print("debug; song filename is empty");
      return; // error, either an empty song or no song with this id in the database
    }
    Result? r; // parser result
    try {
      if (isSongFile(filename)) {
        r = lp.song.end().parse(s);
      }
      if (isChordProFile(filename)) {
        r = cp.song.end().parse(s);
      }
    } catch (e) {
      _errorDialog("Syntax Error", "$filename:  $e");
    }
    if (r != null && r.isSuccess) {
      setState(() {
        this.song = r!.value;
        this._originalKey = this.song.header.key;
        var t = "";
        if (currentSetlist == 0) {
          t = allSongs;
        } else {
          t = setlists[currentSetlist].title;
        }
        if (index != null) {
          final position = "(${index + 1}/${songIndex.length})";
          this._title = "$t $position";
          currentSong = index;
          // change the key to the one in song index, in case
          // it was changed in a setlist.  The original key
          // remains the one given in the song file.
          this.song.header.key = songIndex[index].key;
        } else
          this._title =
              ""; // show no title in the header bar if the file was not selected from a setlist
      });
    } else {
      // print("debug: r is null or the parser failed");
      if (r != null)
        _errorDialog("Syntax Error",
            "In file \"$filename\" at about ${r.toPositionString()} (line:character)");
    }
  }

  // loadSong: load the song with the given index in the songIndex
  Future<void> loadSong(int index) async {
    if (songIndex.length == 0) {
      // Song database is empty, so set the current song to null
      // and reset the _title
      setState(() {
        this._title = "$allSongs (0/0)";
        this.song = Song.empty();
      });
      return;
    }
    if (index < 0 || index > songIndex.length - 1) {
      // index out of range; do nothing
      return;
    }
    // print("debug: songIndex[index].file = ${songIndex[index].file}");
    loadSongWithFilename(songIndex[index].file, index = index);
    if (settings != null) settings!.setInt('lastSongIndex', index);
  }

  void selectSetlist(int setlistIndex, {int song = 0}) async {
    if (setlistIndex == 0) {
      currentSetlist = 0; // all songs
      await makeSongIndex();
    } else {
      currentSetlist = setlistIndex;
      if (setlistIndex < setlists.length)
        await makeSongIndex(setlists[setlistIndex]);
    }
    if (settings != null) settings!.setInt('lastSetlistIndex', setlistIndex);
    loadSong(song);
  }

  KeyClass keyChangeToKeyClass(KeyChange kc) {
    switch (kc) {
      case KeyChange.current:
        return song.header.key.keyClass;
      case KeyChange.original:
        return _originalKey.keyClass;
      case KeyChange.a:
        return KeyClass.a;
      case KeyChange.aSharp:
        return KeyClass.aSharp;
      case KeyChange.bFlat:
        return KeyClass.bFlat;
      case KeyChange.b:
        return KeyClass.b;
      case KeyChange.c:
        return KeyClass.c;
      case KeyChange.cSharp:
        return KeyClass.cSharp;
      case KeyChange.dFlat:
        return KeyClass.dFlat;
      case KeyChange.d:
        return KeyClass.d;
      case KeyChange.dSharp:
        return KeyClass.dSharp;
      case KeyChange.eFlat:
        return KeyClass.eFlat;
      case KeyChange.e:
        return KeyClass.e;
      case KeyChange.f:
        return KeyClass.f;
      case KeyChange.fSharp:
        return KeyClass.fSharp;
      case KeyChange.gFlat:
        return KeyClass.gFlat;
      case KeyChange.g:
        return KeyClass.g;
      case KeyChange.gSharp:
        return KeyClass.gSharp;
      case KeyChange.aFlat:
        return KeyClass.aFlat;
      default:
        return KeyClass.c;
    }
  }

  PopupMenuButton<KeyChange> keyMenu() => PopupMenuButton<KeyChange>(
        icon: Icon(Icons.vpn_key_rounded), // Text('Key'),
        // captureInheritedThemes: false,
        onSelected: (KeyChange result) {
          setState(() {
            if (result == KeyChange.roman) {
              _style = Style.roman;
            } else if (result == KeyChange.nashville) {
              _style = Style.nashville;
            } else {
              _style = Style.letters;
              song.header.key =
                  MusicKey(keyChangeToKeyClass(result), song.header.key.mode);
            }
          });
        },
        itemBuilder: (BuildContext context) => <PopupMenuEntry<KeyChange>>[
          const PopupMenuItem<KeyChange>(
            value: KeyChange.roman,
            child: Text('Roman'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.nashville,
            child: Text('Nashville'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.current,
            child: Text('Current'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.original,
            child: Text('Original'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.a,
            child: Text('A'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.aSharp,
            child: Text('A#'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.bFlat,
            child: Text('Bb'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.b,
            child: Text('B'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.c,
            child: Text('C'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.cSharp,
            child: Text('C#'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.dFlat,
            child: Text('Db'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.d,
            child: Text('D'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.dSharp,
            child: Text('D#'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.eFlat,
            child: Text('Eb'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.e,
            child: Text('E'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.f,
            child: Text('F'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.fSharp,
            child: Text('F#'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.gFlat,
            child: Text('Gb'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.g,
            child: Text('G'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.gSharp,
            child: Text('G#'),
          ),
          const PopupMenuItem<KeyChange>(
            value: KeyChange.aFlat,
            child: Text('Ab'),
          )
        ],
      );

  PopupMenuButton<String> mainMenu() => PopupMenuButton<String>(
        icon: Icon(Icons.menu),
        // captureInheritedThemes: false,
        onSelected: (String result) async {
          switch (result) {
            case "songs":
              Navigator.of(context).push(MaterialPageRoute<Null>(
                builder: (BuildContext context) {
                  return SongChooser(this);
                },
                fullscreenDialog: true,
              ));
              break;
            case "setlists":
              Navigator.of(context).push(MaterialPageRoute<Null>(
                builder: (BuildContext context) {
                  return SetlistChooser(this);
                },
                fullscreenDialog: true,
              ));
              break;
            case "import":
              _importFiles();
              break;
            case "export":
              _exportPDFs(currentSongOnly: true);
              break;
            /* 
              case "refresh":
              await makeSongIndex();
              break;
            */
            case "loadExamples":
              var loaded = await loadExamplesDialog(context);
              // The delay below is used only because Dart does not wait
              // for the loadExamplesDialog to complete.  Bug?
              await Future.delayed(Duration(milliseconds: 5000));
              if (loaded) selectSetlist(0);
              break;
            case "reset":
              var reset = await resetDialog(context);
              // The delay below is used only because Dart does not wait
              // for the loadExamplesDialog to complete.  Bug?
              await Future.delayed(Duration(milliseconds: 5000));
              if (reset) selectSetlist(0);
              break;
            case "about":
              aboutDialog(context);
              break;
            case "help":
              // if (Platform.isLinux)
              launchURL(helpUrl);
              // else
              //  Navigator.of(context).push(MaterialPageRoute<Null>(
              //    builder: (BuildContext context) {
              //      return HelpViewer(this);
              //    },
              //    fullscreenDialog: true,
              //  ));
              break;
            case "licenses":
              showLicensePage(
                  context: context,
                  applicationName: "SessionCharts",
                  applicationVersion: version);
              break;
            case "settings":
              settingsDialog(context);
              break;
          }
        },
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          const PopupMenuItem<String>(
            value: "songs",
            child: Text('Songs'),
          ),
          const PopupMenuItem<String>(
            value: "setlists",
            child: Text('Setlists'),
          ),
          const PopupMenuItem<String>(
            value: "import",
            child: Text('Import'),
          ),
          const PopupMenuItem<String>(
            value: "export",
            child: Text("Export"),
          ),
          const PopupMenuItem<String>(
            value: "loadExamples",
            child: Text('Examples'),
          ),
          const PopupMenuItem<String>(
            value: "reset",
            child: Text('Reset'),
          ),
          const PopupMenuItem<String>(
            value: "settings",
            child: Text('Settings'),
          ),
          const PopupMenuItem<String>(
            value: "help",
            child: Text('Help'),
          ),
          const PopupMenuItem<String>(
            value: "licenses",
            child: Text('Licenses'),
          ),
          const PopupMenuItem<String>(
            value: "about",
            child: Text('About'),
          ),
        ],
      );

  void _importFiles() async {
    // clear temporary files to make sure we get the latest versions
    // FilePicker.platform.clearTemporaryFiles();
    String? lastSong; // the filename of the last song imported
    var r = await FilePicker.platform
        .pickFiles(allowMultiple: true, type: FileType.any);
    if (r != null) {
      final pd = ProgressDialog(context: context);
      pd.show(
          max: r.files.length,
          progressType: ProgressType.valuable,
          msg: 'Importing files...');
      int m = 0; // number of files imported thus far
      for (var e in r.files) {
        m++;
        pd.update(value: m, msg: "Importing files");
        await Future.delayed(Duration(milliseconds: 100));
        // print("e.name = ${e.name}; e.size = ${e.size}");
        final filename = e.name;
        String content = "";
        // store the file in the database
        // print("debug: importing $filename");
        if (e.bytes != null) {
          content = String.fromCharCodes(e.bytes!);
        } else if (e.path != null) {
          File file = File(e.path!);
          content = await file.readAsString();
        }
        // print("debug: content:\n $content");
        // print("debug; import $filename\n $content");
        if (content != "") {
          await storeFile(filename, content);
          if (isSongFile(filename) || isChordProFile(filename))
            lastSong = e.name;
        } else {
          print("debug: empty content");
        }
      }
      await makeSongIndex();
      if (lastSong != null) loadSongWithFilename(lastSong);
      pd.close(delay: 100);
    }
  }

  void _exportPDFs({bool currentSongOnly = true}) async {
    void exportSong(String filename) async {
      final s = await getSongFile(filename);
      if (s == "") return;
      Result? r; // parser result
      try {
        if (isSongFile(filename)) {
          r = lp.song.end().parse(s);
        }
        if (isChordProFile(filename)) {
          r = cp.song.end().parse(s);
        }
      } catch (e) {
        _errorDialog("Syntax Error", "$filename:  $e");
      }
      if (r != null && r.isSuccess) {
        await downloadPDF(r.value, filename, style: Style.letters);
      }
    }

    final pd = ProgressDialog(context: context);
    pd.show(
        max: songIndex.length,
        progressType: ProgressType.valuable,
        msg: 'Exporting PDFs');
    if (currentSongOnly)
      exportSong(songIndex[currentSong].file);
    else {
      for (var i in songIndex) {
        final filename = i.file;
        int m = 0; // number of files exported thus far
        pd.update(value: m, msg: "Exporting PDFs");
        await Future.delayed(Duration(milliseconds: 100));
        exportSong(filename);
      }
    }
    pd.close(delay: 10);
  }
}
