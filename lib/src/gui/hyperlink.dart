// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Hyperlink extends StatelessWidget {
  final String _url;
  final String _text;

  Hyperlink(this._url, this._text);

  Future<void> _launchURL() async {
    if (await canLaunchUrl(Uri.https(_url))) {
      await launchUrl(Uri.https(_url));
    } else {
      throw 'Could not launch $_url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final style = Theme.of(context)
        .textTheme
        .bodyLarge!
        .copyWith(decoration: TextDecoration.underline);
    return InkWell(
      child: Text(
        _text,
        style: style,
      ),
      onTap: _launchURL,
    );
  }
}
