// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'chord_chart_widget.dart';
import 'smufl.dart';
import 'settings.dart';

// interlacedChordsAndLyrics assumes chordsAndLyricParity in
// sectionWidget is true.
Widget interlacedChart(BuildContext context, List<MetricElement>? chords,
    List<LyricElement>? lyric, MusicKey key, Style style) {
  List<Wrap> chart = [];
  var compact = settings?.getBool('compact') ?? false;

  // To show the chord chart, build the metric rows, interlacing chords
  // and lyrics, and add them to the body.
  if (chords != null &&
      chords.isNotEmpty &&
      lyric != null &&
      lyric.isNotEmpty) {
    List<Widget> lineRow = [];
    int li = 0; // current lyric element index
    for (var e in chords) {
      if (e is BarLine) {
        // add remaining phrases in the previous measure
        while (li < lyric.length - 1 && lyric[li] is Phrase) {
          if (lyric[li] is Phrase) {
            Phrase phrase = lyric[li] as Phrase;
            lineRow.add(Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [Text(""), Text(phrase.string.trimLeft())]));
            li += 1;
          }
        }
        BarLine barline = e;
        lineRow.add(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // Text(barline.toString() + ' ')
            children: [barlineToWidget(context, barline), Text('')]));
      } else if (e is Measure) {
        var chords = e.events.cast<Event>();
        for (var chordEvent in chords) {
          if (chordEvent is ChordEvent) {
            // add phrases before the next slot in the lyrics of the measure
            while (li < lyric.length - 1 && lyric[li] is Phrase) {
              if (lyric[li] is Phrase) {
                Phrase phrase = lyric[li] as Phrase;
                lineRow.add(Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [Text(phrase.string.trimLeft())]));
                li += 1;
              }
            }
            if (li < lyric.length - 1 && lyric[li] is Slot) {
              if (li + 1 < lyric.length - 1 && lyric[li + 1] is Phrase) {
                // add chord above phrase after the slot, if there is a phrase
                if (lyric[li + 1] is Phrase) {
                  Phrase phrase = lyric[li + 1] as Phrase;
                  lineRow.add(Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        chordEventToWidget(context, chordEvent, key, style),
                        Text(phrase.string.trimLeft())
                      ]));
                  li = li + 2;
                }
              } else {
                // No phrase left in the measure for this chord. Show
                // the chord above an empty string.
                lineRow.add(Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      chordEventToWidget(context, chordEvent, key, style),
                      Text("")
                    ]));
                li = li + 1;
              }
            } else if (li < lyric.length - 1 && lyric[li] is NewLine) {
              // li is incremented below, with the NewLine of the chords
            }
          }
        }
      } else if (e is NewLine) if (!compact) {
        chart.add(Wrap(
            crossAxisAlignment: WrapCrossAlignment.end, children: lineRow));
        lineRow = []; // start next row
        li += 1;
      } else {
        // add a down left arrow to separate the lines
        lineRow.add(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Text(" $downLeftArrow ")]));
        li += 1;
      }
    }
    if (compact)
      // put all the lines in one row
      chart.add(
          Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: lineRow));
  }

  return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: chart);
}
