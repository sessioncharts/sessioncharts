// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum KeyChange {
  roman,
  nashville,
  current,
  original,
  a,
  aSharp,
  bFlat,
  b,
  c,
  cSharp,
  dFlat,
  d,
  dSharp,
  eFlat,
  e,
  f,
  fSharp,
  gFlat,
  g,
  gSharp,
  aFlat
}
