// Copyright (c) 2021 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/storage/examples.dart';

Future<bool> loadExamplesDialog(BuildContext context) async {
  var result = true;
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text(""),
        content: SingleChildScrollView(
            child: Text("Load the example songs and setlists?")),
        actions: <Widget>[
          TextButton(
            child: Text("Cancel"),
            onPressed: () {
              result = false;
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text("OK"),
            onPressed: () async {
              await loadExamples();
              result = true;
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
  return result;
}
