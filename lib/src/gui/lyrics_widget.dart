// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/main.dart';
import 'smufl.dart';
import 'settings.dart';

Widget lyricsWidget(List<LyricElement>? lyric, bool showSlots) {
  List<Widget> rows = [];
  var lyricRow = StringBuffer();
  var compact = settings?.getBool('compact') ?? false;
  if (lyric != null)
    for (var e in lyric) {
      if (e is Phrase) {
        if (e.string.isNotEmpty) {
          lyricRow.write(e.string.trimLeft());
        }
      } else if (e is Slot) {
        if (showSlots) {
          lyricRow.write('_');
        }
      } else if (e is NewLine) {
        if (!compact) {
          rows.add(RichText(
              text: TextSpan(
                  style: themeManager.theme.textTheme.bodyMedium,
                  text: lyricRow.toString()),
              softWrap: true));
          // reset for next line
          lyricRow = StringBuffer();
        } else {
          // compact
          lyricRow.write(downLeftArrow);
        }
      }
    }
  if (compact)
    rows.add(RichText(
        text: TextSpan(
            style: themeManager.theme.textTheme.bodyMedium,
            text: lyricRow.toString()),
        softWrap: true));
  return Column(crossAxisAlignment: CrossAxisAlignment.start, children: rows);
}
