// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'theme.dart';
import 'tone_pair.dart';
import 'smufl.dart';
import 'drawings.dart';
import 'settings.dart';

Widget noteEventToWidget(
    BuildContext context, NoteEvent noteEvent, MusicKey key, Style style) {
  final bt2 = Theme.of(context).textTheme.bodyMedium!;
  // final ms = musicTextStyle(context);
  final note = noteEvent.note;
  // Use arabic numerals for melodies when
  // chords are displayed using roman numerals.
  if (style == Style.roman) style = Style.nashville;
  final rootPair = toneToTonePair(note.tone, key, style, lowerCase: true);

  // dottedDuration: true for dotted notelengths, such as dotted eighth notes
  bool dottedDuration(NoteLength nl) {
    switch (nl) {
      case NoteLength.dottedQuarter:
        return true;
      case NoteLength.dottedEighth:
        return true;
      case NoteLength.dottedSixteenth:
        return true;
      case NoteLength.dottedThirtySecond:
        return true;
      case NoteLength.dottedSixtyFourth:
        return true;
      default:
        return false;
    }
  }

  var columns = <Widget>[];

  // tie column
  // ToDo: fix heights of the sized boxes
  if (note.tied)
    columns.add(Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [emptyOctaveWidget(context), Text("~")]));

  final rootWidget = RichText(
      text: TextSpan(
          text: rootPair.accidental,
          style: bt2.copyWith(fontSize: bt2.fontSize! * 0.65),
          children: [TextSpan(text: rootPair.root, style: bt2)]));

  var rows = <Widget>[];
  if (octaveAboveMiddle(note.octave))
    rows.add(octaveWidget(context, note.octave));
  else
    rows.add(emptyOctaveWidget(context));
  rows.add(rootWidget);
  if (showHorizontalBars(noteEvent.duration))
    rows.add(horizontalBarWidget(context, noteEvent.duration));
  if (octaveBelowMiddle(note.octave))
    rows.add(octaveWidget(context, note.octave));

  // main note column
  columns.add(Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: rows));

  // dotted rhythm column
  if (dottedDuration(noteEvent.duration))
    columns.add(Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          emptyOctaveWidget(context),
          Text("\u{2022}"), // bullet
        ]));

  return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: columns);
}

// Format a measure in a melody chart.
List<Widget> measureToWidgets(
    BuildContext context, Measure measure, MusicKey key, Style style) {
  // final fontSize = Theme.of(context).textTheme.bodyMedium.fontSize;
  // final emptyBox = SizedBox(height: fontSize * 1.5);
  List<Widget> l = [];

  final dash = Column(children: [
    emptyOctaveWidget(context),
    Text("\u{2013}") // en-dash
  ]);

  // Return the note followed by n dashes
  List<Widget> dashes(NoteEvent e, int n) {
    List<Widget> l = [noteEventToWidget(context, e, key, style)];
    for (var i = 0; i < n; i++) {
      l.add(dash);
    }
    return l;
  }

  List<Widget> noteWidgets(NoteEvent e) {
    List<Widget> l = [];
    switch (e.duration) {
      case NoteLength.whole:
        l.addAll(dashes(e, 3));
        break;
      case NoteLength.half:
        l.addAll(dashes(e, 1));
        break;
      case NoteLength.dottedHalf:
        l.addAll(dashes(e, 2));
        break;
      default: // in case the duration is null
        l.addAll(dashes(e, 0));
    }
    return l;
  }

  // text: puts the text in a column so that
  // it lines up properly in a melody line
  Column text(String s) {
    return Column(children: [emptyOctaveWidget(context), Text(s)]);
  }

  List<Widget> eventWidgets(List events) {
    List<Widget> l = [];
    for (var e in events) {
      if (e is NoteEvent) {
        l.addAll(noteWidgets(e));
      } else if (e is Tuplet) {
        List<Widget> l2 = [];
        l2.add(text('('));
        l2.addAll(eventWidgets(e.events));
        l2.add(text(')'));
        // l.add(Column(children: [Row(children: l2, mainAxisAlignment: MainAxisAlignment.start)]));
        l.addAll(l2);
      }
    }
    return l;
  }

  l.addAll(eventWidgets(measure.events));

  return l;
}

Widget barlineToWidget(BuildContext context, BarLine bl) {
  final bt2 = Theme.of(context).textTheme.bodyMedium!;
  final fontSize = bt2.fontSize!;
  final notationStyle = musicTextStyle(context);
  final extraSpace = SizedBox(height: fontSize / 8.0);
  Widget f(String symbol, [String label = ""]) {
    final labelSize = fontSize * 0.65;
    if (label.isEmpty)
      return Column(children: [
        extraSpace,
        emptyOctaveWidget(context),
        Text(" $symbol ", style: notationStyle)
      ]);
    else
      return Column(children: [
        extraSpace,
        emptyOctaveWidget(context),
        RichText(
            text: TextSpan(text: " " + symbol, style: notationStyle, children: [
          TextSpan(
              text: " " + label + " ", style: bt2.copyWith(fontSize: labelSize))
        ]))
      ]);
  }

  switch (bl.kind) {
    case BarLineSymbol.standard:
      return f(barlineSingle);
    case BarLineSymbol.double:
      return f(barlineDouble);
    case BarLineSymbol.beginRepeat:
      return f(repeatLeft);
    case BarLineSymbol.endRepeat:
      return f(repeatRight);
    case BarLineSymbol.endBeginRepeat:
      return f(repeatRightLeft);
    case BarLineSymbol.variantRepeat:
      return f(barlineSingle, bl.label);
    case BarLineSymbol.end:
      return f(barlineFinal);
    default:
      return f(barlineSingle);
  }
}

// chordChart:  Each row represents a line in the chord chart,
// with a item for each barline and each measure in the line.
// Barlines and measures in each line are lined-up.
Widget melodyWidget(BuildContext context, List<MetricElement>? melody,
    MusicKey key, Style style) {
  final bt2 = Theme.of(context).textTheme.bodyMedium!;
  final fontSize = bt2.fontSize!;
  List<Widget> rows = [];
  List<Widget> row = [];
  var compact = settings?.getBool('compact') ?? false;

  if (melody != null)
    for (var e in melody) {
      if (e is BarLine) {
        row.add(barlineToWidget(context, e));
      } else if (e is Measure) {
        for (var e2 in measureToWidgets(context, e, key, style)) {
          row.add(e2);
        }
      } else if (e is NewLine) {
        if (!compact) {
          rows.add(Wrap(
              runSpacing: fontSize,
              spacing: fontSize / 2,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: row));
          row = []; // reset for next line
        } else {
          // compact
          row.add(Text(downLeftArrow));
        }
      }
    }
  if (compact) {
    rows.add(Wrap(
        runSpacing: fontSize,
        spacing: fontSize / 2,
        crossAxisAlignment: WrapCrossAlignment.start,
        children: row));
  }
  return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: rows);
}
