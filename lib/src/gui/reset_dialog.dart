// Copyright (c) 2021-2022 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/storage/database.dart';

Future<bool> resetDialog(BuildContext context) async {
  var result = true;
  final txt = """Reset by deleting all songs and setlists? 
Warning: You will need to reimport your files to recover them.""";
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text("Reset Files"),
        content: SingleChildScrollView(child: Text(txt)),
        actions: <Widget>[
          TextButton(
            child: Text("Cancel"),
            onPressed: () {
              result = false;
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text("OK"),
            onPressed: () async {
              await resetDatabase();
              result = true;
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
  return result;
}
