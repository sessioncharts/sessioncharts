// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/chordpro_widget.dart';
import 'package:sessioncharts/src/song.dart';
import 'interlaced_chart.dart';
import 'chord_chart_widget.dart';
import 'compact_chart_widget.dart';
import 'lyrics_widget.dart';
import 'melody_widget.dart';
import 'tablature_widget.dart';

// _chordsAndLyricParity: returns true iff the chords and lyric are both not null,
// have the same number of lines and there are the same number of chords
// and slots in each line.
bool _chordsAndLyricParity(
    List<MetricElement>? chords, List<LyricElement>? lyric) {
  if (chords == null || lyric == null) return false;
  List<int> chordLines = [],
      lyricLines = []; // number of chords or slots per line

  // count chords per line
  var n = 0;
  for (var e in chords) {
    if (e is BarLine) {
      continue;
    } else if (e is Measure) {
      n = n + e.events.length;
    } else if (e is NewLine) {
      chordLines.add(n);
      n = 0; // reset for next line
    }
  }

  // count slots per line in the lyric
  var m = 0;
  for (var e in lyric) {
    if (e is Phrase) {
      continue;
    } else if (e is Slot) {
      m += 1;
    } else if (e is NewLine) {
      lyricLines.add(m);
      m = 0; // reset for next line
    }
  }

  // compare chordLines and lyricLines
  if (chordLines.length != lyricLines.length) {
    return false;
  }
  for (var i = 0; i < chordLines.length; i++) {
    if (chordLines[i] != lyricLines[i]) {
      return false;
    }
  }
  return true;
}

// The chords, lyrics, melody and tab switches turn on and off the
// display of these parts of the section
Widget sectionWidget(BuildContext context, Section section, int index,
    MusicKey key, Meter meter, Style style,
    {bool compact = false,
    bool chords = true,
    bool lyrics = true,
    bool melody = true,
    bool tab = true}) {
  const Widget emptyRow = Text(" ");
  List<Widget> body = [];
  void addEmptyRows(int count) {
    for (var i = 0; i < count; i++) {
      body.add(emptyRow);
    }
  }

  final sectionTitle =
      section.name.isEmpty ? "" : section.name + index.toString();

  if (section.comment != null) {
    body.add(
        Text(section.comment!, style: TextStyle(fontStyle: FontStyle.italic)));
  }

  // various ways to format chords and lyrics
  if (compact && chords && lyrics) {
    body.add(
        compactChartWidget(context, section.chords, section.lyric, key, style));
  } else {
    // verbose formats
    if (chords &&
        lyrics &&
        _chordsAndLyricParity(section.chords, section.lyric))
      body.add(
          interlacedChart(context, section.chords, section.lyric, key, style));
    else if (lyrics && chords && section.lyricHasChords()) {
      body.add(chordproWidget(context, section.lyric, key, style));
    } else {
      if (chords && section.chords != null)
        // chords but no lyrics
        body.add(chordChart(context, section.chords, key, meter, style));
      if (lyrics && section.lyric != null)
        // lyrics but no chords
        body.add(lyricsWidget(section.lyric, false));
    }
  }
  if (melody && section.melody != null) {
    body.add(emptyRow);
    body.add(melodyWidget(context, section.melody, key, style));
  }
  if (tab && section.tab != "") {
    body.add(emptyRow);
    body.add(tabWidget(section.tab));
  }
  addEmptyRows(1);
  return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
    Text(sectionTitle.padRight(8),
        style: TextStyle(fontWeight: FontWeight.bold)),
    // Section Comment and Body
    Expanded(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: body)),
  ]);
}
