// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/home_view.dart';
import 'package:sessioncharts/src/storage/database.dart';

class SetlistChooser extends StatefulWidget {
  final HomeViewState home;
  SetlistChooser(this.home);

  @override
  _SetlistChooserState createState() => _SetlistChooserState(home);
}

class _SetlistChooserState extends State<SetlistChooser> {
  HomeViewState home;
  _SetlistChooserState(this.home);

  @override
  Widget build(BuildContext context) {
    final titleStyle = Theme.of(context).textTheme.bodyLarge!;
    final subTitleStyle = Theme.of(context).textTheme.bodyMedium!.copyWith(
        fontStyle: FontStyle.italic, fontSize: titleStyle.fontSize! - 10);
    final sc = ScrollController();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Choose Setlist'),
      ),
      body: Scrollbar(
          controller: sc,
          child: ListView.builder(
              controller: sc,
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(vertical: 8),
              itemCount: setlists.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                    child: Ink(
                        // color: Colors.lightGreen,
                        child: ListTile(
                            title: Text("${setlists[index].title}",
                                style: titleStyle),
                            subtitle: Text("${setlists[index].description}",
                                style: subTitleStyle),
                            onTap: () {
                              home.selectSetlist(index);
                              Navigator.of(context).pop();
                            })));
              })),
    );
  }
}
