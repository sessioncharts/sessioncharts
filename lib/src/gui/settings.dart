// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// with help from https://www.androidcoding.in/2020/07/13/flutter-shared-preference/

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sessioncharts/main.dart' as main;

SharedPreferences? settings;

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();

  static Future init() async {
    settings = await SharedPreferences.getInstance();
  }
}

class _SettingsState extends State<Settings> {
  var _initialized = false;
  var bodyFontSize = 30.0;
  var lastSongFile = "";
  var compact = false;

  @override
  Widget build(BuildContext context) {
    if (settings != null && !_initialized) {
      if (settings!.getDouble('bodyFontSize') != null)
        this.bodyFontSize = settings!.getDouble('bodyFontSize')!;
      else if (Theme.of(context).textTheme.bodyLarge != null)
        this.bodyFontSize = Theme.of(context).textTheme.bodyLarge!.fontSize!;
      this.lastSongFile =
          settings!.getString('lastSongFile') ?? "BillBailey.sc";
      this.compact = settings?.getBool('compact') ?? false;
      _initialized = true;
    }
    return Form(
        child: Scrollbar(
            child: Align(
                alignment: Alignment.topCenter,
                child: Card(
                    child: SingleChildScrollView(
                        padding: EdgeInsets.all(16),
                        child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 400),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  fontSizeControl(),
                                  compactControl(),
                                  saveOrCancel()
                                ])))))));
  }

  Column fontSizeControl() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Preferred Font Size',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            Text(
              bodyFontSize.toInt().toString(),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
        Slider(
          min: 10,
          max: 50,
          divisions: 50,
          value: bodyFontSize,
          label: bodyFontSize.round().toString(),
          onChanged: (double value) {
            setState(() {
              bodyFontSize = value;
            });
          },
        ),
      ],
    );
  }

  Column compactControl() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              'Compact View',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            Checkbox(
              checkColor: Colors.white,
              // fillColor: MaterialStateProperty.resolveWith(getColor),
              value: compact,
              onChanged: (bool? x) {
                setState(() {
                  compact = x ?? true;
                });
              },
            )
          ])
        ]);
  }

  Row saveOrCancel() {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      ElevatedButton(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          }),
      SizedBox(width: 100),
      ElevatedButton(
          child: Text("Save"),
          onPressed: () {
            if (settings != null) {
              settings!.setDouble('bodyFontSize', bodyFontSize);
              settings!.setBool('compact', compact);
            }
            main.themeManager.update();
            // setState(() {});
            Navigator.of(context).pop();
          }),
    ]);
  }
}

void settingsDialog(BuildContext context) {
  // flutter defined function
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Settings();
      });
}
