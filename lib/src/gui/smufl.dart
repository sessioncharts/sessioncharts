// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0
// Standard Music Font Layout
// https://www.smufl.org/fonts/

const accidentalSharp = "\u{266F}";
const accidentalFlat = "\u{266D}";
const csymAccidentalSharp = "\u{ED62}";
const csymAccidentalFlat = "\u{ED60}";
const csymMajorSeventh = "\u{E873}"; // delta
const csymDiminished = "\u{E870}"; // o
const csymHalfDiminished = "\u{E871}"; // circle crossed
const csymAugmented = "\u{E872}"; // +
const articTenutoAbove = "\u{1D17D}"; // Nashville hold
const articMarcatoStaccatoAbove = "\u{1D180}"; // Nashville choke
const articAccentAbove = "\u{1D17B}"; // Nashville push
const csymAlteredBassSlash = "\u{E87B}"; // Chord bass note slash
const csymDiagonalArrangementSlash = "\u{E87C}";
const repeatBarSlash = "\u{E504}"; // rhythmic slash
const noteheadWhole = "\u{E0A2}"; // whole note
const noteHalfUp = "\u{1D15E}"; // half note
const noteQuarterUp = "\u{1D15F}"; // quarter note
const note8thUp = "\u{E1D7}"; // eighth note
const note16thUp = "\u{E1D9}"; // sixteenth note
const note32thUp = "\u{E1DB}"; // thirtysecond note
const note64thUp = "\u{E1DD}"; // sixtyfourth note
const augmentationDot = "\u{1D16D}"; // for dotted notes
const barlineSingle = "\u{1D100}"; // standard barline
const barlineDouble = "\u{1D101}"; // double barline
const repeatLeft = "\u{1D106}"; // begin repeat
const repeatRight = "\u{1D107}"; // end repeat
const repeatRightLeft = "\u{E042}"; // end begin repeat
const barlineFinal = "\u{1D102}"; // end barline
const repeat1Bar = "\u{1D10E}"; // repeat previous measure
const repeatBarUpperDot = "\u{E503}";
const repeatBarLowerDot = "\u{E505}";
// for numbered musical notation:
const dotOperator = "\u{22C5}"; // one octave higher or lower
const ratio = "\u{2236}"; // two octaves higher or lower
const verticalEllipsis = "\u{22EE}"; // three octaves higher or lower
const minusSign = "\u{2212}"; // eigth note
const equalsSign = "\u{003D}"; // sixteenth note
const identicalTo = "\u{2261}"; // thirty-second note
const strictlyEquivalentTo = "\u{2263}"; // sixty-fourth note

// other special characters, defined in the Unicode standard
// and not limited to SMUFL fonts
const downLeftArrow = "\u{2936}";
const diamond = "\u{25C7}"; // Nashville hold alternative
const lessThanSign = "\u{003C}"; // Nashville push alternative
const caret = "\u{2038}"; // Nashville choke  alternative
