// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/home_view.dart';
import 'package:sessioncharts/src/gui/song_view.dart';
import 'package:sessioncharts/src/storage/database.dart';

// import 'package:sessioncharts/src/song.dart' as sng;

class SongChooser extends StatefulWidget {
  final HomeViewState home;
  SongChooser(this.home);

  @override
  _SongChooserState createState() => _SongChooserState(home);
}

class _SongChooserState extends State<SongChooser> {
  HomeViewState home;
  _SongChooserState(this.home);

  @override
  Widget build(BuildContext context) {
    final titleStyle = Theme.of(context).textTheme.bodyLarge!;
    final subtitleStyle = Theme.of(context).textTheme.bodyLarge!.copyWith(
        fontSize: titleStyle.fontSize! - 10, fontStyle: FontStyle.italic);
    final sc = ScrollController();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Choose Song'),
      ),
      body: Scrollbar(
          controller: sc,
          child: ListView.builder(
              controller: sc,
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(vertical: 8),
              itemCount: songIndex.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                    child: Ink(
                        // color: Colors.lightGreen,
                        child: ListTile(
                            title: Text("${sortTitle(songIndex[index])}",
                                style: titleStyle),
                            subtitle: Text("${contributors(songIndex[index])}",
                                style: subtitleStyle),
                            onTap: () {
                              home.loadSong(index);
                              Navigator.of(context).pop();
                            })));
              })),
    );
  }
}
