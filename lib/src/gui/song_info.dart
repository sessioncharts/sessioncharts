// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:sessioncharts/src/gui/home_view.dart';
import 'hyperlink.dart';
import 'package:sessioncharts/src/song.dart';

// import 'package:sessioncharts/src/song.dart' as sng;

class SongInfo extends StatefulWidget {
  final HomeViewState home;
  SongInfo(this.home);

  @override
  _SongInfoState createState() => _SongInfoState(home);
}

class _SongInfoState extends State<SongInfo> {
  HomeViewState home;
  _SongInfoState(this.home);
  TextStyle _fieldTextStyle = TextStyle(fontSize: 10.0);

  @override
  Widget build(BuildContext context) {
    if (Theme.of(context).textTheme.bodyLarge != null)
      _fieldTextStyle = Theme.of(context).textTheme.bodyLarge!;
    final header = home.song.header;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Song Information'),
        ),
        body: _buildForm(header));
  }

  Widget _buildForm(Meta header) {
    final sc = ScrollController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (sc.hasClients) {
        sc.animateTo(
          sc.position.minScrollExtent,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 200),
        );
      }
    });
    return Container(
        padding: EdgeInsets.all(20),
        child: Scrollbar(
            thumbVisibility: true,
            controller: sc,
            child: SingleChildScrollView(
                controller: sc,
                scrollDirection: Axis.vertical, //.horizontal
                child: Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _buildTitle(header.title),
                        _buildSubtitle(header.subtitle),
                        _buildComposer(header.composer),
                        _buildLyricist(header.lyricist),
                        _buildArtist(header.artist),
                        _buildRecording(header.recording),
                        _buildAlbum(header.album),
                        _buildYear(header.year),
                        _buildCopyright(header.copyright),
                        _buildKey(header.key),
                        _buildMeter(header.meter),
                        _buildRhythm(header.rhythm),
                        _buildTempo(header.tempo),
                        _buildNotes(header.notes),
                        // Further ABC header fields which don't seem too useful
                        // for now:
                        // _buildOrigin(header.origin),
                        // _buildUnitNoteLength(header.unitNoteLength),
                        // _buildTranscription(header.transcription),
                        // _buildBook(header.book),
                        // _buildSource(header.source),
                      ],
                    )))));
  }

  Widget _buildTitle(String title) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Title'),
      style: _fieldTextStyle,
      initialValue: title,
      readOnly: true,
    );
  }

  Widget _buildSubtitle(String subtitle) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Subtitle'),
      style: _fieldTextStyle,
      initialValue: subtitle,
      readOnly: true,
    );
  }

  Widget _buildComposer(String composer) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Composer'),
      style: _fieldTextStyle,
      initialValue: composer,
      readOnly: true,
    );
  }

  Widget _buildLyricist(String lyricist) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Lyricist'),
      style: _fieldTextStyle,
      readOnly: true,
    );
  }

  Widget _buildArtist(String artist) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Artist'),
      style: _fieldTextStyle,
      initialValue: artist,
      readOnly: true,
    );
  }

  Widget _buildCopyright(String copyright) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Copyright'),
      style: _fieldTextStyle,
      initialValue: copyright,
      readOnly: true,
    );
  }

// Widget _buildOrigin(String origin) {
//   return TextFormField(
//     decoration: InputDecoration(labelText: 'Origin'),
//     style: _fieldTextStyle,
//     initialValue: origin,
//     readOnly: true,
//   );
// }

  Widget _buildMeter(Meter meter) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Meter'),
      style: _fieldTextStyle,
      initialValue: meter.toString(),
      readOnly: true,
    );
  }

// Widget _buildUnitNoteLength(NoteLength noteLength) {
//   return TextFormField(
//     decoration: InputDecoration(labelText: 'Unit Note Length'),
//     style: _fieldTextStyle,
//     initialValue: noteLengthToString(noteLength),
//     readOnly: true,
//   );
// }

  Widget _buildTempo(Tempo tempo) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tempo'),
      style: _fieldTextStyle,
      initialValue: tempo.toString(),
      readOnly: true,
    );
  }

// Widget _buildTranscription(String transcription) {
//   return TextFormField(
//     decoration: InputDecoration(labelText: 'Transcription'),
//     style: _fieldTextStyle,
//     initialValue: transcription,
//     readOnly: true,
//   );
// }

  Widget _buildNotes(String notes) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Notes'),
      style: _fieldTextStyle,
      minLines: 2,
      maxLines: 4,
      initialValue: notes,
      readOnly: true,
    );
  }

  Widget _buildKey(MusicKey key) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Key'),
      style: _fieldTextStyle,
      initialValue: key.toString(),
      readOnly: true,
    );
  }

  Widget _buildRhythm(String rhythm) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Rhythm'),
      style: _fieldTextStyle,
      initialValue: rhythm,
      readOnly: true,
    );
  }

// Widget _buildBook(String book) {
//   return TextFormField(
//     decoration: InputDecoration(labelText: 'Book'),
//     style: theme.textTheme.bodyLarge,
//     initialValue: book,
//     readOnly: true,
//   );
// }

  Widget _buildAlbum(String album) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Album'),
      style: _fieldTextStyle,
      initialValue: album,
      readOnly: true,
    );
  }

  Widget _buildYear(String year) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Year'),
      style: _fieldTextStyle,
      initialValue: year,
      readOnly: true,
    );
  }

// Widget _buildSource(String source) {
//   return TextFormField(
//     decoration: InputDecoration(labelText: 'Source'),
//     style: _fieldTextStyle,
//     initialValue: source,
//     readOnly: true,
//   );
// }

  Widget _buildRecording(String recording) {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            // icon: const Icon(Icons.color_lens),
            labelText: 'Recording',
          ),
          // isEmpty:
          child: Hyperlink(Uri.encodeFull(recording), recording),
        );
      },
    );
    // readOnly: true,
  }
}
