// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:sessioncharts/src/gui/section_widget.dart';
import 'package:sessioncharts/src/song.dart';

String contributors(Meta? m) {
  if (m != null) {
    final subtitle = m.subtitle;
    final composer = m.composer;
    final lyricist = m.lyricist;
    final artist = m.artist;
    var l = [subtitle, composer, lyricist, artist];
    l.removeWhere((item) => item == "");
    return l.join(" / ");
  } else {
    return "";
  }
}

String sortTitle(Meta m) {
  return m.sortTitle != "" ? m.sortTitle : m.title;
}

String properties(Meta m) {
  final key = m.key.toString();
  final rhythm = m.rhythm;
  final meter = m.meter.toString();
  final tempo = m.tempo.toString();
  final capo = m.capo == 0 ? "" : "capo: " + m.capo.toString();

  return key +
      (meter == "" ? "" : "; " + meter) +
      (rhythm == "" ? "" : " " + rhythm) +
      (tempo == "" ? "" : "; " + tempo) +
      (capo == "" ? "" : "; " + capo);
}

class SongView extends StatelessWidget {
  final Song? song;
  final Style style;
  final bool chords, lyrics, melody, tab, compact;

  SongView(
      {@required this.song,
      this.style = Style.roman,
      this.chords = true,
      this.lyrics = true,
      this.melody = true,
      this.tab = true,
      this.compact = false,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> rows = [];
    var sectionIndex = Map();

    int incrementSection(String sectionName) {
      if (sectionIndex.containsKey(sectionName)) {
        sectionIndex[sectionName] = sectionIndex[sectionName] + 1;
      } else {
        sectionIndex[sectionName] = 1;
      }
      return sectionIndex[sectionName];
    }

    Row titleRow(BuildContext context) {
      String title = song?.header.title ?? "";
      String credits = song != null ? contributors(song!.header) : "";
      String s;
      if (credits != "") {
        s = "$title – $credits";
      } else {
        s = title;
      }
      return Row(
          children: [Text(s, style: Theme.of(context).textTheme.displayLarge)]);
    }

    Row propertiesRow(BuildContext context) {
      if (song != null && !song!.isEmpty())
        return Row(children: [
          Text(properties(song!.header),
              style: Theme.of(context).textTheme.displayMedium)
        ]);
      else
        return Row(children: []);
    }

    if (song != null) {
      // print("key = ${song!.header.key.toString()}");
      song!.normalizeChords(); // to facilitate trasposition
      song!.completeArrangement();
    }

    List<Widget> header = [titleRow(context), propertiesRow(context)];
    if (song != null && song!.header.notes.isNotEmpty)
      header.add(Text(song!.header.notes,
          style: TextStyle(fontStyle: FontStyle.italic)));
    // header
    rows.add(FittedBox(
        fit: BoxFit.fill,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: header)));
    rows.add(Text(" ")); // empty row
    if (song != null) {
      MusicKey key = song!.header.key;
      // If the capo is on the nth fret, transpose *down* n steps
      if (song!.header.capo != 0) key = key.transpose(-song!.header.capo);

      for (var section in song!.arrangement) {
        rows.add(sectionWidget(context, section, incrementSection(section.name),
            key, song!.header.meter, style,
            compact: compact,
            chords: chords,
            lyrics: lyrics,
            melody: melody,
            tab: tab));
      }
    }

    final sc = ScrollController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (sc.hasClients) {
        sc.animateTo(
          sc.position.minScrollExtent,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 200),
        );
      }
    });
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
          padding: EdgeInsets.all(20),
          child: SizedBox(
              width: constraints.maxWidth,
              child: Scrollbar(
                  thumbVisibility: true,
                  controller: sc, // sc.hasClients ? sc : null,
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical, //.horizontal
                      controller: sc,
                      child: Container(
                          padding: EdgeInsets.only(right: 20),
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: rows))))));
    });
  }
}
