// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// Format of tabulatur
Widget tabWidget(String tab) {
  return FittedBox(
      fit: BoxFit.fill, child: Text(tab, style: GoogleFonts.robotoMono()));
}
