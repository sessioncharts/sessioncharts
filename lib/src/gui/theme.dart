// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/material.dart';
import 'package:sessioncharts/src/gui/settings.dart';

var notationFontFamily = 'BravuraText'; // 'Roboto';
var defaultFontSize = 12.0;

class ThemeManager with ChangeNotifier {
  static double bodyFontSize =
      settings?.getDouble('bodyFontSize') ?? defaultFontSize;
  ThemeData theme = makeTheme(bodyFontSize);

  void update() {
    bodyFontSize = settings?.getDouble('bodyFontSize') ?? defaultFontSize;
    theme = makeTheme(bodyFontSize);
    notifyListeners();
  }
}

// The font size of the text in chord charts, lyrics, melodies and tabs
// All other font sizes are defined relative to this size.

ThemeData makeTheme([double bodyFontSize = 30.0]) {
  double uiFontSize = 24.0;
  return ThemeData(
    // primarySwatch: Colors.blue,
    // fontFamily: 'Roboto',
    // See https://api.flutter.dev/flutter/material/TextTheme-class.html
    textTheme: TextTheme(
        // used for lists, e.g. in the song and setlist choosers
        bodyLarge: TextStyle(fontSize: uiFontSize),
        // used for song properties (fields), chords,
        // lyrics, tabs and melodies
        bodyMedium: TextStyle(fontSize: bodyFontSize),
        // used for song titles in song views
        displayLarge: TextStyle(
            fontSize: bodyFontSize + 2.0,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        // used for other song properties in the song view
        displayMedium: TextStyle(
            fontSize: bodyFontSize,
            fontWeight: FontWeight.normal,
            color: Colors.black),
        // used by PopupMenuItem in the key change menu
        titleMedium: TextStyle(fontSize: uiFontSize)),
  );
}

TextStyle? musicTextStyle(BuildContext context) {
  return Theme.of(context)
      .textTheme
      .bodyMedium
      ?.copyWith(fontFamily: notationFontFamily);
}
