// Copyright (c) 2021 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:url_launcher/url_launcher.dart';

void launchURL(String url) async {
  final uri = Uri.parse(url);
  if (await canLaunchUrl(uri)) {
    await launchUrl(uri);
  } else {
    throw 'Could not launch $url';
  }
}
