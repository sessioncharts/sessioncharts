// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0
// ChordPro parser.  Based on the ChordPro 6
// specification at https://www.chordpro.org/

import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/song.dart';
import 'common.dart';

final Parser textFieldName = string('title') |
    string('sorttitle') |
    string('subtitle') |
    string('artist') |
    string('composer') |
    string('lyricist') |
    string('copyright') |
    string('album') |
    string('year') |
    // key, time, tempo and capo handled separately below
    string('duration') |
    string('meta') |
    string('t') | // abbrev for title
    string('st'); // abbrev for subtitle

final Parser key = (string('{key') &
        ((char(':') & ws.star()) | ws.plus()) &
        chordPitchClass &
        accidental.optional() &
        ws.star() &
        extChar.star().flatten() &
        ws.star() &
        char('}'))
    .map((l) {
  return [
    "key",
    MusicKey(
        stringToKeyClass(l[2] + (l[3] == null ? '' : l[3])), stringToMode(l[5]))
  ];
});

final Parser time = (string('{time') &
        ((char(':') & ws.star()) | ws.plus()) &
        digit() &
        char('/') &
        digit() &
        ws.star() &
        char('}'))
    .map((l) {
  return ["time", Meter(int.parse(l[2]), int.parse(l[4]))];
});

final Parser tempo = (string('{tempo') &
        ((char(':') & ws.star()) | ws.plus()) &
        digit().plus().flatten() &
        ws.star() &
        char('}'))
    .map((l) {
  return ["tempo", Tempo(int.parse(l[2]))];
});

final Parser capo = (string('{capo') &
        ((char(':') & ws.star()) | ws.plus()) &
        digit().plus().flatten() &
        ws.star() &
        char('}'))
    .map((l) {
  return ["capo", int.parse(l[2])];
});

final Parser metaDataDirective = (char('{') &
            textFieldName &
            ((char(':') & ws.star()) | ws.plus()) &
            anyChar.plus().flatten() &
            char('}'))
        .map((l) => [l[1], l[3]]) |
    key |
    time |
    tempo |
    capo;

final Parser metaDataDirectiveLine = (whitespaceOrCodeComment.star() &
        metaDataDirective &
        whitespaceOrCodeComment)
    .map((l) => l[1]);

final Parser header = metaDataDirectiveLine.plus().map((l) {
  var meta = Meta();
  for (var i in l) {
    if (i == '') continue;
    var name = i[0];
    var value = i[1];
    switch (name) {
      case 'title':
        meta.title = value;
        break;
      case 't':
        meta.title = value;
        break;
      case 'sorttitle':
        meta.sortTitle = value;
        continue;
      case 'subtitle':
        meta.subtitle = value;
        break;
      case 'st':
        meta.subtitle = value;
        break;
      case 'artist':
        meta.artist = value;
        break;
      case 'composer':
        meta.composer = value;
        break;
      case 'lyricist':
        meta.lyricist = value;
        break;
      case 'copyright':
        meta.copyright = value;
        break;
      case 'album':
        meta.origin = value;
        break;
      case 'year':
        meta.year = value;
        break;
      case 'key':
        meta.key = value;
        break;
      case 'time':
        meta.meter = value;
        break;
      case 'tempo':
        meta.tempo = value;
        break;
      case 'capo':
        meta.capo = value;
        break;
      default:
        continue;
    }
  }
  return meta;
});

// These comments are included in the output
// as directives for the musicians
final Parser commentDirective = string('comment_italic') |
    string('comment_box') |
    string('comment') |
    string('highlight') |
    string('ci') | // abbrev for comment_italic
    string('cb') | // abbrev for comment_box
    string('c'); // abbrev for comment

final Parser comment = (ws.star() &
        char('{') &
        commentDirective &
        ((char(':') & ws.star()) | ws.plus()) &
        (anyChar | whitespace()).star().flatten() &
        char('}') &
        whitespaceOrCodeComment)
    .map((l) => l[4]);

final Parser lyricBlock =
    (whitespaceOrCodeComment.star() & lyricLine.plus()).map((l) {
  List<LyricElement> result = [];
  for (var i in l[1]) {
    for (var j in i) if (j is LyricElement) result.add(j);
  }
  return result;
});

BarLineSymbol stringToBarLineSymbol(String s) {
  switch (s) {
    case "|.":
      return BarLineSymbol.end;
    case "|":
      return BarLineSymbol.standard;
    case "||":
      return BarLineSymbol.double;
    case "|:":
      return BarLineSymbol.beginRepeat;
    case ":|":
      return BarLineSymbol.endRepeat;
    case ":|:":
      return BarLineSymbol.endBeginRepeat;
    default:
      return BarLineSymbol.standard;
  }
}

final Parser barSymbol = (string('||') |
        string('|:') |
        string('|.') | // end bar line
        string('|') |
        string(':|:') |
        string(':|'))
    .map((value) => stringToBarLineSymbol(value));

final Parser barLine = (barSymbol).map((l) => BarLine(l));

final Parser gridElement = (ws.star() &
        (barLine | chord | char('.') | char('/') | string("%%") | char('%')) &
        ws.star())
    .map((l) => l[1]);

// gridElements: all the elements in a grid line, including barlines,
// but without margin notes
// START HERE: reset the prev and prev prev measures and skip
// over the elements of repeated measures starting with % or %%
final Parser gridElements = gridElement.plus().map((l) {
  List<MetricElement> elements = [];
  List<Event> prevMeasure = [];
  List<Event> prevPrevMeasure = []; // the measure before the previous measure
  var repeatMeasures = 0; // the number of measures to repeat
  for (var i = 0; i < l.length; i++) {
    if (l[i] is BarLine) {
      if (repeatMeasures == 2 && prevPrevMeasure.isNotEmpty) {
        repeatMeasures -= 1;
        elements.add(Measure(prevPrevMeasure));
      } else if (repeatMeasures == 1 && prevMeasure.isNotEmpty) {
        repeatMeasures -= 1;
        elements.add(Measure(prevMeasure));
      } else {
        elements.add(Measure(prevMeasure));
      }
      elements.add(l[i]); // add the barline
    } else if (l[i] is Chord) {
      // It shouldn't be necessary to skip over chords, since
      // only dots (.) should appear after a %% or % in the measures
      // to be skipped over. So we are being careful here to handle
      // gracefully a syntactic error.
      if (repeatMeasures == 0) {
        var j = i + 1;
        var n = 1; // duration in number of quarter notes
        // count the duration of the chord in quarter notes
        while (j < l.length - 1 && (l[j] == "." || l[j] == "/")) {
          n += 1;
          j += 1;
        }
        var duration = NoteLength.implicit; // default
        switch (n) {
          case 1:
            duration = NoteLength.quarter;
            break;
          case 2:
            duration = NoteLength.half;
            break;
          case 3:
            duration = NoteLength.dottedHalf;
            break;
          case 4:
            duration = NoteLength.whole;
            break;
        }
        final ce = ChordEvent(l[i], duration);
        if (i > 0 && l[i - 1] is BarLine) {
          // then this is the first chord of the measure
          // and the previous measures are not being repeated.
          // so adjust the prevMeasure and prePrevMeasure
          prevPrevMeasure = prevMeasure;
          prevMeasure = [];
        }
        prevMeasure.add(ce);
      }
    } else if (l[i] == '.' || l[i] == '/') {
      // these are handled above in the Chord branch
      continue;
    } else if (l[i] == "%%") {
      // repeat the last two measures
      repeatMeasures = 2;
    } else if (l[i] == "%") {
      // repeat the last measure
      repeatMeasures = 1;
    }
  }
  return elements;
});

final Parser gridMarginNote = (digit() | extChar | ws).star();

// margin notes (anything before the first bar line and after
// the last bar line) are ignored
final Parser gridLine = (gridMarginNote.optional() &
        barLine &
        gridElements &
        gridMarginNote.optional() &
        newLine)
    .map((l) {
  var result = l[2];
  result.insert(0, l[1]); // restore the first bar line
  result.add(NewLine());
  return result;
});

final Parser gridBlock = gridLine.plus().map((l) {
  List<MetricElement> result = [];
  // l should be a list of a list of metric elements
  for (var i in l) {
    for (var j in i) if (j is MetricElement) result.add(j);
  }
  return result;
});

final Parser gridComment =
    // after a colon comes a comment to be displayed
    (char(':') & ws.star() & anyChar.star().flatten()).map((l) => l[2]) |
        // if there is no colon, the remainder of the
        // text is a ChordPro grid specification about the number or cells
        // which can be ignored for our purposes
        (ws.star() & anyChar.star()).map((l) => "");

// ChordPro grids.  Limitations:
// 1. cell and margin specifications are ignored. (The number of cells per
// line is not needed, since SessionCharts allows any variable
// number of chords per measure and line.)
// 2. Margin notes are ignored.
final Parser gridSection = (whitespaceOrCodeComment.star() &
        (string("{start_of_grid") | string("{sog")) &
        gridComment.optional() &
        char('}') &
        whitespaceOrCodeComment.star() &
        gridBlock &
        (string("{end_of_grid}") | string("{eog}")) &
        whitespaceOrCodeComment.star())
    .map((l) {
  return Section(name: "", comment: l[2], chords: l[5]);
});

final Parser tabStart = (string("{start_of_tab}") | string("{sot}")) |
    ((string('{start_of_tab:') | string('{sot:')) &
            ws.star() &
            anyChar.star().flatten() &
            char('}'))
        .map((l) => l[2]);

final Parser tabSection = (whitespaceOrCodeComment.star() &
        char('{') &
        (string("start_of_tab") | string("sot")) &
        sectionComment.optional() &
        char('}') &
        whitespaceOrCodeComment &
        tabLine.plus().flatten() &
        (string('{end_of_tab}') | string('{eot}')) &
        whitespaceOrCodeComment.star())
    .map(
        (l) => Section(name: "", comment: l[3] != null ? l[3] : "", tab: l[6]));

final Parser sectionBeginLabel = string('start_of_chorus') |
    string('soc') |
    string('start_of_verse') |
    string('sov') |
    string('start_of_bridge') |
    string('sob');

final Parser sectionEndDirective = ws.star() &
    (string('{end_of_chorus}') |
        string('{eoc}') |
        string('{end_of_verse}') |
        string('{eov}') |
        string('{end_of_bridge}') |
        string('{eob}'));

final Parser sectionComment =
    (((char(':') & ws.star()) | ws.plus()) & anyChar.star().flatten())
        .map((l) => l[1]);

final Parser markedSection = (whitespaceOrCodeComment.star() &
        char('{') &
        sectionBeginLabel &
        sectionComment.optional() &
        char('}') &
        whitespaceOrCodeComment.star() &
        lyricBlock.optional() &
        sectionEndDirective &
        whitespaceOrCodeComment.star())
    .map<Section>((l) {
  var name = "S";
  if (l[2] == "start_of_chorus" || l[2] == "soc")
    // always use "C" to name choruses, overriding the name provided, if
    // any, so the "chorus" directive will use the last chorus as a template
    name = "C";
  else if (l[2] == "start_of_verse" || l[2] == "sov") {
    name = "V";
  } else if (l[2] == "start_of_bridge" || l[2] == "sob") {
    name = "B";
  }
  return Section(name: name, comment: l[3] != null ? l[3] : "", lyric: l[6]);
});

final Parser plainSection = (whitespaceOrCodeComment.star() &
        lyricBlock &
        whitespaceOrCodeComment.star())
    .map<Section>((l) => Section(name: "V", lyric: l[1]));

final Parser chorusComment =
    (((char(':') & ws.star()) | ws.plus()) & anyChar.star().flatten())
        .map((l) => l[1]);

final Parser chorus = (whitespaceOrCodeComment.star() &
        string('{chorus') &
        chorusComment.optional() &
        char('}') &
        whitespaceOrCodeComment.star())
    .map<Section>((l) => Section(name: "C", comment: l[2] != null ? l[2] : ""));

final Parser commentSection =
    (whitespaceOrCodeComment.star() & comment & whitespaceOrCodeComment.star())
        .map((l) => Section(name: "", comment: l[1]));

final Parser section = markedSection |
    chorus |
    tabSection |
    commentSection |
    gridSection |
    plainSection;

final Parser song =
    (header & (section | whitespaceOrCodeComment).star()).map((l) {
  var meta = l[0];
  List<Section> arrangement = [];
  for (var e in List.from(l[1])) if (e is Section) arrangement.add(e);
  return Song(meta, arrangement);
});
