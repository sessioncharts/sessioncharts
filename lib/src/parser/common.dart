// Copyright (c) 2019-2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0
// Parser functions used by both the ChordPro and SessionCharts parsers

import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/song.dart';

// handles new lines in Unix and Windows
final Parser newLine = char('\n') | (char('\r') & char('\n'));

final Parser squareBrackets = char('[') | char(']');
final Parser curlyBrackets = char('{') | char('}');

// punctation: (almost) all symbols except curly bracket,
// square brackets and the hash symbol (#)
final Parser punctuation = char(',') |
    char('‚') | // a different comma character then ','
    char(';') |
    char('.') |
    char(':') |
    char(';') |
    char('-') |
    char('–') |
    char('?') |
    char('!') |
    char('(') |
    char(')') |
    char("'") |
    char("’") |  // different than the one above it|  // different than the one above it
    char("'") |
    char('"') |
    char('&') |
    char('\$') |
    char('€') |
    char('@') |
    char('%') |
    char('§') |
    char('/') |
    char('\\') |
    char('~') |
    char('+') |
    char('*') |
    char('<') |
    char('>') |
    char('_') |
    char('|') |
    char('^') |
    char('‘') |
    char('’') |
    char('“') |
    char('”') |
    char('`') |
    char('¡') |
    char('¿') |
    char('«') |
    char('»') |
    char('×') |
    char('÷') |
    char('±') |
    char('£') |
    char('¢') |
    char('§') |
    char('©') |
    char('¬') |
    char('®') |
    char('µ') |
    char('¶') |
    char('=');

// whitespace without new lines, deleted
final Parser ws = (char(' ') | char('\t')).plus().map((l) => "");

// whitespace with new lines, deleted
final Parser whitespaceWithNL = (ws | newLine).map((l) => "");

// letter() extended with Latin 1 characters and some
// additional for coverage of most Western European languages
// See https://en.wikipedia.org/wiki/ISO/IEC_8859-1
final Parser extChar = 
    letter() |  
    pattern("À-ÿ"); 

// anyChar: any extChar except curly brackets and a new line
final Parser anyChar = digit() | extChar | punctuation | squareBrackets | ws;

final Parser chordPitchClass = char('A') |
    char('B') |
    char('C') |
    char('D') |
    char('E') |
    char('F') |
    char('G');

KeyClass stringToKeyClass(String s) {
  switch (s.toLowerCase()) {
    case "a":
      return KeyClass.a;
    case "a#":
      return KeyClass.aSharp;
    case "bb":
      return KeyClass.bFlat;
    case "b":
      return KeyClass.b;
    case "c":
      return KeyClass.c;
    case "c#":
      return KeyClass.cSharp;
    case "db":
      return KeyClass.dFlat;
    case "d":
      return KeyClass.d;
    case "d#":
      return KeyClass.dSharp;
    case "eb":
      return KeyClass.eFlat;
    case "e":
      return KeyClass.e;
    case "f":
      return KeyClass.f;
    case "f#":
      return KeyClass.fSharp;
    case "gb":
      return KeyClass.gFlat;
    case "g":
      return KeyClass.g;
    case "g#":
      return KeyClass.gSharp;
    case "ab":
      return KeyClass.aFlat;
    default:
      return KeyClass.c;
  }
}

// support both roman numerals and nashville numbers
final Parser scaleDegree = string('III') |
    string('II') |
    string('IV') |
    string('I') |
    string('VII') |
    string('VI') |
    string('V') |
    string('1') |
    string('2') |
    string('3') |
    string('4') |
    string('5') |
    string('6') |
    string('7');

ScaleDegree stringToScaleDegree(String s) {
  switch (s.toLowerCase()) {
    case "i":
      return ScaleDegree.i;
    case "1":
      return ScaleDegree.i;
    case "i#":
      return ScaleDegree.iSharp;
    case "1#":
      return ScaleDegree.iSharp;
    case "iib":
      return ScaleDegree.iSharp;
    case "2b":
      return ScaleDegree.iSharp;
    case "ii":
      return ScaleDegree.ii;
    case "2":
      return ScaleDegree.ii;
    case "ii#":
      return ScaleDegree.iiSharp;
    case "2#":
      return ScaleDegree.iiSharp;
    case "iiib":
      return ScaleDegree.iiSharp;
    case "3b":
      return ScaleDegree.iiSharp;
    case "iii":
      return ScaleDegree.iii;
    case "3":
      return ScaleDegree.iii;
    case "iv":
      return ScaleDegree.iv;
    case "4":
      return ScaleDegree.iv;
    case "iv#":
      return ScaleDegree.ivSharp;
    case "4#":
      return ScaleDegree.ivSharp;
    case "vb":
      return ScaleDegree.ivSharp;
    case "5b":
      return ScaleDegree.ivSharp;
    case "v":
      return ScaleDegree.v;
    case "5":
      return ScaleDegree.v;
    case "v#":
      return ScaleDegree.vSharp;
    case "5#":
      return ScaleDegree.vSharp;
    case "vib":
      return ScaleDegree.vSharp;
    case "6b":
      return ScaleDegree.vSharp;
    case "vi":
      return ScaleDegree.vi;
    case "6":
      return ScaleDegree.vi;
    case "vi#":
      return ScaleDegree.viSharp;
    case "6#":
      return ScaleDegree.viSharp;
    case "viib":
      return ScaleDegree.viSharp;
    case "7b":
      return ScaleDegree.viSharp;
    case "vii":
      return ScaleDegree.vii;
    case "7":
      return ScaleDegree.vii;
    case "x":
      return ScaleDegree.x;
    case "n":
      return ScaleDegree.n;
    case "r":
      return ScaleDegree.r;
    default:
      return ScaleDegree.n;
  }
}

String digitToRoman(String i) {
  switch (i) {
    case '0': // Jianpu notation for a rest
      return "r";
    case '1':
      return "i";
    case '2':
      return "ii";
    case '3':
      return "iii";
    case '4':
      return "iv";
    case '5':
      return "v";
    case '6':
      return "vi";
    case '7':
      return "vii";
    default:
      return "n";
  }
}

final Parser notePitchClass = char('a') |
    char('b') |
    char('c') |
    char('d') |
    char('e') |
    char('f') |
    char('g');

PitchClass stringToPitchClass(String s) {
  switch (s.toLowerCase()) {
    case "a":
      return PitchClass.a;
    case "a#":
      return PitchClass.aSharp;
    case "bb":
      return PitchClass.aSharp;
    case "b":
      return PitchClass.b;
    case "c":
      return PitchClass.c;
    case "c#":
      return PitchClass.cSharp;
    case "db":
      return PitchClass.cSharp;
    case "d":
      return PitchClass.d;
    case "d#":
      return PitchClass.dSharp;
    case "eb":
      return PitchClass.dSharp;
    case "e":
      return PitchClass.e;
    case "f":
      return PitchClass.f;
    case "f#":
      return PitchClass.fSharp;
    case "gb":
      return PitchClass.fSharp;
    case "g":
      return PitchClass.g;
    case "g#":
      return PitchClass.gSharp;
    case "ab":
      return PitchClass.gSharp;
    case "x":
      return PitchClass.x;
    case "n":
      return PitchClass.n;
    case "r":
      return PitchClass.r;
    default:
      return PitchClass.n;
  }
}

final Parser accidental = char('#') // sharp
    |
    char('b'); // flat

Articulation stringToArticulation(String s) {
  switch (s) {
    case "*":
      return Articulation.ring;
    case ">":
      return Articulation.push;
    case "!":
      return Articulation.choke;
    default:
      return Articulation.normal;
  }
}

final Parser articulation = (char('>') // push
        |
        char('*') // ring
        |
        char('!')) // choke
    .map((s) => stringToArticulation(s));

final Parser chordTone = string('NC')
        .map((s) => ScaleDegreeTone(ScaleDegree.n)) |
    (scaleDegree & accidental.optional()).map((l) {
      if (l.length == 1)
        return ScaleDegreeTone(stringToScaleDegree(l[0]));
      else
        return ScaleDegreeTone(
            stringToScaleDegree(l[0] + (l[1] == null ? "" : l[1])));
    }) |
    (chordPitchClass & accidental.optional()).map((l) {
      return PitchTone(stringToPitchClass(l[0] + (l[1] == null ? "" : l[1])));
    });

final Parser bassTone = (scaleDegree & accidental.optional()).map((l) {
      if (l.length == 1)
        return ScaleDegreeTone(stringToScaleDegree(l[0]));
      else
        return ScaleDegreeTone(
            stringToScaleDegree(l[0] + (l[1] == null ? "" : l[1])));
    }) |
    (chordPitchClass & accidental.optional()).map((l) {
      if (l.length == 1)
        return PitchTone(stringToPitchClass(l[0]));
      else {
        if (l[1] != null)
          return PitchTone(
              stringToPitchClass(l[0] + (l[1] == null ? "" : l[1])));
        else
          return PitchTone(stringToPitchClass(l[0]));
      }
    });

final Parser bass = (char('/') & bassTone).map((l) => l[1]);

ChordKind stringToChordKind(String s) {
  const major = {"maj", "Ma", "Maj", "M"};
  const major6 = {"maj6", "Maj6", "6"};
  const major7 = {"M7", "Maj7", "maj7"};
  const major9 = {"M9", "Maj9", "maj9"};
  const add9 = {"add9", "9"};
  const minor = {"min", "m"};
  const dim = {"dim", "o"};
  const halfDim = {'0', "m7b5"};
  const aug = {"+", "aug"};
  const sus = {"sus", "sus4"};

  if (major.contains(s))
    return ChordKind.maj;
  else if (major6.contains(s))
    return ChordKind.maj6;
  else if (major7.contains(s))
    return ChordKind.maj7;
  else if (major9.contains(s))
    return ChordKind.maj9;
  else if (add9.contains(s))
    return ChordKind.add9;
  else if (minor.contains(s))
    return ChordKind.min;
  else if (dim.contains(s))
    return ChordKind.dim;
  else if (halfDim.contains(s))
    return ChordKind.halfDim;
  else if (aug.contains(s))
    return ChordKind.aug;
  else if (aug.contains(s))
    return ChordKind.aug;
  else if (sus.contains(s))
    return ChordKind.sus;
  else if (s == '7')
    return ChordKind.dom7;
  else if (s == '7#9')
    return ChordKind.sevenSharp9;
  else if (s == "9#5")
    return ChordKind.nineSharp5;
  else if (s == 'm6')
    return ChordKind.min6;
  else if (s == 'm7')
    return ChordKind.min7;
  else if (s == 'm9')
    return ChordKind.min9;
  else if (s == 'sus2')
    return ChordKind.sus2;
  else if (s == 'sus9')
    return ChordKind.sus9;
  else if (s == '11')
    return ChordKind.eleventh;
  else if (s == '13')
    return ChordKind.thirteenth;
  else if (s == '69')
    return ChordKind.sixNine;
  else
    return ChordKind.na;
}

final Parser chordKind = (string('maj7') |
        string('maj6') |
        string('maj9') |
        string('Maj6') |
        string('Maj7') |
        string('Maj9') |
        string('Maj') |
        string('maj') |
        string('Ma') |
        string('69') |
        string('6') |
        string('7#9') |
        string('9#5') |
        string('7') |
        string('M7') |
        string('M9') |
        string('M') |
        string('add9') |
        string('9') |
        string('min') |
        string('m6') |
        string('m7b5') | // half dim
        string('m7') |
        string('m9') |
        string('m') |
        string('dim') |
        string('o') | // alternative dim
        string('0') | // half dim
        string('+') |
        string('aug') |
        string('sus4') |
        string('sus2') |
        string('sus9') |
        string('sus') |
        string('11') |
        string('13'))
    .map((s) => stringToChordKind(s));

Mode stringToMode(String s) {
  switch (s.toLowerCase()) {
    case 'ionian':
      return Mode.major;
    case 'major':
      return Mode.major;
    case 'maj':
      return Mode.major;
    case 'aeolian':
      return Mode.minor;
    case 'minor':
      return Mode.minor;
    case 'm':
      return Mode.minor;
    case 'dorian':
      return Mode.dorian;
    case 'dor':
      return Mode.dorian;
    case 'mixolydian':
      return Mode.mixolydian;
    case 'mix':
      return Mode.mixolydian;
    case 'phrygian':
      return Mode.phrygian;
    case 'phr':
      return Mode.phrygian;
    case 'lydian':
      return Mode.lydian;
    case 'lyd':
      return Mode.lydian;
    case 'locrian':
      return Mode.locrian;
    case 'loc':
      return Mode.locrian;
    default:
      return Mode.major;
  }
}

final Parser chord = (articulation & chordTone & chordKind & bass)
        .map((l) => Chord(l[1], l[2], bass: l[3], articulation: l[0])) |
    (chordTone & chordKind & bass).map((l) => Chord(l[0], l[1], bass: l[2])) |
    (articulation & chordTone & chordKind)
        .map((l) => Chord(l[1], l[2], articulation: l[0])) |
    (articulation & chordTone & bass).map(
        (l) => Chord(l[1], ChordKind.maj, bass: l[2], articulation: l[0])) |
    (articulation & chordTone)
        .map((l) => Chord(l[1], ChordKind.maj, articulation: l[0])) |
    (chordTone & chordKind).map((l) => Chord(l[0], l[1])) |
    (chordTone & bass).map((l) => Chord(l[0], ChordKind.maj, bass: l[1])) |
    chordTone.map((ct) {
      return Chord(ct, ChordKind.maj);
    });

final Parser lyricString =
    (digit() | extChar | punctuation | ws).plus().flatten();

final Parser phrase = lyricString.map((l) => Phrase(l));

final Parser slot =
    (char('[') & chord.optional() & char(']')).map((l) => Slot(l[1]));

// A lyric element is a phrase or slot with optional whitespace
final Parser lyricElement =
    phrase.map((l) => l as LyricElement) | slot.map((l) => l as LyricElement);

// A lyric line is a line of lyric elements
final Parser lyricLine =
    (lyricElement.plus() & whitespaceOrCodeComment).map((l) {
  var result = l[0].cast<LyricElement>();
  result.add(NewLine());
  return result;
});

// These comments are notes for the person
// who enters or maintains the source file
// for the song and not shown to the musicians
// These comments are ignored by the parser.
final Parser codeComment = (ws.star() &
        char('#') &
        (digit() |
                char('{') |
                char('}') |
                char('#') |
                extChar |
                ws |
                punctuation |
                squareBrackets)
            .star() &
        newLine)
    .map((l) => "");

final Parser whitespaceOrCodeComment = newLine | whitespaceWithNL | codeComment;

// tabFieldChar: chars allowed in tabs
final Parser tabChar = digit() | extChar | punctuation | squareBrackets | ws;

final Parser tabString = tabChar.star().flatten();

final Parser tabLine = (tabString & (codeComment | newLine)).map((l) {
  String s = l[0] as String;
  if (s.isNotEmpty)
    return s + "\n";
  else
    return s;
});
