// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/song.dart';
import 'common.dart';

BarLineSymbol stringToBarLineSymbol(String s) {
  switch (s) {
    case "|":
      return BarLineSymbol.standard;
    case "||":
      return BarLineSymbol.double;
    case "|:":
      return BarLineSymbol.beginRepeat;
    case ":|":
      return BarLineSymbol.endRepeat;
    case ":|:":
      return BarLineSymbol.endBeginRepeat;
    case "[":
      return BarLineSymbol.variantRepeat;
    case "|]":
      return BarLineSymbol.end;
    default:
      return BarLineSymbol.standard;
  }
}

final Parser barSymbol = (string('|]') |
        string('||') |
        string('|:') |
        string('|') |
        string(':|:') |
        string(':|') |
        string('['))
    .map((value) => stringToBarLineSymbol(value));

final Parser barLine = (barSymbol & variantLabel.optional()).map((List l) {
  if (l.length == 1 || l[1] == null)
    return BarLine(l[0]);
  else
    return BarLine(l[0], l[1]);
});

final Parser variantLabel =
    (variantLabelElement & (char(',') & variantLabelElement).star()).flatten();

final Parser variantLabelElement = (digit() & char('-') & digit()) | digit();

// Using TextMusic notation for Jianpu
final Parser octave =
    (char('"') | char("'") | char('^') | char(',') | char(':') | char(';'))
        .map((s) {
  switch (s) {
    case "'":
      return Octave.upOne;
    case '"':
      return Octave.upTwo;
    case "^":
      return Octave.upThree;
    case ",":
      return Octave.downOne;
    case ":":
      return Octave.downTwo;
    case ";":
      return Octave.downThree;
    default:
      return Octave.middle;
  }
});

final Parser note = char('z') // ABC notation for a rest
        .map((s) => Note(PitchTone(PitchClass.r), Octave.middle)) |
    char('x').map((s) => Note(PitchTone(PitchClass.x), Octave.middle)) |
    (accidental & notePitchClass & octave).map((l) {
      return Note(PitchTone(stringToPitchClass(l[1] + l[0])), l[2]);
    }) |
    (accidental & notePitchClass).map((l) {
      return Note(PitchTone(stringToPitchClass(l[1] + l[0])), Octave.middle);
    }) |
    (notePitchClass & octave).map((l) {
      return Note(PitchTone(stringToPitchClass(l[0])), l[1]);
    }) |
    notePitchClass
        .map((s) => Note(PitchTone(stringToPitchClass(s)), Octave.middle)) |
    (accidental & digit() & octave).map((l) {
      return Note(ScaleDegreeTone(stringToScaleDegree(l[1] + l[0])), l[2]);
    }) |
    (digit() & octave).map((l) {
      return Note(ScaleDegreeTone(stringToScaleDegree(l[0])), l[1]);
    }) |
    (accidental & digit()).map((l) {
      return Note(
          ScaleDegreeTone(stringToScaleDegree(l[1] + l[0])), Octave.middle);
    }) |
    digit().map((n) {
      return Note(
          ScaleDegreeTone(stringToScaleDegree(digitToRoman(n))), Octave.middle);
    });

final Parser tiedNote = (char('~') & note).map((l) {
  Note n = l[1];
  return Note(n.tone, n.octave, tied: true);
});

NoteLength stringToDuration(s) {
  switch (s) {
    case "---":
      return NoteLength.whole;
    case "--":
      return NoteLength.dottedHalf;
    case "-.":
      return NoteLength.dottedHalf;
    case "-":
      return NoteLength.half;
    case "=.":
      return NoteLength.dottedSixteenth;
    case "=":
      return NoteLength.sixteenth;
    case "_.":
      return NoteLength.dottedEighth;
    case "_":
      return NoteLength.eighth;
    case ".":
      return NoteLength.dottedQuarter;
    case "/":
      return NoteLength.thirtySecond;
    case "/.":
      return NoteLength.dottedThirtySecond;
    case "\\":
      return NoteLength.sixtyFourth;
    case "\\.":
      return NoteLength.dottedSixtyFourth;
    default:
      return NoteLength.quarter;
  }
}

final Parser duration = ((string('---') |
                (ws.optional() & char('-') & ws & char('-') & ws & char('-')))
            .map((s) => "---") |
        (string('--') | (ws.optional() & char('-') & ws & char('-')))
            .map((s) => "--") |
        string('-.') |
        (string('-') | (ws.optional() & char('-'))).map((s) => "-") |
        string('=.') |
        string('=') |
        string('_.') |
        string('_') |
        string('.') |
        string('/.') |
        string('/') |
        string("\\.") |
        string("\\"))
    .map((s) => stringToDuration(s));

// allow beats to be denoted both with slashes and hyphens
final Parser beat = ws.plus() & (char('/') | char('-') | char('-'));

final Parser beats = beat.plus().map((l) {
  switch (l.length) {
    case 0:
      return NoteLength.oneBeat;
    case 1:
      return NoteLength.twoBeats;
    case 2:
      return NoteLength.threeBeats;
    case 3:
      return NoteLength.fourBeats;
    case 4:
      return NoteLength.fiveBeats;
    case 5:
      return NoteLength.sixBeats;
    case 6:
      return NoteLength.sevenBeats;
    case 7:
      return NoteLength.eightBeats;
    case 8:
      return NoteLength.nineBeats;
    case 9:
      return NoteLength.tenBeats;
    case 10:
      return NoteLength.elevenBeats;
    case 11:
      return NoteLength.twelveBeats;
    default:
      return NoteLength.implicit;
  }
});

final Parser chordDuration = duration | beats;

final Parser chordSingularEvent = (chord & chordDuration.optional()).map((l) {
  if (l.length == 2 && l[1] != null) {
    return ChordEvent(l[0], l[1]);
  } else {
    return ChordEvent(l[0], NoteLength.implicit);
  }
});

// final Parser tiedNoteOrNote = tiedNote | note;

final Parser noteEvent = ((tiedNote | note) & duration.optional()).map((l) {
  if (l.length == 2 && l[1] != null)
    return NoteEvent(l[0], l[1]);
  else
    return NoteEvent(l[0], NoteLength.quarter);
});

// final Parser singularEvent = chordSingularEvent | noteEvent;

// Tuplets use the same syntax as in TextMusic
final Parser tuplet = (char('(') &
        ((ws.star() & noteEvent & ws.star()).pick(1)).plus() &
        char(')'))
    .map((l) => Tuplet(l[1].cast<NoteEvent>()));

final Parser chordTuplet = (char('(') &
        ((ws.star() & chordSingularEvent & ws.star()).pick(1)).plus() &
        char(')'))
    .map((l) => Tuplet(l[1].cast<ChordEvent>()));

// Beams are denoted similar to tuplets, but with square brackets
// final Parser beam = (char('[') &
//         ((ws.star() & noteEvent & ws.star()).pick(1)).plus() &
//         char(']'))
//     .map((l) => Beam(l[1].cast<NoteEvent>()));

// final Parser chordBeam = (char('[') &
//         ((ws.star() & chordSingularEvent & ws.star()).pick(1)).plus() &
//         char(']'))
//     .map((l) => Beam(l[1].cast<ChordEvent>()));

final Parser event = (tuplet | noteEvent);
final Parser chordEvent = (chordTuplet | chordSingularEvent);

final Parser measure =
    ((ws.star() & event & ws.star()).pick(1)).plus().map((l) {
  return Measure(l.cast<Event>());
});

final Parser chordMeasure =
    ((ws.star() & chordEvent & ws.star()).pick(1)).plus().map((l) {
  return Measure(l.cast<Event>());
});

final Parser element = barLine | measure | ws;
// (ws.star() & (barLine | measure) & ws.star()).map((l) => l[1]);

final Parser chordElement = barLine | chordMeasure | ws;
// (ws.star() & (barLine | chordMeasure) & ws.star()).map((l) => l[1]);

final Parser chordLine =
    (chordElement.plus() & whitespaceOrCodeComment).map((l) {
  var result = l[0];
  result.add(NewLine());
  return result;
});

final Parser melodyLine = (element.plus() & whitespaceOrCodeComment).map((l) {
  var result = l[0];
  result.add(NewLine());
  return result;
});

final Parser unitNoteLength = string('unit note length:') &
    (ws.star() & digit() & char('/') & digit()).map((l) {
      return ["unit note length", stringToNoteLength(l[1] + '/' + l[3])];
    });

final Parser tempo = (string('tempo:') &
        ws.star() &
        digit().plus().flatten() &
        ws.star() &
        anyChar.star().flatten())
    .map((l) {
  return ["tempo", Tempo(int.parse(l[2]), l[4])];
});

final Parser capo =
    (string('capo:') & ws.star() & digit().plus().flatten()).map((l) {
  return ["capo", int.parse(l[2])];
});

final Parser keyValue = (chordPitchClass &
        accidental.optional() &
        ws.star() &
        letter().star().flatten())
    .map((l) {
  return MusicKey(
      stringToKeyClass(l[0] + (l[1] == null ? '' : l[1])), stringToMode(l[3]));
});

final Parser key = (string('key:') & ws.star() & keyValue).map((l) {
  return ["key", l[2]];
});

final Parser textFieldName = string('title') |
    string('sorttitle') |
    string('subtitle') |
    string('composer') |
    string('lyricist') |
    string('artist') |
    string('copyright') |
    string('origin') |
    string('transcription') |
    string('notes') |
    string('rhythm') |
    string('book') |
    string('album') |
    string('year') |
    string('file') |
    string('source') |
    string('recording') |
    string('duration');

final Parser field =
    (textFieldName & char(':') & ws.star() & anyChar.plus().flatten())
            .map((l) => [l[0], l[3]]) |
        (textFieldName &
                ws.star() &
                char('{') &
                (anyChar | whitespaceWithNL).plus().flatten() &
                char('}'))
            .map((l) => [l[0], l[3]]) |
        meter |
        unitNoteLength |
        tempo |
        key |
        capo;

final Parser fieldLine =
    (whitespaceOrCodeComment.star() & field & whitespaceOrCodeComment)
        .map((l) => l[1]);

final Parser lyricString = (digit() | extChar | punctuation | ws).plus();

final Parser slot =
    (char('[') & chord.optional() & char(']')).map((l) => Slot(l[1]));

// A lyric element is a phrase or slot with optional whitespace
final Parser lyricElement = (phrase).map((l) => l[1] as LyricElement) |
    (ws.star() & slot & ws.star()).map((l) => l[1] as LyricElement);

final Parser chordBlock1 = (string('chords') &
        ws.star() &
        char('{') &
        ws.star() &
        newLine &
        whitespaceOrCodeComment.star() &
        (codeComment.map((l) => []) | chordLine).plus() &
        whitespaceOrCodeComment.star() &
        char('}') &
        ws.star() &
        newLine.optional())
    .map((l) {
  List<MetricElement> result = [];
  for (var i in l[6]) {
    for (var j in i) if (j is MetricElement) result.add(j);
  }
  return result;
});

final Parser chordBlock2 =
    (string('chords:') & ws.star() & chordLine & whitespaceOrCodeComment)
        .map((l) {
  List<MetricElement> result = [];
  for (var j in l[2]) if (j is MetricElement) result.add(j);
  return result;
});

final Parser chordBlock = chordBlock1 | chordBlock2;

final Parser melodyBlock1 = (string('melody') &
        ws.star() &
        char('{') &
        ws.star() &
        newLine &
        whitespaceOrCodeComment.star() &
        (codeComment.map((l) => []) | melodyLine).plus() &
        whitespaceOrCodeComment.star() &
        char('}') &
        ws.star() &
        newLine.optional())
    .map((l) {
  List<MetricElement> result = [];
  for (var i in l[6]) {
    for (var j in i) if (j is MetricElement) result.add(j);
  }
  return result;
});

final Parser melodyBlock2 = (string('melody:') & melodyLine).map((l) {
  List<MetricElement> result = [];
  for (var j in l[1]) if (j is MetricElement) result.add(j);
  return result;
});

final Parser melodyBlock = melodyBlock1 | melodyBlock2;

final Parser lyricBlock1 = (string('lyric') &
        ws.star() &
        char('{') &
        whitespaceOrCodeComment.star() &
        lyricLine.star() &
        whitespaceOrCodeComment.star() &
        char('}') &
        whitespaceOrCodeComment.star())
    .map((l) {
  List<LyricElement> result = [];
  for (var i in l[4]) {
    for (var j in i) if (j is LyricElement) result.add(j);
  }
  return result;
});

final Parser lyricBlock2 =
    (string('lyric:') & lyricLine & whitespaceOrCodeComment.star()).map((l) {
  List<LyricElement> result = [];
  for (var j in l[1]) if (j is LyricElement) result.add(j);
  return result;
});

final Parser lyricBlock = lyricBlock1 | lyricBlock2;

final Parser tabBlock1 = (string('tab') &
        ws.star() &
        char('{') &
        ws.star() &
        newLine &
        tabLine.plus() &
        ws.star() &
        char('}') &
        ws.star() &
        newLine.optional())
    .map((l) {
  var result = "";
  for (var e in l[5]) result = result + e;
  return result;
});

final Parser tabBlock2 = (string('tab:') & tabLine).map((l) => l[1]);

final Parser tabBlock = tabBlock1 | tabBlock2;

// These comments are included in the output
// as directives for the musicians
final Parser comment =
    (string('comment:') & anyChar.star().flatten() & newLine).map((l) => l[1]);

final Parser section = (string('section:') &
        ws.star() &
        (extChar & (digit() | extChar).star()).flatten() &
        ws.star() &
        newLine &
        comment.optional() &
        whitespaceOrCodeComment.star() &
        chordBlock.optional() &
        whitespaceOrCodeComment.star() &
        melodyBlock.optional() &
        whitespaceOrCodeComment.star() &
        lyricBlock.optional() &
        whitespaceOrCodeComment.star() &
        tabBlock.optional() &
        whitespaceOrCodeComment.star())
    .map<Section>((l) => Section(
        name: l[2],
        comment: l[5] != null ? l[5] : "",
        chords: l[7],
        melody: l[9],
        lyric: l[11],
        tab: l[13] != null ? l[13] : ""));

final Parser meter =
    (string('meter:') & ws.star() & digit() & char('/') & digit()).map((l) {
  return ["meter", Meter(int.parse(l[2]), int.parse(l[4]))];
});

final Parser header = fieldLine.plus().map((l) {
  var meta = Meta();
  for (var i in l) {
    if (i == '') continue;
    var name = i[0];
    var value = i[1];
    switch (name) {
      case 'title':
        meta.title = value;
        break;
      case 'sorttitle':
        meta.sortTitle = value;
        break;
      case 'subtitle':
        meta.subtitle = value;
        break;
      case 'composer':
        meta.composer = value;
        break;
      case 'lyricist':
        meta.lyricist = value;
        break;
      case 'artist':
        meta.artist = value;
        break;
      case 'copyright':
        meta.copyright = value;
        break;
      case 'origin':
        meta.origin = value;
        break;
      case 'meter':
        meta.meter = value;
        break;
      case 'unitNoteLength':
        meta.unitNoteLength = value;
        break;
      case 'tempo':
        meta.tempo = value;
        break;
      case 'transcription':
        meta.transcription = value;
        break;
      case 'notes':
        meta.notes = value;
        break;
      case 'key':
        meta.key = value;
        break;
      case 'rhythm':
        meta.rhythm = value;
        break;
      case 'book':
        meta.book = value;
        break;
      case 'album':
        meta.album = value;
        break;
      case 'year':
        meta.year = value;
        break;
      case 'file':
        meta.file = value;
        break;
      case 'source':
        meta.source = value;
        break;
      case 'recording':
        meta.recording = value;
        break;
      case 'duration':
        meta.duration = value;
        break;
      case 'capo':
        meta.capo = value;
        break;
      default:
        continue;
    }
  }
  return meta;
});

final Parser song = (header &
        whitespaceOrCodeComment.star() &
        section.plus() &
        whitespaceOrCodeComment.star())
    .map((l) {
  var meta = l[0];
  List<Section> arrangement = List.from(l[2]);
  return Song(meta, arrangement);
});
