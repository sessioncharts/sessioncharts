// Copyright (c) 2023 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:sessioncharts/src/song.dart';
import 'tone_pair.dart';
import 'characters.dart';
import 'theme.dart';
// import 'util.dart';
// import 'drawings.dart';
import 'package:sessioncharts/src/gui/settings.dart';

TextSpan chordKindSpan(TextStyle ts, ChordKind kind) {
  switch (kind) {
    case ChordKind.maj:
      return TextSpan(style: ts, text: "");
    case ChordKind.maj6:
      return TextSpan(style: ts, text: "6");
    case ChordKind.maj7:
      return TextSpan(style: ts, text: "maj7");
    case ChordKind.maj9:
      return TextSpan(style: ts, text: "maj9");
    case ChordKind.dom7:
      return TextSpan(style: ts, text: "7");
    case ChordKind.add9:
      return TextSpan(style: ts, text: "9");
    case ChordKind.sevenSharp9:
      return TextSpan(style: ts, text: "7${accidentalSharp}9");
    case ChordKind.nineSharp5:
      return TextSpan(style: ts, text: "9${accidentalSharp}5");
    case ChordKind.min:
      return TextSpan(style: ts, text: "m");
    case ChordKind.min6:
      return TextSpan(style: ts, text: "m6");
    case ChordKind.min7:
      return TextSpan(style: ts, text: "m7");
    case ChordKind.min9:
      return TextSpan(style: ts, text: "m9");
    case ChordKind.dim:
      return TextSpan(style: ts, text: "o"); // dim
    case ChordKind.halfDim:
      return TextSpan(style: ts, text: oWithStroke); // m7\u{266D}5");
    case ChordKind.aug:
      return TextSpan(style: ts, text: "+"); // "aug"
    case ChordKind.sus:
      return TextSpan(style: ts, text: "sus");
    case ChordKind.sus2:
      return TextSpan(style: ts, text: "sus2");
    case ChordKind.sus9:
      return TextSpan(style: ts, text: "sus9");
    case ChordKind.sixNine:
      return TextSpan(style: ts, text: "6/9");
    case ChordKind.eleventh:
      return TextSpan(style: ts, text: "11");
    case ChordKind.thirteenth:
      return TextSpan(style: ts, text: "13");
    default:
      return TextSpan(style: ts, text: ""); // major
  }
}

String articulationToMusicChar(Articulation art) {
  switch (art) {
    case Articulation.ring:
      return diamond; // articTenutoAbove;
    case Articulation.push:
      return ">"; // articAccentAbove;
    case Articulation.choke:
      return "∧"; //  articMarcatoStaccatoAbove;
    default:
      return "";
  }
}

Widget chordEventToWidget(
    ChordEvent chordEvent, ThemeData theme, MusicKey key, Style style,
    {bool showDuration = false}) {
  final chord = chordEvent.chord;
  final rootPair =
      toneToTonePair(chord?.tone ?? PitchTone(PitchClass.n), key, style);
  final ts = theme.defaultTextStyle;
  final fontWidth = ts.fontSize;

  var children = <InlineSpan>[]; // accidental, kind and bass

  // Accidental
  if (rootPair.accidental != "") {
    children.add(TextSpan(style: ts, text: "${rootPair.accidental}"));
  }

  // Kind

  children.add(chordKindSpan(ts, chord?.kind ?? ChordKind.na));

  // Bass
  if (chord != null && chord.bass != null) {
    final bassPair = toneToTonePair(chord.bass, key, style);
    children.add(TextSpan(style: ts, text: "/", children: [
      TextSpan(style: ts, text: "${bassPair.root}"),
      TextSpan(style: ts, text: "${bassPair.accidental}")
    ]));
  }

  // add a dot to the right if the rhythm is a dotted note
  // if (showDuration == true && showDot(chordEvent.duration)) {
  //  children.add(TextSpan(style: ts, text: "\u{2022}"));
  // }

  final chordWidget = RichText(
      text: TextSpan(
          style: ts.copyWith(fontWeight: FontWeight.bold),
          text: "${rootPair.root}",
          children: children),
      softWrap: false);

  var column = <Widget>[];

  // If the articulation is not normal, align the chord and articulation
  // in a the column.
  if (chord != null && chord.articulation != Articulation.normal) {
    column.add(SizedBox(
        height: fontWidth! / 2,
        child: RichText(
            text: TextSpan(children: [
          // TextSpan(text: " ", style: ts),
          WidgetSpan(
            child: Transform.translate(
              offset: PdfPoint(0, -fontWidth / 4.5),
              child: Text(
                articulationToMusicChar(chord.articulation),
                style: ts,
                textScaleFactor: 0.7,
              ),
            ),
          )
        ]))));
  }

  column.add(chordWidget);

  // if (showDuration)
  //  column.add(horizontalBarWidget(ts, chordEvent.duration));

  // add white space below the chord to line the chord up with the barlines
  column.add(SizedBox(height: fontWidth! / 10));

  return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: column),
        // To Do: add dots for dotted durations
        // add white space after the chord
        SizedBox(width: fontWidth / 2) // extra right space
      ]);
}

// Format a measure of chords in a chord chart.
// The rhythm is left implicit, if the durations of *all* chords
// the measure are implicit.  Slashes are used if the duration of
// a chord is an even number of beats.  Traditional rhythmic notation
// is used only if subdivisions of a beat are needed.
List<Widget> chordMeasureToWidgets(
    Measure measure, ThemeData theme, MusicKey key, Meter meter, Style style) {
  final ts = theme.defaultTextStyle;
  bool allDurationsImplict() {
    bool implicit(NoteLength nl) {
      return nl == NoteLength.implicit;
    }

    for (var e in measure.events) {
      if (e is ChordEvent && !implicit(e.duration)) {
        return false;
      }
    }
    return true;
  }

  List<Widget> l = [];

  if (allDurationsImplict()) {
    for (var e in measure.events) {
      if (e is ChordEvent) {
        l.add(chordEventToWidget(e, theme, key, style));
      }
    }
    return l;
  }

  // Otherwise denote rhythm using slashes, if possible,
  // or traditional notation, if not.

  final fontSize = ts.fontSize;
  final slash = Container(
      // raise the slash a bit to align its baseline with the text
      padding: EdgeInsets.only(bottom: fontSize! / 10),
      // Add an en-dash character, instead of a slash, to use
      // the same rhythmic notation as in melodies.
      // This also avoids confusion with the slash used for inversions
      child: Text(enDash.padRight(3)));

  // Return the chord followed by n slashes
  List<Widget> slashes(ChordEvent e, int n) {
    List<Widget> l = [chordEventToWidget(e, theme, key, style)];
    for (var i = 0; i < n; i++) {
      l.add(slash);
    }
    return l;
  }

  for (var ce in measure.events) {
    if (ce is ChordEvent) {
      switch (ce.duration) {
        case NoteLength.whole:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 3));
          else // meter.unit == 8
            l.addAll(slashes(ce, 7));
          break;
        case NoteLength.half:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 1));
          else // meter.unit == 8
            l.addAll(slashes(ce, 3));
          break;
        case NoteLength.dottedHalf:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 2));
          else // meter.unit == 8
            l.addAll(slashes(ce, 5));
          break;
        case NoteLength.quarter:
          if (meter.unit == 4)
            l.addAll(slashes(ce, 0));
          else // meter.unit == 8
            l.addAll(slashes(ce, 1));
          break;
        case NoteLength.dottedQuarter:
          if (meter.unit == 4)
            l.add(
                chordEventToWidget(ce, theme, key, style, showDuration: true));
          else // meter.unit == 8
            l.addAll(slashes(ce, 2));
          break;
        case NoteLength.eighth:
          if (meter.unit == 4)
            l.add(
                chordEventToWidget(ce, theme, key, style, showDuration: true));
          else // meter.unit === 8
            l.addAll(slashes(ce, 0));
          break;
        case NoteLength.implicit:
          l.addAll(slashes(ce, 0));
          break;
        case NoteLength.oneBeat:
          l.addAll(slashes(ce, 0));
          break;
        case NoteLength.twoBeats:
          l.addAll(slashes(ce, 1));
          break;
        case NoteLength.threeBeats:
          l.addAll(slashes(ce, 2));
          break;
        case NoteLength.fourBeats:
          l.addAll(slashes(ce, 3));
          break;
        case NoteLength.fiveBeats:
          l.addAll(slashes(ce, 4));
          break;
        case NoteLength.sixBeats:
          l.addAll(slashes(ce, 5));
          break;
        case NoteLength.sevenBeats:
          l.addAll(slashes(ce, 6));
          break;
        case NoteLength.eightBeats:
          l.addAll(slashes(ce, 7));
          break;
        case NoteLength.nineBeats:
          l.addAll(slashes(ce, 8));
          break;
        case NoteLength.tenBeats:
          l.addAll(slashes(ce, 9));
          break;
        case NoteLength.elevenBeats:
          l.addAll(slashes(ce, 10));
          break;
        case NoteLength.twelveBeats:
          l.addAll(slashes(ce, 11));
          break;
        default: // in case the duration is null
          l.add(chordEventToWidget(ce, theme, key, style, showDuration: true));
      }
    }
  }
  return l;
}

Widget barlineToWidget(BarLine bl, ThemeData theme) {
  Widget f(String symbol, [String label = ""]) {
    TextStyle ts = theme.defaultTextStyle;
    TextStyle ms = musicNotationStyle(theme);
    if (label.isEmpty)
      return Text("$symbol".padRight(5), style: ms);
    else
      return RichText(
          text: TextSpan(text: symbol, style: ms, children: [
        TextSpan(
            text: " " + label + " ",
            style: ts.copyWith(fontSize: ts.fontSize! * 0.65)),
        TextSpan(text: "".padRight(5))
      ]));
  }

  switch (bl.kind) {
    case BarLineSymbol.standard:
      return f(barlineSingle);
    case BarLineSymbol.double:
      return f(barlineDouble);
    case BarLineSymbol.beginRepeat:
      return f(repeatLeft);
    case BarLineSymbol.endRepeat:
      return f(repeatRight);
    case BarLineSymbol.endBeginRepeat:
      return f(repeatRightLeft);
    case BarLineSymbol.variantRepeat:
      return f(barlineSingle, bl.label);
    case BarLineSymbol.end:
      return f(barlineFinal);
    default:
      return f(barlineSingle);
  }
}

// chordChart:  Each row represents a line in the chord chart,
// with a item for each barline and each measure in the line.
// Barlines and measures in each line are lined-up.
Widget chordChart(List<MetricElement>? elements, ThemeData theme, MusicKey key,
    Meter meter, Style style) {
  List<Widget> rows = [];
  List<Widget> row = [];
  var compact = settings?.getBool('compact') ?? false;

  if (elements != null)
    for (var e in elements) {
      if (e is BarLine) {
        row.add(barlineToWidget(e, theme));
      } else if (e is Measure) {
        for (var e2 in chordMeasureToWidgets(e, theme, key, meter, style)) {
          row.add(e2);
        }
      } else if (e is NewLine) {
        if (!compact) {
          // Start a new row, unless the presentation is to be compact.
          // Add extra height to increase space between rows;
          rows.add(
              Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: row));
          row = []; // reset for next line
        } else {
          row.add(Text(" $downLeftArrow "));
        }
      }
    }
  if (compact) {
    // If the presentation is compact, all the chords and barlines are in one row
    rows.add(Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: row));
  }

  return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: rows);
}
