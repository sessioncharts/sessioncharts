// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
// import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/src/gui/settings.dart';
import 'chord_chart.dart';
import 'characters.dart';

// Use to display sections with no chord block but with chords
// embedded in the lyrics, as in ChordPro.  Show the chords
// above the lyrics.
Widget chordproWidget(
    List<LyricElement>? lyric, ThemeData theme, MusicKey key, Style style) {
  List<Widget> body = [];
  var compact = settings?.getBool('compact') ?? false;

  // To show the chord chart, build the metric rows, interlacing chords
  // and lyrics, and add them to the body.
  if (lyric != null && lyric.isNotEmpty) {
    List<Widget> lineRow = [];
    for (var i = 0; i < lyric.length; i++) {
      if (lyric[i] is Phrase) {
        Phrase p = lyric[i] as Phrase;
        if (p.string.trim().isNotEmpty) {
          lineRow.add(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(""), Text(p.string.trimLeft())]));
        }
      } else if (lyric[i] is Slot) {
        Slot s = lyric[i] as Slot;
        if (lyric[i + 1] is Phrase) {
          Phrase p = lyric[i + 1] as Phrase;
          i += 1;
          lineRow.add(
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            chordEventToWidget(
                ChordEvent(s.content, NoteLength.implicit), theme, key, style),
            Text(p.string)
          ]));
        } else {
          lineRow.add(
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            chordEventToWidget(
                ChordEvent(s.content, NoteLength.implicit), theme, key, style),
            Text("")
          ]));
        }
      } else if (lyric[i] is NewLine) {
        if (!compact) {
          // lineRow.add(Column(
          //    crossAxisAlignment: CrossAxisAlignment.start,
          //    children: [Text(""), Text(" $downLeftArrow")]));
          body.add(Wrap(
              crossAxisAlignment: WrapCrossAlignment.end,
              children: lineRow)); // List<Widget>.from(lineRow)));
          lineRow = []; // start next row
        } else {
          // compact
          lineRow.add(Text("$downLeftArrow "));
        }
      }
    }
    if (compact) {
      body.add(Wrap(
          crossAxisAlignment: WrapCrossAlignment.end,
          children: lineRow)); // List<Widget>.from(lineRow)));
    }
  }

  return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: body);
}
