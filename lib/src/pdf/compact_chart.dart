// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
// import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/src/text/compact.dart';
// import 'package:flutter/material.dart';

Widget compactChartWidget(List<MetricElement>? chords,
    List<LyricElement>? lyric, MusicKey key, Style style) {
  List<Widget> body = [];

  if (chords != null && chords.isNotEmpty) {
    body.add(Text(metricToString(chords, key, style)));
  }
  if (lyric != null && lyric.isNotEmpty) {
    body.add(Text(slashifyLyric(lyric, showSlots: true)));
  }

  return FittedBox(
      fit: BoxFit.fill,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: body));
}
