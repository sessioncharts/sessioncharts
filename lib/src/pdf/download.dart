// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'dart:io';
import 'dart:convert';
import 'dart:html' as html;
import 'dart:async';
import 'package:pdf/pdf.dart' as pdf;
import 'package:pdf/widgets.dart';
import 'package:sessioncharts/src/song.dart';
import 'theme.dart';
import 'song.dart';
import 'package:path/path.dart' as p;
import 'package:flutter/foundation.dart' show kIsWeb;
// import 'package:path_provider/path_provider.dart';
import 'package:file_saver/file_saver.dart';

// PDF Testing; ToDo: Move this code somewhere else
// And provide a way for the user to customize the view
Future<void> downloadPDF(Song song, String filename,
    {double fontSize = 15.0, Style style = Style.nashville}) async {
  final theme = await makeTheme(fontSize);
  final doc = Document(
      theme: theme,
      title: song.header.title,
      author: contributors(song.header));
  // Set the PDF page format below to be as wide as a4 but 3 times as long
  // i.e. with room for 5 DIN a4 pages but without page breaks
  final cm = pdf.PdfPageFormat.cm;
  final margin = 0.5 * cm;
  doc.addPage(MultiPage(
      pageFormat: pdf.PdfPageFormat.a4.copyWith(
          marginLeft: margin,
          marginRight: margin,
          marginTop: margin,
          marginBottom: margin),
      build: (Context context) {
        return songWidgets(song, theme, style: style);
      })); // Page
  final basename = p.basename(p.withoutExtension(filename));
  final content = base64Encode(await doc.save());
  if (kIsWeb) {
    html.AnchorElement(
        href: "data:application/octet-stream;charset=utf-16le;base64,$content")
      ..setAttribute("download", "$basename.pdf")
      ..click();
  } else {
    // final output = await getTemporaryDirectory();
    // final file = File("${output.path}/$basename.pdf");
    // await file.writeAsBytes(doc.save());
    var bytes = await doc.save();
    // print("bytes size == $bytes.length\n");
    await FileSaver.instance.saveFile(name: "$basename.pdf", bytes: bytes);
  }
}
