// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
// import 'package:flutter/material.dart';
import 'package:sessioncharts/src/song.dart';
import 'package:sessioncharts/src/gui/settings.dart';
import 'characters.dart';

Widget lyricsWidget(
    List<LyricElement>? lyric, ThemeData theme, bool showSlots) {
  List<Widget> rows = [];
  var lyricRow = StringBuffer();
  var compact = settings?.getBool('compact') ?? false;
  final ts = theme.defaultTextStyle;
  if (lyric != null)
    for (var e in lyric) {
      if (e is Phrase) {
        if (e.string.isNotEmpty) {
          lyricRow.write(e.string.trimLeft());
        }
      } else if (e is Slot) {
        if (showSlots) {
          lyricRow.write('_');
        }
      } else if (e is NewLine) {
        if (!compact) {
          rows.add(RichText(
              text: TextSpan(
                  style: ts, // themeManager.theme.textTheme.bodyText2,
                  text: lyricRow.toString()),
              softWrap: true));
          // reset for next line
          lyricRow = StringBuffer();
        } else {
          // compact
          lyricRow.write(downLeftArrow);
        }
      }
    }
  if (compact)
    rows.add(RichText(
        text: TextSpan(
            style: ts, // themeManager.theme.textTheme.bodyText2,
            text: lyricRow.toString()),
        softWrap: true));
  return Column(crossAxisAlignment: CrossAxisAlignment.start, children: rows);
}
