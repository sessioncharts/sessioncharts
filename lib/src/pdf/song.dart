// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:pdf/widgets.dart';
import 'package:sessioncharts/src/song.dart';
import 'section.dart';

String contributors(Meta? m) {
  if (m != null) {
    final subtitle = m.subtitle;
    final composer = m.composer;
    final lyricist = m.lyricist;
    final artist = m.artist;
    var l = [subtitle, composer, lyricist, artist];
    l.removeWhere((item) => item == "");
    return l.join(" / ");
  } else {
    return "";
  }
}

String properties(Meta m) {
  final key = m.key.toString();
  final rhythm = m.rhythm;
  final meter = m.meter.toString();
  final tempo = m.tempo.toString();
  final capo = m.capo == 0 ? "" : "capo: " + m.capo.toString();

  return key +
      (meter == "" ? "" : "; " + meter) +
      (rhythm == "" ? "" : " " + rhythm) +
      (tempo == "" ? "" : "; " + tempo) +
      (capo == "" ? "" : "; " + capo);
}

// The chords, lyrics, melody and tab switches turn on and off the
// display of these parts of the section
List<Widget> songWidgets(Song song, ThemeData theme,
    {bool compact = false,
    bool chords = true,
    bool lyrics = true,
    bool melody = true,
    bool tab = true,
    Style style = Style.letters}) {
  List<Widget> rows = [];
  var sectionIndex = Map();

  int incrementSection(String sectionName) {
    if (sectionIndex.containsKey(sectionName)) {
      sectionIndex[sectionName] = sectionIndex[sectionName] + 1;
    } else {
      sectionIndex[sectionName] = 1;
    }
    return sectionIndex[sectionName];
  }

  Row titleRow() {
    String title = song.header.title;
    String credits = contributors(song.header);
    String s;
    if (credits != "") {
      s = "$title -  $credits";
    } else {
      s = title;
    }
    return Row(children: [Text(s, style: theme.header1)]);
  }

  Row propertiesRow() {
    if (!song.isEmpty())
      return Row(
          children: [Text(properties(song.header), style: theme.header2)]);
    else
      return Row(children: []);
  }

  song.normalizeChords(); // to facilitate trasposition
  song.completeArrangement();

  List<Widget> header = [titleRow(), propertiesRow()];
  if (song.header.notes.isNotEmpty)
    header.add(
        Text(song.header.notes, style: TextStyle(fontStyle: FontStyle.italic)));
  // header
  rows.add(FittedBox(
      fit: BoxFit.fill,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: header)));
  rows.add(Text(" ")); // empty row
  MusicKey key = song.header.key;
  // If the capo is on the nth fret, transpose *down* n steps
  if (song.header.capo != 0) key = key.transpose(-song.header.capo);

  final fontSize = theme.defaultTextStyle.fontSize ?? 12.0;
  final emptyRow = SizedBox(height: fontSize + 2.0);

  for (var section in song.arrangement) {
    rows.add(emptyRow);
    rows.add(sectionWidget(section, theme, incrementSection(section.name), key,
        song.header.meter, style,
        compact: compact,
        chords: chords,
        lyrics: lyrics,
        melody: melody,
        tab: tab));
  }

  /* return Container(
      padding: EdgeInsets.only(right: 20),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: rows));
  */
  return rows;
}

