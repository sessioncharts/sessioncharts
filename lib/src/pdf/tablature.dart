// Copyright (c) 2020-23 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:pdf/widgets.dart';

// Format of tabulatur
Widget tabWidget(String tab) {
  return FittedBox(
      fit: BoxFit.fill,
      child: Text(tab, style: TextStyle(font: Font.courier())));
}
