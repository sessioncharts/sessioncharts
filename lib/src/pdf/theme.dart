// Copyright (c) 2023 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:pdf/widgets.dart';
import 'package:flutter/services.dart' show rootBundle;

// Use the tableCell style of ThemeData for music notation symbols
TextStyle musicNotationStyle(ThemeData td) {
  return td.tableCell;
}

Future<ThemeData> makeTheme([double fontSize = 12.0]) async {
  final bravuraPath = 'fonts/BravuraText.ttf';
  final bravura = Font.ttf(await rootBundle.load(bravuraPath));
  final ts =
      TextStyle.defaultStyle().copyWith(fontSize: fontSize, fontFallback: [bravura]);
  final ms = TextStyle.defaultStyle()
      .copyWith(fontNormal: bravura, fontSize: fontSize);
  return ThemeData(
      defaultTextStyle: ts,
      paragraphStyle: ts,
      header0: ts.copyWith(fontWeight: FontWeight.bold, fontSize: fontSize+4),
      header1: ts.copyWith(fontWeight: FontWeight.bold, fontSize: fontSize+2),
      header2: ts,
      header3: ts,
      header4: ts,
      header5: ts,
      bulletStyle: ts,
      tableHeader: ts,
      tableCell: ms);
}
