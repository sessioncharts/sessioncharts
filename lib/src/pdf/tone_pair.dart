// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';
import 'characters.dart';

// A TonePair represent the parts of the tone
// as strings, so that the parts can be laid out
// by chordEventToWidget.
class TonePair {
  String root = ""; // Roman numeral, Nashville or letter
  String accidental = "";
  TonePair(this.root, [this.accidental = ""]);
}

// A pair of the string representations of the tone and accidental
// of a pitch class. If a sharp or flat is needed, the choice
// depends on the key.
TonePair pitchClassToTonePair(PitchClass pc, MusicKey k) {
  var kt = k.keyType();
  switch (pc) {
    case PitchClass.a:
      return TonePair('A');
    case PitchClass.aSharp:
      if (kt == KeyType.sharp) {
        return TonePair('A', accidentalSharp);
      } else {
        return TonePair('B', accidentalFlat);
      }
    case PitchClass.b:
      return TonePair('B');
    case PitchClass.c:
      return TonePair('C');
    case PitchClass.cSharp:
      if (kt == KeyType.sharp) {
        return TonePair('C', accidentalSharp);
      } else {
        return TonePair('D', accidentalFlat);
      }
    case PitchClass.d:
      return TonePair('D');
    case PitchClass.dSharp:
      if (kt == KeyType.sharp) {
        return TonePair('D', accidentalSharp);
      } else {
        return TonePair('E', accidentalFlat);
      }
    case PitchClass.e:
      return TonePair('E');
    case PitchClass.f:
      return TonePair('F');
    case PitchClass.fSharp:
      if (kt == KeyType.sharp) {
        return TonePair('F', accidentalSharp);
      } else {
        return TonePair('G', accidentalFlat);
      }
    case PitchClass.g:
      return TonePair('G');
    case PitchClass.gSharp:
      if (kt == KeyType.sharp) {
        return TonePair('G', accidentalSharp);
      } else {
        return TonePair('A', accidentalFlat);
      }
    case PitchClass.n:
      return TonePair('NC');
    case PitchClass.x:
      return TonePair('X');
    case PitchClass.r:
      return TonePair('0');
    default:
      return TonePair('');
  }
}

// A pair of the string representations of the tone and accidental
// of a scale degree, using Roman numerals or Nashville numbers,
// depending on the selected style. Prefer flats to sharps when
// using scale degrees
TonePair scaleDegreeToTonePair(ScaleDegree sd, Style style) {
  final accidentalFlat = "\u{266D}"; // "b";
  switch (sd) {
    case ScaleDegree.i:
      if (style == Style.nashville)
        return TonePair('1');
      else
        return TonePair('I');
    case ScaleDegree.iSharp:
      if (style == Style.nashville)
        return TonePair('2', accidentalFlat);
      else
        return TonePair('II', accidentalFlat);
    case ScaleDegree.ii:
      if (style == Style.nashville)
        return TonePair('2');
      else
        return TonePair('II');
    case ScaleDegree.iiSharp:
      if (style == Style.nashville)
        return TonePair('3', accidentalFlat);
      else
        return TonePair('III', accidentalFlat);
    case ScaleDegree.iii:
      if (style == Style.nashville)
        return TonePair('3');
      else
        return TonePair('III');
    case ScaleDegree.iv:
      if (style == Style.nashville)
        return TonePair('4');
      else
        return TonePair('IV');
    case ScaleDegree.ivSharp:
      if (style == Style.nashville)
        return TonePair('5', accidentalFlat);
      else
        return TonePair('V', accidentalFlat);
    case ScaleDegree.v:
      if (style == Style.nashville)
        return TonePair('5');
      else
        return TonePair('V');
    case ScaleDegree.vSharp:
      if (style == Style.nashville)
        return TonePair('6', accidentalFlat);
      else
        return TonePair('VI', accidentalFlat);
    case ScaleDegree.vi:
      if (style == Style.nashville)
        return TonePair('6');
      else
        return TonePair('VI');
    case ScaleDegree.viSharp:
      if (style == Style.nashville)
        return TonePair('7', accidentalFlat);
      else
        return TonePair('VII', accidentalFlat);
    case ScaleDegree.vii:
      if (style == Style.nashville)
        return TonePair('7');
      else
        return TonePair('VII');
    case ScaleDegree.n:
      return TonePair('NC');
    case ScaleDegree.x:
      return TonePair('X');
    case ScaleDegree.r:
      return TonePair('0');
    default:
      return TonePair('');
  }
}

TonePair toneToTonePair(Tone? tone, MusicKey key, Style style,
    {bool lowerCase = false}) {
  if (tone == null) return TonePair("");
  switch (style) {
    case Style.letters:
      if (tone is ScaleDegreeTone) {
        final pc = key.relativeToAbsolute(tone.sd);
        final tp = pitchClassToTonePair(pc, key);
        if (lowerCase)
          return TonePair(tp.root.toLowerCase(), tp.accidental);
        else
          return tp;
      } else if (tone is PitchTone) {
        final tp = pitchClassToTonePair(tone.pc, key);
        if (lowerCase)
          return TonePair(tp.root.toLowerCase(), tp.accidental);
        else
          return tp;
      } else {
        // Since all tones are either scale degrees or pitches, this
        // branch shouldn't be reached.
        return TonePair("");
      }
    case Style.nashville:
      if (tone is ScaleDegreeTone) {
        return scaleDegreeToTonePair(tone.sd, Style.nashville);
      } else if (tone is PitchTone) {
        final sd = key.absoluteToRelative(tone.pc);
        return scaleDegreeToTonePair(sd, Style.nashville);
      } else {
        return TonePair("");
      }
    case Style.roman:
      if (tone is PitchTone) {
        final sd = key.absoluteToRelative(tone.pc);
        return scaleDegreeToTonePair(sd, Style.roman);
      } else if (tone is ScaleDegreeTone) {
        return scaleDegreeToTonePair(tone.sd, Style.roman);
      } else {
        return TonePair("");
      }
    default:
      return TonePair("");
  }
}
