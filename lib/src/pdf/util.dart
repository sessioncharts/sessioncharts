// Copyright (c) 2023 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:pdf/widgets.dart';
import 'package:sessioncharts/src/song.dart';

// show a dot only if the duration is a dotted
// note, except for dotted half notes, which
// are displayed using dashes
bool showDot(NoteLength nl) {
  switch (nl) {
    case NoteLength.dottedQuarter:
      return true;
    case NoteLength.dottedEighth:
      return true;
    case NoteLength.dottedSixteenth:
      return true;
    case NoteLength.dottedThirtySecond:
      return true;
    case NoteLength.dottedSixtyFourth:
      return true;
    default:
      return false;
  }
}

// show durations using horizontal bars
// as in numbered music notation,
// if the duration is less than a quarter note
bool showHorizontalBars(NoteLength nl) {
  switch (nl) {
    case NoteLength.eighth:
      return true;
    case NoteLength.dottedEighth:
      return true;
    case NoteLength.sixteenth:
      return true;
    case NoteLength.dottedSixteenth:
      return true;
    case NoteLength.thirtySecond:
      return true;
    case NoteLength.dottedThirtySecond:
      return true;
    case NoteLength.sixtyFourth:
      return true;
    case NoteLength.dottedSixtyFourth:
      return true;
    default:
      return false;
  }
}

// True if the if the duration is less than a quarter note
bool lessThanQuarterNote(NoteLength nl) {
  switch (nl) {
    case NoteLength.eighth:
      return true;
    case NoteLength.dottedEighth:
      return true;
    case NoteLength.sixteenth:
      return true;
    case NoteLength.dottedSixteenth:
      return true;
    case NoteLength.thirtySecond:
      return true;
    case NoteLength.dottedThirtySecond:
      return true;
    case NoteLength.sixtyFourth:
      return true;
    case NoteLength.dottedSixtyFourth:
      return true;
    default:
      return false;
  }
}

Widget emptyOctaveWidget(TextStyle style) {
  final w = (style.fontSize ?? 12.0) * 0.8;
  final h = w / 5;

  return SizedBox(width: w, height: h * 3);
}
