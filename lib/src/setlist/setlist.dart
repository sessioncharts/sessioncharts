// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:yaml/yaml.dart';
import 'setlist_song.dart';
import 'package:sessioncharts/src/song/music_key.dart';
import 'package:sessioncharts/src/parser/sessioncharts_parser.dart' as lp;

class Setlist {
  String title;
  String description;
  List<SetlistSong> songs;
  Setlist(this.title, this.description, this.songs);

  Setlist.fromMap(YamlMap m)
      : title = "${m['title']}",
        description = "${m['description']}",
        songs = listToSongs(m['songs']);
}

// listToSongs: the list must be a list of maps, one
// map per song, as imported from YAML or JSON
List<SetlistSong> listToSongs(List<dynamic> l) {
  List<SetlistSong> songs = [];
  for (var m in l) {
    assert(m is Map);
    final filename = m['file'];
    var k = m['key'];
    if (k != null) {
      var r = lp.keyValue.parse(m['key']);
      if (r.isSuccess) {
        MusicKey key = r.value;
        songs.add(SetlistSong(filename, key));
      } else
        songs.add(SetlistSong(filename, null));
    } else
      songs.add(SetlistSong(filename, null));
  }
  return songs;
}
