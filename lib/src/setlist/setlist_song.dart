// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';

class SetlistSong {
  String filename = "";
  MusicKey? key; // for transposing songs, null if not changed

  SetlistSong(this.filename, this.key);
}
