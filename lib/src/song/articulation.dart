// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum Articulation { normal, ring, push, choke }

String articulationToString(Articulation art) {
  switch (art) {
    case Articulation.ring:
      return "*";
    case Articulation.push:
      return ">";
    case Articulation.choke:
      return "!";
    default:
      return "";
  }
}
