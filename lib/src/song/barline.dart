// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'barline_symbol.dart';
import 'metric_element.dart';

class BarLine implements MetricElement {
  BarLineSymbol kind;
  String label; // for variant repeats

  BarLine(this.kind, [this.label = ""]);

  String toString() {
    if (kind == BarLineSymbol.variantRepeat) {
      return barLineSymbolToString(kind) + label;
    } else {
      return barLineSymbolToString(kind);
    }
  }
}
