// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum BarLineSymbol {
  standard, // "|"
  double, // "||"
  beginRepeat, // "|:"
  endRepeat, // ":|"
  endBeginRepeat, // ":|:"
  variantRepeat, // "["
  end // final, "|]"
}

String barLineSymbolToString(BarLineSymbol sym) {
  switch (sym) {
    case BarLineSymbol.double:
      return "||";
    case BarLineSymbol.beginRepeat:
      return "|:";
    case BarLineSymbol.endRepeat:
      return ":|";
    case BarLineSymbol.endBeginRepeat:
      return ":|:";
    case BarLineSymbol.variantRepeat:
      return "[";
    case BarLineSymbol.end:
      return "|]";
    default:
      return "|"; // standard
  }
}
