// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'event.dart';
import 'singular_event.dart';

// Beams group singular events but are not tuplets
class Beam implements Event {
  List<SingularEvent> events;

  Beam(this.events);
}
