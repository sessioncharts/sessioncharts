// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'chord_kind.dart';
import 'articulation.dart';
import 'tone.dart';

class Chord {
  Tone tone;
  ChordKind kind;
  Tone? bass; // null if none
  Articulation articulation;

  Chord(this.tone, this.kind,
      {this.bass, this.articulation = Articulation.normal});
}
