// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'singular_event.dart';
import 'chord.dart';
import 'note_length.dart';

class ChordEvent extends SingularEvent {
  Chord? chord;
  ChordEvent(this.chord, NoteLength d) : super(d);
}
