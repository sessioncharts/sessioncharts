// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum ChordKind {
  maj,
  maj6,
  maj7,
  maj9,
  dom7,
  add9,
  sevenSharp9, // the "Hendrix chord"
  nineSharp5,
  min,
  min6,
  min7,
  min9,
  dim,
  halfDim,
  aug,
  sus,
  sus2,
  sus9,
  sixNine,
  eleventh,
  thirteenth,
  na // not applicable
}

String chordKindToString(ChordKind kind) {
  switch (kind) {
    case ChordKind.maj6:
      return "6";
    case ChordKind.maj7:
      return "M7";
    case ChordKind.maj9:
      return "M9";
    case ChordKind.dom7:
      return "7";
    case ChordKind.add9:
      return "9";
    case ChordKind.sevenSharp9:
      return "7#9";
    case ChordKind.nineSharp5:
      return "9#5";
    case ChordKind.min:
      return "m";
    case ChordKind.min6:
      return "m6";
    case ChordKind.min7:
      return "m7";
    case ChordKind.min9:
      return "m9";
    case ChordKind.dim:
      return "dim";
    case ChordKind.halfDim:
      return "0";
    case ChordKind.aug:
      return "+";
    case ChordKind.sus:
      return "sus";
    case ChordKind.sus2:
      return "sus2";
    case ChordKind.sus9:
      return "sus9";
    case ChordKind.sixNine:
      return "6/9";
    case ChordKind.eleventh:
      return "11";
    case ChordKind.thirteenth:
      return "13";
    default:
      return ""; // major
  }
}
