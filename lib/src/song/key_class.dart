// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'pitch_class.dart';

// Similar to PitchClass, but includes
// values for flat keys as well as sharp keys
enum KeyClass {
  a,
  aSharp,
  bFlat,
  b,
  c,
  cSharp,
  dFlat,
  d,
  dSharp,
  eFlat,
  e,
  f,
  fSharp,
  gFlat,
  g,
  gSharp,
  aFlat
}

bool isFlatKey(KeyClass kc) {
  switch (kc) {
    case KeyClass.bFlat:
      return true;
    case KeyClass.dFlat:
      return true;
    case KeyClass.eFlat:
      return true;
    case KeyClass.gFlat:
      return true;
    case KeyClass.aFlat:
      return true;
    default:
      return false;
  }
}

PitchClass keyClassToPitchClass(KeyClass kc) {
  switch (kc) {
    case KeyClass.a:
      return PitchClass.a;
    case KeyClass.aSharp:
      return PitchClass.aSharp;
    case KeyClass.bFlat:
      return PitchClass.aSharp;
    case KeyClass.b:
      return PitchClass.b;
    case KeyClass.c:
      return PitchClass.c;
    case KeyClass.cSharp:
      return PitchClass.cSharp;
    case KeyClass.dFlat:
      return PitchClass.cSharp;
    case KeyClass.d:
      return PitchClass.d;
    case KeyClass.dSharp:
      return PitchClass.dSharp;
    case KeyClass.eFlat:
      return PitchClass.dSharp;
    case KeyClass.e:
      return PitchClass.e;
    case KeyClass.f:
      return PitchClass.f;
    case KeyClass.fSharp:
      return PitchClass.fSharp;
    case KeyClass.gFlat:
      return PitchClass.fSharp;
    case KeyClass.g:
      return PitchClass.g;
    case KeyClass.gSharp:
      return PitchClass.gSharp;
    case KeyClass.aFlat:
      return PitchClass.gSharp;
    default:
      return PitchClass.c;
  }
}

// If flat is true, use a flat key rather than a sharp key
KeyClass pitchClassToKeyClass(PitchClass pc, bool flat) {
  switch (pc) {
    case PitchClass.a:
      return KeyClass.a;
    case PitchClass.aSharp:
      return flat ? KeyClass.bFlat : KeyClass.aSharp;
    case PitchClass.b:
      return KeyClass.b;
    case PitchClass.c:
      return KeyClass.c;
    case PitchClass.cSharp:
      return flat ? KeyClass.dFlat : KeyClass.cSharp;
    case PitchClass.d:
      return KeyClass.d;
    case PitchClass.dSharp:
      return flat ? KeyClass.eFlat : KeyClass.dSharp;
    case PitchClass.e:
      return KeyClass.e;
    case PitchClass.f:
      return KeyClass.f;
    case PitchClass.fSharp:
      return flat ? KeyClass.gFlat : KeyClass.fSharp;
    case PitchClass.g:
      return KeyClass.g;
    case PitchClass.gSharp:
      return flat ? KeyClass.aFlat : KeyClass.gSharp;
    default:
      return KeyClass.c;
  }
}

String keyClassToString(KeyClass kc) {
  switch (kc) {
    case KeyClass.a:
      return "A";
    case KeyClass.aSharp:
      return "A#";
    case KeyClass.bFlat:
      return "Bb";
    case KeyClass.b:
      return "B";
    case KeyClass.c:
      return "C";
    case KeyClass.cSharp:
      return "C#";
    case KeyClass.dFlat:
      return "Db";
    case KeyClass.d:
      return "D";
    case KeyClass.dSharp:
      return "D#";
    case KeyClass.eFlat:
      return "Eb";
    case KeyClass.e:
      return "E";
    case KeyClass.f:
      return "F";
    case KeyClass.fSharp:
      return "F#";
    case KeyClass.gFlat:
      return "Gb";
    case KeyClass.g:
      return "G";
    case KeyClass.gSharp:
      return "G#";
    case KeyClass.aFlat:
      return "Ab";
    default:
      return "C";
  }
}

// Transpose up (if positive) or down (if negative) the given
// number of half steps. If the key change is out of range, return the
// input keyClass unchanged.
KeyClass transposeKeyClass(KeyClass keyClass, int halfSteps) {
  final i = pitchClassIndex(keyClassToPitchClass(keyClass));
  var j = i + halfSteps;
  if (j < 1) {
    j += 12;
  } else if (j > 12) {
    j -= 12;
  }
  if (j > 0 && j < 13) {
    return pitchClassToKeyClass(indexToPitchClass(j), isFlatKey(keyClass));
  }
  // Out of range. Return the input KeyClass unchanged.
  return keyClass;
}
