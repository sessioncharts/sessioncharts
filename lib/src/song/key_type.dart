// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum KeyType { natural, sharp, flat }
