// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// LyricElement = Phrase | Slot | NewLine
abstract class LyricElement {}
