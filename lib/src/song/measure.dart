// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'event.dart';
import 'metric_element.dart';

class Measure implements MetricElement {
  List<Event> events;

  Measure(this.events);
}
