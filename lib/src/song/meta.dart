// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'meter.dart';
import 'tempo.dart';
import 'music_key.dart';
import 'note_length.dart';
import 'key_class.dart';
import 'mode.dart';

class Meta {
  String title = "";
  String sortTitle = "";
  String subtitle = "";
  String composer = "";
  String lyricist = "";
  String artist = "";
  String copyright = "";
  String origin = "";
  Meter meter = Meter(4, 4);
  NoteLength unitNoteLength = NoteLength.eighth;
  Tempo tempo = Tempo(120);
  String transcription = "";
  String notes = ""; // comments, instructions, etc.
  MusicKey key = MusicKey(KeyClass.c, Mode.major);
  String rhythm = ""; // hornpipe, jig, etc.
  String book = "";
  String album = ""; // discography in ABC;
  String year = "";
  String file = ""; // filename or path
  String source = "";
  String recording = ""; // a URL
  String duration = ""; // from ChordPro spec
  int capo = 0; // from ChordPro
}
