// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

class Meter {
  int beats;
  int unit;

  Meter(this.beats, this.unit);

  @override
  String toString() {
    return beats.toString() + "/" + unit.toString();
  }
}
