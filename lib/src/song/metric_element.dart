// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// MetricElement = Measure | BarLine | NewLine
// For chords and melodies
abstract class MetricElement {}
