// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum Mode {
  major, // ionian
  dorian,
  phrygian,
  lydian,
  mixolydian,
  minor, // aeolian
  locrian
}

// return the index of the mode
// on a chromatic scale, where the
// major mode has the index 0
int modeIndex(Mode m) {
  switch (m) {
    case Mode.major:
      return 0;
    case Mode.dorian:
      return 2;
    case Mode.phrygian:
      return 4;
    case Mode.lydian:
      return 5;
    case Mode.mixolydian:
      return 7;
    case Mode.minor:
      return 9;
    case Mode.locrian:
      return 11;
    default:
      return 0;
  }
}

// The interval, in half steps, between two modes.
// The result may be negative.
int modeDelta(Mode m1, Mode m2) {
  return modeIndex(m1) - modeIndex(m2);
}

String modeToString(Mode m) {
  switch (m) {
    case Mode.major:
      return "major";
    case Mode.dorian:
      return "dorian";
    case Mode.phrygian:
      return "phrygian";
    case Mode.lydian:
      return "lydian";
    case Mode.mixolydian:
      return "mixolydian";
    case Mode.minor:
      return "minor";
    case Mode.locrian:
      return "locrian";
    default:
      return "major";
  }
}
