// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'key_class.dart';
import 'pitch_class.dart';
import 'mode.dart';
import 'key_type.dart';
import 'scale_degree.dart';

class MusicKey {
  final KeyClass keyClass;
  PitchClass root;
  final Mode mode;

  MusicKey(this.keyClass, [this.mode = Mode.major, this.root = PitchClass.c]) {
    root = keyClassToPitchClass(this.keyClass);
  }

  @override
  String toString() {
    return keyClassToString(keyClass) + " " + modeToString(mode);
  }

  // Transpose up (if positive) or down (if negative) the given
  // number of half steps
  MusicKey transpose(int halfSteps) {
    return MusicKey(transposeKeyClass(keyClass, halfSteps), mode);
  }

  bool isFlatKey() {
    switch (this.keyClass) {
      case KeyClass.aFlat:
        return true;
      case KeyClass.bFlat:
        return true;
      case KeyClass.dFlat:
        return true;
      case KeyClass.eFlat:
        return true;
      case KeyClass.gFlat:
        return true;
      default:
        return false;
    }
  }

  // Returns the relative major of this key.  For example,
  // the relative major of Am is C major and the relative
  // major of C mixolydian is F major.  If the relative
  // major needs an accidental, it is chosen to use the same
  // sharp or flat accidental as the minor key.
  MusicKey relativeMajor() {
    if (mode == Mode.major) return this;
    final i = pitchClassIndex(root);
    var j = i - modeDelta(mode, Mode.major);
    // if j is less than 1, move it up an octave
    if (j < 1) {
      j = j + 12;
    }
    switch (j) {
      case 1:
        return MusicKey(KeyClass.a);
      case 2:
        return isFlatKey()
            ? MusicKey(KeyClass.bFlat)
            : MusicKey(KeyClass.aSharp);
      case 3:
        return MusicKey(KeyClass.b);
      case 4:
        return MusicKey(KeyClass.c);
      case 5:
        return isFlatKey()
            ? MusicKey(KeyClass.dFlat)
            : MusicKey(KeyClass.cSharp);
      case 6:
        return MusicKey(KeyClass.d, Mode.major);
      case 7:
        return isFlatKey()
            ? MusicKey(KeyClass.eFlat)
            : MusicKey(KeyClass.dSharp);
      case 8:
        return MusicKey(KeyClass.e, Mode.major);
      case 9:
        return MusicKey(KeyClass.f, Mode.major);
      case 10:
        return isFlatKey()
            ? MusicKey(KeyClass.gFlat)
            : MusicKey(KeyClass.aSharp);
      case 11:
        return MusicKey(KeyClass.g, Mode.major);
      case 12:
        return isFlatKey()
            ? MusicKey(KeyClass.aFlat)
            : MusicKey(KeyClass.gSharp);
      default:
        return this;
    }
  }

  // keyType: Used to determine whether pitch classes in the key
  // should be named using sharps or flats.  The key type
  // is determined solely by the how the key class of the key,
  // independent of the mode.
  KeyType keyType() {
    switch (keyClass) {
      case KeyClass.a:
        return KeyType.sharp;
      case KeyClass.aSharp:
        return KeyType.sharp;
      case KeyClass.bFlat:
        return KeyType.flat;
      case KeyClass.b:
        return KeyType.sharp;
      case KeyClass.c:
        return KeyType.natural; // only C major has no sharps or flats
      case KeyClass.cSharp:
        return KeyType.sharp;
      case KeyClass.dFlat:
        return KeyType.flat;
      case KeyClass.d:
        return KeyType.sharp;
      case KeyClass.dSharp:
        return KeyType.sharp;
      case KeyClass.eFlat:
        return KeyType.flat;
      case KeyClass.e:
        return KeyType.sharp;
      case KeyClass.f:
        return KeyType.flat;
      case KeyClass.fSharp:
        return KeyType.sharp;
      case KeyClass.gFlat:
        return KeyType.flat;
      case KeyClass.g:
        return KeyType.sharp;
      case KeyClass.gSharp:
        return KeyType.sharp;
      case KeyClass.aFlat:
        return KeyType.flat;
      default:
        return KeyType.natural;
    }
  }

  // Maps a pitch class to a scale degree relative to
  // this key.  Example: In the key of C major, D dorian and A minor a
  // G# would be vSharp, since the scale degree is always relative to the major
  // mode, C here.
  ScaleDegree absoluteToRelative(PitchClass pc) {
    switch (pc) {
      case PitchClass.n:
        return ScaleDegree.n;
      case PitchClass.r:
        return ScaleDegree.r;
      case PitchClass.x:
        return ScaleDegree.x;
      default:
        break;
    }
    var pci = pitchClassIndex(pc);
    final rm = relativeMajor();
    final rmi = pitchClassIndex(rm.root);
    // if the index of the pitch class is less than
    // the index of the pitch class of the root of the
    // relative major key, reset it an octave higher
    if (pci < rmi) pci = pci + 12;
    final interval = pci - rmi;

    switch (interval) {
      case 0:
        return ScaleDegree.i;
      case 1:
        return ScaleDegree.iSharp;
      case 2:
        return ScaleDegree.ii;
      case 3:
        return ScaleDegree.iiSharp;
      case 4:
        return ScaleDegree.iii;
      case 5:
        return ScaleDegree.iv;
      case 6:
        return ScaleDegree.ivSharp;
      case 7:
        return ScaleDegree.v;
      case 8:
        return ScaleDegree.vSharp;
      case 9:
        return ScaleDegree.vi;
      case 10:
        return ScaleDegree.viSharp;
      case 11:
        return ScaleDegree.vii;
      default:
        return ScaleDegree.n;
    }
  }

  // Converts relative to parallel scale degrees.
  // For example, in the key of C mixolydian, the note
  // C has the relative scale degree of v (8) but the
  // parallel scale degree of i (1).
  ScaleDegree relativeToParallel(ScaleDegree sd) {
    final i = scaleDegreeIndex(sd);
    final d = modeDelta(mode, Mode.major);
    return indexToScaleDegree(i - d);
  }

  // Returns the pitch class of a scale degree in this
  // key.
  PitchClass relativeToAbsolute(ScaleDegree sd) {
    switch (sd) {
      case ScaleDegree.n:
        return PitchClass.n;
      case ScaleDegree.r:
        return PitchClass.r;
      case ScaleDegree.x:
        return PitchClass.x;
      default:
        break;
    }

    final ki = pitchClassIndex(root) - 1;
    var i = ki + scaleDegreeIndex(sd);
    final delta = modeDelta(mode, Mode.major);
    var n = i - delta;

    if (n <= 0) {
      // If i is less than 0, reset it an octave higher.
      n = n + 12;
    } else if (n > 12) {
      // If I is greater than 12, reset it an octave lower.
      n = n - 12;
    }
    return indexToPitchClass(n);
  }
}
