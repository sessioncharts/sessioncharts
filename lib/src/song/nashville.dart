// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// Roman representation of chords,
// with translation to and from nashville numbers

enum Roman {
  i,
  ii,
  iii,
  iv,
  v,
  vi,
  vii,
  nc // no chord
}

// extension on Roman {
//   int get nashville {
//     switch (this) {
//       case Roman.i: return 1;
//       case Roman.ii: return 2;
//       case Roman.iii: return 3;
//       case Roman.iv: return 4;
//       case Roman.v: return 5;
//       case Roman.vi: return 6;
//       case Roman.vii: return 7;
//       default: return 0;  // no chord
//     }
//   }

//   String get string {
//     switch (this) {
//       case Roman.i: return 'I';
//       case Roman.ii: return 'II';
//       case Roman.iii: return 'III';
//       case Roman.iv: return 'IV';
//       case Roman.v: return 'V';
//       case Roman.vi: return 'VI';
//       case Roman.vii: return 'VII';
//       default: return 'NC';
//     }
//   }
// }

// Roman nashvilleToRoman (int i) {
//   switch (i) {
//     case 1: return Roman.i;
//     case 2: return Roman.ii;
//     case 3: return Roman.iii;
//     case 4: return Roman.iv;
//     case 5: return Roman.v;
//     case 6: return Roman.vi;
//     case 7: return Roman.vii;
//     default: return Roman.nc;
//   }
// }
