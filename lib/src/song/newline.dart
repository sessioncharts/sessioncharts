// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'metric_element.dart';
import 'lyric_element.dart';

class NewLine implements MetricElement, LyricElement {
  const NewLine();
}
