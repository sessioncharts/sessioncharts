// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'octave.dart';
import 'tone.dart';

class Note {
  Tone tone;
  Octave octave;
  bool tied; // tied to previous note

  Note(this.tone, this.octave, {this.tied = false});
}
