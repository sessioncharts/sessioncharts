// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'singular_event.dart';
import 'note.dart';
import 'note_length.dart';

class NoteEvent extends SingularEvent {
  Note note;

  NoteEvent(this.note, NoteLength d) : super(d);

  // @override
  // String toString() {
  //   return note.toString() + noteLengthToString(duration);
  // }

}
