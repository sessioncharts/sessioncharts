// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// NoteLength: absolute (e.g quarter note), relative (e.g. two beats) or implicit
enum NoteLength {
  whole,
  half,
  dottedHalf,
  quarter,
  dottedQuarter,
  eighth,
  dottedEighth,
  sixteenth,
  dottedSixteenth,
  thirtySecond,
  dottedThirtySecond,
  sixtyFourth,
  dottedSixtyFourth,
  oneBeat,
  twoBeats,
  threeBeats,
  fourBeats,
  fiveBeats,
  sixBeats,
  sevenBeats,
  eightBeats,
  nineBeats,
  tenBeats,
  elevenBeats,
  twelveBeats,
  implicit
}

NoteLength stringToNoteLength(String s) {
  switch (s) {
    case '1':
      return NoteLength.whole;
    case '1/1':
      return NoteLength.whole;
    case '1/2':
      return NoteLength.half;
    case '1/2.':
      return NoteLength.dottedHalf;
    case '1/4':
      return NoteLength.quarter;
    case '1/4.':
      return NoteLength.dottedQuarter;
    case '1/8':
      return NoteLength.eighth;
    case '1/8.':
      return NoteLength.dottedEighth;
    case '1/16':
      return NoteLength.sixteenth;
    case '1/16.':
      return NoteLength.dottedSixteenth;
    case '1/32':
      return NoteLength.thirtySecond;
    case '1/32.':
      return NoteLength.dottedThirtySecond;
    case '1/64':
      return NoteLength.sixtyFourth;
    case '1/64.':
      return NoteLength.dottedSixtyFourth;
    default:
      return NoteLength.implicit;
  }
}

String noteLengthToString(NoteLength nl) {
  switch (nl) {
    case NoteLength.whole:
      return '1/1';
    case NoteLength.half:
      return '1/2';
    case NoteLength.dottedHalf:
      return '1/2.';
    case NoteLength.quarter:
      return '1/4';
    case NoteLength.dottedQuarter:
      return '1/4.';
    case NoteLength.eighth:
      return '1/8';
    case NoteLength.dottedEighth:
      return '1/8.';
    case NoteLength.sixteenth:
      return '1/16';
    case NoteLength.dottedSixteenth:
      return '1/16.';
    case NoteLength.thirtySecond:
      return '1/32';
    case NoteLength.dottedThirtySecond:
      return '1/32.';
    case NoteLength.sixtyFourth:
      return '1/64';
    case NoteLength.dottedSixtyFourth:
      return '1/64.';
    case NoteLength.implicit:
      return '';
    default:
      return '';
  }
}
