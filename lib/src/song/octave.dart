// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

// five octaves
enum Octave { downThree, downTwo, downOne, middle, upOne, upTwo, upThree }

bool octaveAboveMiddle(Octave octave) {
  switch (octave) {
    case Octave.upOne:
      return true;
    case Octave.upTwo:
      return true;
    case Octave.upThree:
      return true;
    default:
      return false;
  }
}

bool octaveBelowMiddle(Octave octave) {
  switch (octave) {
    case Octave.downOne:
      return true;
    case Octave.downTwo:
      return true;
    case Octave.downThree:
      return true;
    default:
      return false;
  }
}
