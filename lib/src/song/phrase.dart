// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'lyric_element.dart';

class Phrase implements LyricElement {
  String string = "";

  Phrase(this.string);
}
