// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'music_key.dart';
import 'key_type.dart';

enum PitchClass {
  a,
  aSharp, // a sharp, b flat
  b,
  c,
  cSharp, // c sharp, d flat
  d,
  dSharp, // d sharp, e flat
  e,
  f,
  fSharp, // f sharp, g flat
  g,
  gSharp, // g sharp, a flat
  x, // rhythm beat
  n, // none, used for no chord and unspecified
  r // rest
}

// Maps pitch classes to integers in the range
// of 0-12, where 0 is used for x,n, and r.
// Useful, e.g., for transposing and finding relative
// major and minor keys.
int pitchClassIndex(PitchClass pc) {
  switch (pc) {
    case PitchClass.a:
      return 1;
    case PitchClass.aSharp:
      return 2;
    case PitchClass.b:
      return 3;
    case PitchClass.c:
      return 4;
    case PitchClass.cSharp:
      return 5;
    case PitchClass.d:
      return 6;
    case PitchClass.dSharp:
      return 7;
    case PitchClass.e:
      return 8;
    case PitchClass.f:
      return 9;
    case PitchClass.fSharp:
      return 10;
    case PitchClass.g:
      return 11;
    case PitchClass.gSharp:
      return 12;
    default:
      return 0;
  }
}

PitchClass indexToPitchClass(int i) {
  final m = {
    1: PitchClass.a,
    2: PitchClass.aSharp,
    3: PitchClass.b,
    4: PitchClass.c,
    5: PitchClass.cSharp,
    6: PitchClass.d,
    7: PitchClass.dSharp,
    8: PitchClass.e,
    9: PitchClass.f,
    10: PitchClass.fSharp,
    11: PitchClass.g,
    12: PitchClass.gSharp
  };
  if (i > 0 && i < 13)
    return m[i]!;
  else
    return PitchClass.n; // unspecified
}

// A textual representation of a pitch class, using capital
// letters, such as "C#" or "Bb".  If a sharp or flat
// is needed, the choice depends on the key.
String pitchClassToString(PitchClass pc, MusicKey k,
    {bool prefixAccidental = true}) {
  var kt = k.keyType();
  switch (pc) {
    case PitchClass.a:
      return 'A';
    case PitchClass.aSharp:
      if (kt == KeyType.sharp) {
        return prefixAccidental ? '#A' : 'A#';
      } else {
        return prefixAccidental ? 'bB' : 'Bb';
      }
    case PitchClass.b:
      return 'B';
    case PitchClass.c:
      return 'C';
    case PitchClass.cSharp:
      if (kt == KeyType.sharp) {
        return prefixAccidental ? '#C' : 'C#';
      } else {
        return prefixAccidental ? 'bD' : 'Db';
      }
    case PitchClass.d:
      return 'D';
    case PitchClass.dSharp:
      if (kt == KeyType.sharp) {
        return prefixAccidental ? '#D' : 'D#';
      } else {
        return prefixAccidental ? 'bE' : 'Eb';
      }
    case PitchClass.e:
      return 'E';
    case PitchClass.f:
      return 'F';
    case PitchClass.fSharp:
      if (kt == KeyType.sharp) {
        return prefixAccidental ? '#F' : 'F#';
      } else {
        return prefixAccidental ? 'bG' : 'Gb';
      }
    case PitchClass.g:
      return 'G';
    case PitchClass.gSharp:
      if (kt == KeyType.sharp) {
        return prefixAccidental ? '#G' : 'G#';
      } else {
        return prefixAccidental ? 'bA' : 'Ab';
      }
    case PitchClass.n:
      return 'NC';
    case PitchClass.r:
      return 'R';
    case PitchClass.x:
      return 'X';
    default:
      return '';
  }
}
