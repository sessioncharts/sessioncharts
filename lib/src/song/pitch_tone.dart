// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'pitch_class.dart';
import 'tone.dart';

class PitchTone implements Tone {
  final PitchClass pc;
  PitchTone(this.pc);
}
