// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum ScaleDegree {
  i,
  iSharp, // i sharp, ii flat
  ii,
  iiSharp, // ii sharp, iii flat
  iii,
  iv,
  ivSharp, // iv sharp, v flat
  v,
  vSharp, // v sharp, vi flat
  vi,
  viSharp, // vi sharp, vii flat
  vii,
  x, // rhythmn beat
  n, // none, used for no chord and unspecified
  r // rest
}

int scaleDegreeIndex(ScaleDegree sd) {
  switch (sd) {
    case ScaleDegree.i:
      return 1;
    case ScaleDegree.iSharp:
      return 2;
    case ScaleDegree.ii:
      return 3;
    case ScaleDegree.iiSharp:
      return 4;
    case ScaleDegree.iii:
      return 5;
    case ScaleDegree.iv:
      return 6;
    case ScaleDegree.ivSharp:
      return 7;
    case ScaleDegree.v:
      return 8;
    case ScaleDegree.vSharp:
      return 9;
    case ScaleDegree.vi:
      return 10;
    case ScaleDegree.viSharp:
      return 11;
    case ScaleDegree.vii:
      return 12;
    default:
      return 0; // for x, n and r
  }
}

ScaleDegree indexToScaleDegree(int i) {
  switch (i) {
    case 1:
      return ScaleDegree.i;
    case 2:
      return ScaleDegree.iSharp;
    case 3:
      return ScaleDegree.ii;
    case 4:
      return ScaleDegree.iiSharp;
    case 5:
      return ScaleDegree.iii;
    case 6:
      return ScaleDegree.iv;
    case 7:
      return ScaleDegree.ivSharp;
    case 8:
      return ScaleDegree.v;
    case 9:
      return ScaleDegree.vSharp;
    case 10:
      return ScaleDegree.vi;
    case 11:
      return ScaleDegree.viSharp;
    case 12:
      return ScaleDegree.vii;
    default:
      return ScaleDegree.n;
  }
}

// scaleDegreeToRoman: if flats is true, use flat accidentals
String scaleDegreeToRoman(ScaleDegree sd, {bool flats = true}) {
  switch (sd) {
    case ScaleDegree.i:
      return "I";
    case ScaleDegree.iSharp:
      return flats ? "IIb" : "I#";
    case ScaleDegree.ii:
      return "II";
    case ScaleDegree.iiSharp:
      return flats ? "IIIb" : "II#";
    case ScaleDegree.iii:
      return "III";
    case ScaleDegree.iv:
      return "IV";
    case ScaleDegree.ivSharp:
      return flats ? "Vb" : "IV#";
    case ScaleDegree.v:
      return "V";
    case ScaleDegree.vSharp:
      return flats ? "VIb" : "V#";
    case ScaleDegree.vi:
      return "VI";
    case ScaleDegree.viSharp:
      return flats ? "VIIb" : "VI#";
    case ScaleDegree.vii:
      return "VII";
    case ScaleDegree.x:
      return "X";
    case ScaleDegree.n:
      return "NC";
    case ScaleDegree.r:
      return "R";
    default:
      return "N";
  }
}

// scaleDegreeToRoman: if prefixAccidental is true, put accidentals before the
// scale degree; if flats is true, use flat accidentals
String scaleDegreeToNashville(ScaleDegree sd,
    {bool prefixAccidental = true, bool flats = true}) {
  final pa = prefixAccidental;
  switch (sd) {
    case ScaleDegree.i:
      return "1";
    case ScaleDegree.iSharp:
      if (flats)
        return pa ? 'b2' : '2b';
      else
        return pa ? '#1' : "1#";
    case ScaleDegree.ii:
      return "2";
    case ScaleDegree.iiSharp:
      if (flats)
        return pa ? 'b3' : '3b';
      else
        return pa ? "#2" : "2#";
    case ScaleDegree.iii:
      return "3";
    case ScaleDegree.iv:
      return "4";
    case ScaleDegree.ivSharp:
      if (flats)
        return pa ? 'b5' : '5b';
      else
        return pa ? "#4" : "4#";
    case ScaleDegree.v:
      return "5";
    case ScaleDegree.vSharp:
      if (flats)
        return pa ? 'b6' : '6b';
      else
        return pa ? "#5" : "5#";
    case ScaleDegree.vi:
      return "6";
    case ScaleDegree.viSharp:
      if (flats)
        return pa ? 'b7' : '7b';
      else
        return pa ? "#6" : "6#";
    case ScaleDegree.vii:
      return "7";
    case ScaleDegree.x:
      return "X";
    case ScaleDegree.n:
      return "NC";
    case ScaleDegree.r:
      return "0";
    default:
      return "N";
  }
}
