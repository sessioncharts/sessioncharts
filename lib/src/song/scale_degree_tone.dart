// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'scale_degree.dart';
import 'tone.dart';

class ScaleDegreeTone implements Tone {
  final ScaleDegree sd;
  ScaleDegreeTone(this.sd);

  @override
  String toString() {
    return scaleDegreeToRoman(sd, flats: true);
  }
}
