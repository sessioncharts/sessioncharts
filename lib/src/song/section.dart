// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'metric_element.dart';
import 'lyric_element.dart';
import 'slot.dart';

class Section {
  // All these instance variables, except name, are nullable.
  // If null replace with values from a template if available.
  // But if empty override the default value with the empty value.
  String name = "";
  String? comment;
  List<MetricElement>? chords;
  List<MetricElement>? melody;
  List<LyricElement>? lyric;
  String tab = ""; // tablature

  Section(
      {this.name = "",
      this.comment = "",
      this.chords,
      this.melody,
      this.lyric,
      this.tab = ""});

  bool lyricHasChords() {
    if (lyric == null || lyric!.isEmpty) {
      return false;
    }
    for (var e in lyric!) {
      if (e is Slot) {
        return true;
      }
    }
    return false;
  }

  bool lyricHasSlots() {
    if (lyric == null || lyric!.isEmpty) {
      return false;
    }
    for (var e in lyric!) {
      if (e is Slot) {
        return true;
      }
    }
    return false;
  }
}
