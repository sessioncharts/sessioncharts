// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'note_length.dart';
import 'event.dart';

abstract class SingularEvent extends Event {
  NoteLength duration = NoteLength.implicit;

  SingularEvent(this.duration);
}
