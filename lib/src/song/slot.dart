// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'lyric_element.dart';
import 'chord.dart';

// A Slot holds the text describing a chord in a chord chart
// with lyrics, as in the ChordPro format, for example.
class Slot implements LyricElement {
  Chord? content; // null if empty slot

  Slot([this.content]);

  bool isEmpty() {
    return content == null;
  }
}
