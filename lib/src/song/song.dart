// Copyright (c) 2019-2022 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';

class Song {
  Meta header = Meta();
  List<Section> arrangement = [];
  Map<String, Section> templates = Map();

  Song(this.header, this.arrangement);

  Song.empty() {
    this.header = Meta();
    this.arrangement = [];
  }

  // Initialize the templates. The first section
  // with a given name is the template for
  // all sections with this same name.
  void initTemplates() {
    for (var s in this.arrangement) {
      if (s.name.isNotEmpty && !this.templates.containsKey(s.name)) {
        this.templates[s.name] = s;
      }
    }
  }

  bool isEmpty() {
    return this.arrangement == [];
  }

  // Complete each section with information from the templates.
  // First Initialize the templates if empty.
  void completeArrangement() {
    if (this.templates.isEmpty) {
      initTemplates();
    }
    for (var section in this.arrangement) {
      if (section.name.isEmpty) continue;
      var template = this.templates[section.name];
      if (template != null) {
        if (section.comment == null) {
          section.comment = template.comment;
        } 
        if (section.chords == null) {
          section.chords = template.chords;
        }
        if (section.melody == null) {
          section.melody = template.melody;
        }
        if (section.lyric == null) {
          section.lyric = template.lyric;
        }
        section.tab = template.tab;
      }
    }
  }

  // normalizeChords: Represent all chord tones as relative scale degrees,
  // to make it easier to transpose the key of the song; Chords
  // in lyrics blocks are also normalized.
  void normalizeChords() {
    for (var section in arrangement) {
      if (section.chords != null) {
        for (var e in section.chords!) {
          if (e is BarLine || e is NewLine) {
          } else if (e is Measure) {
            for (var ce in e.events) {
              if (ce is ChordEvent) {
                if (ce.chord != null) {
                  if (ce.chord!.tone is PitchTone) {
                    PitchTone pt = ce.chord!.tone as PitchTone;
                    ce.chord!.tone =
                        ScaleDegreeTone(header.key.absoluteToRelative(pt.pc));
                  }
                }
              }
            }
          }
        }
      }
      if (section.lyric != null) {
        for (var e in section.lyric!) {
          if (e is Phrase || e is NewLine) {
          } else if (e is Slot) {
            if (e.content != null && e.content is Chord) {
              if (e.content!.tone is ScaleDegreeTone) {
                // The tone is already in normal form
              } else if (e.content!.tone is PitchTone) {
                PitchTone pt = e.content!.tone as PitchTone;
                e.content!.tone =
                    ScaleDegreeTone(header.key.absoluteToRelative(pt.pc));
              }
            }
          }
        }
      }
    }
  }
}
