// Copyright (c) 2020 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

enum Style {
  nashville, // arabic numerals
  roman,
  letters // pitches
}
