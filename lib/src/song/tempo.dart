// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

class Tempo {
  // NoteLength noteLength;
  int bpm; // beats per minute
  String description;

  Tempo(this.bpm, [this.description = '']);

  @override
  String toString() {
    final d = description != "" ? " ($description)" : "";
    return bpm.toString() + " bpm" + d;
  }
}
