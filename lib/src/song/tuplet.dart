// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'event.dart';
import 'singular_event.dart';

// Tuplets generalize triplets to any number of singular events
class Tuplet implements Event {
  List<SingularEvent> events;

  Tuplet(this.events);
}
