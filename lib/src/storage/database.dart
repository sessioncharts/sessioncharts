// Copyright (c) 2020-2022 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'dart:convert';
import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/song.dart' as sng;
import 'package:sessioncharts/src/parser/sessioncharts_parser.dart' as lp;
import 'package:sessioncharts/src/parser/chordpro_parser.dart' as cp;
import 'package:sessioncharts/src/setlist/setlist.dart';
import 'package:sessioncharts/src/constants.dart';
import 'package:yaml/yaml.dart';
import 'package:hive/hive.dart';
// import 'dart:js' as js;

final collection = BoxCollection.open(
  'SessionChartsBoxes', // Name of your database
  {'setlists', 'songs'}, // Names of your boxes
  // path: './', // Path where to store your boxes (Only used in Flutter / Dart IO)
  // key: HiveCipher(), // Key to encrypt your boxes (Only used in Flutter / Dart IO)
);

// final setlistBox = collection.openBox('setlists'); // filename -> content
// final songBox = collection.openBox('songs'); // filename -> content

List<Setlist> setlists = [];
List<sng.Meta> songIndex = [];
int currentSetlist =
    0; // index of the current set list in setlist. If null, use all songs
int currentSong = 0; // index of current song in the set list or all songs

Future<String> getSongFile(String filename) async {
  final c = await collection;
  final songBox = await c.openBox('songs');
  final content = await songBox.get(filename);
  final String s = utf8.decode(content.codeUnits); 
  return s; 
}

Future<List<Setlist>> _getSetlists() async {
  final allSongs = Setlist("All Songs", "All songs in the database", []);
  List<Setlist> setlists = [];
  final c = await collection;
  final setlistBox = await c.openBox('setlists');
  for (var filename in await setlistBox.getAllKeys()) {
    final content = await setlistBox.get(filename) as String;
    // js.context.callMethod("alert", <String>["${filename}: $content"]);
    setlists.add(Setlist.fromMap(loadYaml(content)));
  }
  setlists.sort((a, b) {
    return a.title.compareTo(b.title);
  });
  List<Setlist> result = [allSongs];
  result.addAll(setlists);
  return result;
}

// _getSongHeader: returns null if the file is
// not a song file or contains fatal syntax errors
Future<sng.Meta?> _getSongHeader(String filename, String content) async {
  final ext = filename.split('.').last;
  // ignore all files except SessionCharts and ChordPro files
  if (songFilenameExtensions.contains(ext)) {
    final s = content;
    try {
      Result r = lp.header.parse(s);
      if (r.value is sng.Meta) {
        sng.Meta header = r.value;
        return header;
      } else {
        // print(r.value);
        return null;
      }
    } catch (e) {
      return null;
    }
  } else if (chordProFilenameExtensions.contains(ext)) {
    final s = content;
    try {
      Result r = cp.header.parse(s);
      if (r.value is sng.Meta) {
        sng.Meta header = r.value;
        return header;
      } else {
        // print(r.value);
        return null;
      }
    } catch (e) {
      return null;
    }
  } else {
    return null;
  }
}

// _getSongHeaders: if the setlist is null, get all the songs in the
// song directory, otherwise get only the songs in the setlist.
// If the key of a song is changed in a setlist, change the header
// to use the new key.
Future<List<sng.Meta>> _getSongHeaders([Setlist? setlist]) async {
  List<sng.Meta> headers = [];
  final c = await collection;
  final songBox = await c.openBox('songs');

  if (setlist == null) {
    // get all the songs
    final songBox = await c.openBox('songs');
    final map = await songBox.getAllValues();
    for (var song in map.entries) {
      var header = await _getSongHeader(song.key, song.value);
      if (header != null) {
        // store the filename of the SongFile in the header to allow
        // the song to be easily retrieved.
        header.file = song.key;
        headers.add(header);
      }
    }
  } else {
    // get only the files in the set list
    for (var song in setlist.songs) {
      var content = await songBox.get(song.filename);
      if (content != null) {
        var header = await _getSongHeader(song.filename, content);
        if (header != null) {
          // change the key to the key given in the setlist, if any
          if (song.key != null) header.key = song.key!;
          // store the id of the SongFile in the header to allow
          // the song to be easily retrieved.
          header.file = song.filename;
          headers.add(header);
        }
      } else
        print(
            "error; the setlist references a non-existent file with the name '${song.filename}'");
    }
  }
  return headers;
}

Future<void> makeSongIndex([Setlist? setlist]) async {
  songIndex = []; // reset

  // refresh the setlists
  setlists = await _getSetlists();
  songIndex = await _getSongHeaders(setlist);

  // Sort the songs by title if no setlist has been selected
  if (setlist == null)
    songIndex.sort((a, b) {
      final at = a.sortTitle != "" ? a.sortTitle : a.title;
      final bt = b.sortTitle != "" ? b.sortTitle : b.title;
      return at.compareTo(bt);
    });
}

// Reset the database by removing the songs and setslists and rebuilding the index.
// This function does not ask for confirmation before deleting.
// Confirmation is to be handled by the GUI.
Future<void> resetDatabase() async {
  songIndex = [];
  setlists = [];
  final c = await collection;
  final songBox = await c.openBox('songs');
  final setlistBox = await c.openBox('setlists');
  await songBox.clear();
  await setlistBox.clear();
}

bool isSongFile(String filename) {
  final ext = filename.split('.').last;
  return songFilenameExtensions.contains(ext);
}

bool isChordProFile(String filename) {
  final ext = filename.split('.').last;
  return chordProFilenameExtensions.contains(ext);
}

bool isSetlistFile(String filename) {
  final ext = filename.split('.').last;
  return setlistFilenameExtensions.contains(ext);
}

// storeFile: stores the file in the database and returns its key
Future<String?> storeFile(String filename, String content) async {
  final c = await collection;
  final songBox = await c.openBox('songs');
  final setlistBox = await c.openBox('setlists');
  if (isSetlistFile(filename)) {
    await setlistBox.put(filename, content);
    return filename;
  } else if (isSongFile(filename) || isChordProFile(filename)) {
    await songBox.put(filename, content);
    return filename;
  } else {
    return null; // error, no entry with this filename
  }
}
