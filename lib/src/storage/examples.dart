// Copyright (c) 2021-2022 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:archive/archive.dart';
import 'package:sessioncharts/src/storage/database.dart';
import 'package:flutter/services.dart' show rootBundle;

// load the example songs and setlists from the examples.zip asset
Future<void> loadExamples() async {
  final bytes = await rootBundle.load('files/examples.zip');

  // Decode the Zip file
  final archive = ZipDecoder().decodeBytes(
      bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));

  // copy each file to the songs directory
  for (final f in archive) {
    String s = String.fromCharCodes(f.content);
    storeFile(f.name, s);
  }
  await makeSongIndex();
}
