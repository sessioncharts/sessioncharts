// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';

String toneToString(Tone tone, MusicKey key, Style style,
    {bool lowerCase = false, bool prefixAccidental = true}) {
  var s = "";
  switch (style) {
    case Style.letters:
      if (tone is ScaleDegreeTone) {
        final pc = key.relativeToAbsolute(tone.sd);
        s = pitchClassToString(pc, key, prefixAccidental: prefixAccidental);
      } else if (tone is PitchTone) {
        s = pitchClassToString(tone.pc, key,
            prefixAccidental: prefixAccidental);
      } else {
        s = tone.toString();
      }
      return lowerCase ? s.toLowerCase() : s;
    case Style.nashville:
      if (tone is ScaleDegreeTone) {
        return scaleDegreeToNashville(tone.sd,
            prefixAccidental: prefixAccidental, flats: true);
      } else if (tone is PitchTone) {
        final sd = key.absoluteToRelative(tone.pc);
        return scaleDegreeToNashville(sd,
            prefixAccidental: prefixAccidental, flats: true);
      } else {
        return tone.toString();
      }
    case Style.roman:
      if (tone is PitchTone) {
        final sd = key.absoluteToRelative(tone.pc);
        return scaleDegreeToRoman(sd, flats: true);
      } else if (tone is ScaleDegreeTone) {
        return scaleDegreeToRoman(tone.sd, flats: true);
      } else {
        return tone.toString();
      }
    default:
      return tone.toString();
  }
}

String chordToString(Chord? chord, MusicKey key, Style style) {
  if (chord == null) return "";
  final tone = toneToString(chord.tone, key, style, prefixAccidental: false);
  final articulation = articulationToString(chord.articulation);
  final kind = chordKindToString(chord.kind);
  var bass = "";
  if (chord.bass != null) {
    bass = "/" + toneToString(chord.bass!, key, style, prefixAccidental: false);
  }
  // print("chord.tone=${chord.tone}; tone=${tone}");
  return articulation + tone + kind + bass;
}

String noteLengthToNumberedNotationString(NoteLength nl) {
  switch (nl) {
    case NoteLength.whole:
      return ' - - - ';
    case NoteLength.half:
      return ' - ';
    case NoteLength.dottedHalf:
      return ' - - ';
    case NoteLength.quarter:
      return '';
    case NoteLength.dottedQuarter:
      return '.';
    case NoteLength.eighth:
      return '_';
    case NoteLength.dottedEighth:
      return '_.';
    case NoteLength.sixteenth:
      return '=';
    case NoteLength.dottedSixteenth:
      return '=.';
    case NoteLength.implicit:
      return '';
    default:
      return '';
  }
}

String octaveToString(Octave o) {
  switch (o) {
    case Octave.downThree:
      return ";";
    case Octave.downTwo:
      return ":";
    case Octave.downOne:
      return ",";
    case Octave.middle:
      return "";
    case Octave.upOne:
      return "'";
    case Octave.upTwo:
      return '"';
    case Octave.upThree:
      return "^";
    default:
      return "";
  }
}

// display notes in melodies
String noteToString(Note note, MusicKey key, Style style) {
  // never display notes using roman numerals, only chords
  final style1 = style == Style.roman ? Style.nashville : style;
  return toneToString(note.tone, key, style1,
          lowerCase: true, prefixAccidental: true) +
      octaveToString(note.octave);
}

String eventToString(Event e, MusicKey key, Style style) {
  if (e is SingularEvent) {
    return singularEventToString(e, key, style);
  } else if (e is Beam) {
    return beamToString(e, key, style);
  } else if (e is Tuplet) {
    return tupletToString(e, key, style);
  } else {
    return e.toString();
  }
}

String singularEventToString(SingularEvent e, MusicKey key, Style style) {
  if (e is NoteEvent) {
    return noteEventToString(e, key, style);
  } else if (e is ChordEvent) {
    return chordEventToString(e, key, style);
  } else {
    return e.toString();
  }
}

String noteEventToString(NoteEvent e, MusicKey key, Style style) {
  return noteToString(e.note, key, style) +
      noteLengthToNumberedNotationString(e.duration);
}

String chordEventToString(ChordEvent ce, MusicKey key, Style style) {
  return chordToString(ce.chord, key, style) +
      noteLengthToNumberedNotationString(ce.duration);
}

String tupletToString(Tuplet t, MusicKey key, Style style) {
  final l = t.events.map((e) => eventToString(e, key, style));
  return "(" + l.join(' ') + ")";
}

String beamToString(Beam b, MusicKey key, Style style) {
  final l = b.events.map((e) => eventToString(e, key, style));
  return "[" + l.join(' ') + "]";
}
