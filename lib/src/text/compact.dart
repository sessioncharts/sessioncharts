// Copyright (c) 2019 Thomas F. Gordon
// SPDX-License-Identifier: Apache-2.0

import 'package:sessioncharts/src/song.dart';
import 'common.dart';

String joinContributors(
    String subtitle, String composer, String lyricist, String artist) {
  var l = [subtitle, composer, lyricist, artist];
  l.removeWhere((item) => item == "");
  return l.join(" / ");
}

String headerToString(Meta m) {
  final title = m.title;
  final subtitle = m.subtitle;
  final composer = m.composer;
  final lyricist = m.lyricist;
  final artist = m.artist;
  final key = m.key.toString();
  final rhythm = m.rhythm;
  final meter = m.meter.toString();
  final tempo = m.tempo.toString();
  final capo = m.capo == 0 ? "" : m.capo.toString();
  final contributors = joinContributors(subtitle, composer, lyricist, artist);

  return "### " +
      title +
      " - " +
      contributors +
      "\n" +
      key +
      (meter == "" ? "" : "; " + meter) +
      (rhythm == "" ? "" : " " + rhythm) +
      (tempo == "" ? "" : "; " + tempo) +
      (capo == "" ? "" : "; capo: " + capo) +
      "\n\n";
}

String flattenEvent(Event e, MusicKey key, Style style) {
  if (e is SingularEvent) {
    if (e is ChordEvent) {
      return chordEventToString(e, key, style);
    } else if (e is NoteEvent) {
      return noteEventToString(e, key, style);
    } else {
      return e.toString();
    }
  } else if (e is Tuplet) {
    return tupletToString(e, key, style);
  } else if (e is Beam) {
    return beamToString(e, key, style);
  } else {
    return e.toString();
  }
}

// flatten the events of a measure into a string.
String flattenEvents(List<Event> l, MusicKey key, Style style) {
  return l.map((e) => flattenEvent(e, key, style)).join(' ');
}

// Flattens a list of section elements, so that the resulting list
// contains just strings. Measures with more than one event (chord,
// note or rest) are group between parentheses. Normal bar lines
// are not displayed. New lines are replaced by slashes.
List<String> flattenMetric(List<MetricElement> l, MusicKey key, Style style) {
  List<String> result = [];
  for (var i = 0; i < l.length; i++) {
    final e = l[i];
    if (e is BarLine) {
      if (e.kind != BarLineSymbol.standard) {
        result.add(barLineSymbolToString(e.kind));
      }
    } else if (e is Measure) {
      if (e.events.length == 1) {
        result.add(flattenEvent(e.events[0], key, style));
      } else {
        final l = e.events.map((e2) => eventToString(e2, key, style));
        result.add("(" + l.join(" ") + ")");
      }
    } else if (e is NewLine) {
      if (i < l.length - 1) result.add("/");
    }
  }
  return result;
}

// In a list of strings, where each string represents a
// section element (measure, barline or slash),
// replaces subsequent duplicate measures with a % symbol.
List<String> handleDuplicateMeasures(List<String> l) {
  var lastMeasure = "";
  List<String> result = [];
  for (var s in l) {
    if (s == lastMeasure) {
      result.add("%");
    } else {
      // ignore slashes and bar symbols
      if (s != "/" && !{'|', ':', '['}.contains(s[0])) lastMeasure = s;
      result.add(s);
    }
  }
  return result;
}

// Convert the metric of chord chart or melody to a string
String metricToString(List<MetricElement>? l1, MusicKey key, Style style) {
  if (l1 == null || l1.isEmpty) {
    return "";
  }
  final l2 = flattenMetric(l1, key, style);
  final l3 = handleDuplicateMeasures(l2);
  return l3.join(" ");
}

// Replaces newlines in a lyric string with slashes.
// Returns a single line of text, with no line breaks
String slashifyLyric(List<LyricElement>? lyric,
    {bool showSlots = false, bool markdown = false}) {
  if (lyric == null || lyric.isEmpty) {
    return "";
  }
  var s = "";
  for (var i = 0; i < lyric.length; i++) {
    if (lyric[i] is Phrase) {
      s = s + (lyric[i] as Phrase).string;
    }
    if (lyric[i] is NewLine && i < lyric.length - 1) {
      s = s + " / ";
    }
    if (lyric[i] is Slot && showSlots) {
      // In markdown output underlines need to be escaped
      // to avoid the following text being shown in italics
      if (markdown) {
        s = s + "\\_";
      } else {
        s = s + "_";
      }
    }
  }
  return s;
}

String sectionCommentToString(String? comment) {
  if (comment == null) {
    return "";
  } else {
    return " (" + comment.trim() + ") \n";
  }
}

// There is no space after the section name to avoid
// Panddoc from creating enumerations from single letter
// section names.  A space is added only if there is a comment,
// chord block or lyric block in the section.
String sectionNameToString(String name) {
  if (name == "") {
    return "";
  } else {
    return name + ".";
  }
}

// Format a section in a string. Melodies and tabs are not displayed
// in this compact format, just chords and lyrics.
// ToDo: melody and tabs
String sectionToString(Section s, MusicKey key, Style style) {
  String spaceBeforeLyric() {
    if (s.lyric != null && s.lyric!.isNotEmpty) {
      return " ";
    } else {
      return "";
    }
  }

  if (s.chords == null || s.chords!.isEmpty) {
    return sectionNameToString(s.name) +
        sectionCommentToString(s.comment) +
        spaceBeforeLyric() +
        slashifyLyric(s.lyric, showSlots: false, markdown: true) +
        "\n" +
        "\n";
  } else {
    return sectionNameToString(s.name) +
        sectionCommentToString(s.comment) +
        " " +
        metricToString(s.chords, key, style) +
        "  \n" +
        spaceBeforeLyric() +
        slashifyLyric(s.lyric, showSlots: true, markdown: true) +
        "\n" +
        "\n";
  }
}

void writeCompactChart(Song s, Style style, StringBuffer buffer) {
  buffer.write(headerToString(s.header));
  MusicKey key = s.header.key;
  // If the capo is on the nth fret, transpose *down* n steps
  if (s.header.capo != 0) key = key.transpose(-s.header.capo);
  for (var section in s.arrangement) {
    buffer.write(sectionToString(section, key, style));
  }
}
