# Launching the Webapp in Debug Mode on ChromeOS

$ flutter run -d web-server

# Launching the Webapp in production mode

$ flutter build web
$ cd build/web
$ dhttpd

# Roadmap and Bugs

Now on my SessionCharts Trello board.

# iReal Pro text format

https://irealpro.com/ireal-pro-file-format/
