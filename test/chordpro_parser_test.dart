import 'package:test/test.dart';
import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/parser/chordpro_parser.dart';
import 'package:sessioncharts/src/parser/common.dart';
// import 'package:sessioncharts/src/song/chord_event.dart';
import 'package:sessioncharts/src/song/lyric_element.dart';

void main() {
  test('MetaDataDirectives', () {
    expect(metaDataDirectiveLine.accept("{title: Ain't No Sunshine} \n"), true);
    expect(metaDataDirectiveLine.accept("{composer: Bill Withers}\n"), true);
    expect(metaDataDirectiveLine.accept("{key: Am}\n"), true);
    expect(metaDataDirectiveLine.accept("{tim: 4/4}\n"), false);
    expect(metaDataDirectiveLine.accept("{time: 4/4}\n"), true);
    expect(metaDataDirectiveLine.accept("{tempo: 120}\n"), true);
    expect(metaDataDirectiveLine.accept("{unit note length: 1/4}\n"), false);
    expect(metaDataDirectiveLine.accept("# this is a comment\n"), false);
  });

  test('Header', () {
    expect(header.accept("""
    {title: A Fool For Your Stockings}
    {subtitle:ZZ Top}
    {key: Am}
    {tempo: 100}
    {time: 4/4 }
    """), true);
    expect(header.accept("""
    {title:Trotz Alledem}
    {subtitle:Ferdinand Freiligrath (1843)}
    {key:G}
    """), true);
  });

  test('Phrase', () {
    expect(phrase.accept("	rock me mama like a,"), true);
  });

  test('Chord', () {
    expect(chord.end().accept("V7"), true);
    expect(chord.end().accept("17"), true);
    expect(chord.end().accept("G7"), true);
    expect(chord.end().accept("IIm"), true);
    expect(chord.end().accept("2m"), true);
    expect(chord.end().accept("Dm"), true);
    expect(chord.end().accept("6m"), true);
  });

  test('Lyric Line', () {
    expect(
        lyricLine.accept(
            "	So []rock me mama like a []wagon wheel, []rock me mama anyway you []feel\n"),
        true);
    expect(
        lyricLine.accept("[E]Let me remember the [E]things I love.\n"), true);
    expect(
        lyricLine.accept("With flat car riders and cross-tie walkers\n"), true);
    expect(
        lyricLine
            .accept("And [I]one was a young maid so sweet and so [V7]fair.\n"),
        true);
    expect(
        lyricLine
            .accept("And [1]one was a young maid so sweet and so [57]fair.\n"),
        true);
    expect(
        lyricLine
            .parse(
                "	I was [A]standing [A]all alone against the [F#m]world outside [F#m] \n")
            .value,
        isA<List<LyricElement>>());
  });

  test('Lyric Block', () {
    expect(lyricBlock.parse(""" 
      Wonder _this time where she's _gone,
      _wonder if she's gone to _stay
      Ain't no _sunshine when she's _gone,
      and this _house just ain't no _home 
      Any_time she goes a_way.
    """).value, isA<List<LyricElement>>());
    expect(lyricBlock.accept(""" 
      A[C]mazing grace, how [F]sweet the [C]sound,
      That saved a wretch like [G]me. [G7]
      I [C]once was lost but [F]now am [C]found,
      Was blind, but [G]now I [F]see. [C]
   """), true);
  });

  test('Section 1', () {
    expect(section.accept(r"""

  A[C]mazing grace, how [F]sweet the [C]sound,
  That saved a wretch like [G]me. [G7]
  I [C]once was lost but [F]now am [C]found,
  Was blind, but [G]now I [F]see. [C]

  
  """), true);
  });

  test('Section 2', () {
    expect(section.accept(r"""{sov: verse1}
  A[C]mazing grace, how [F]sweet the [C]sound,
  That saved a wretch like [G]me. [G7]
  I [C]once was lost but [F]now am [C]found,
  Was blind, but [G]now I [F]see. [C]
{eov}

  """), true);
  });

  test('Section 3', () {
    expect(section.end().accept(r"""
    {soc}
  A[C]mazing grace, how [F]sweet the [C]sound,
  That saved a wretch like [G]me. [G7]
  I [C]once was lost but [F]now am [C]found,
  Was blind, but [G]now I [F]see. [C]
  {eoc}
  """), true);
  });

  test('Section 4', () {
    expect(section.accept(r"""
    {sob}
  A[C]mazing grace, how [F]sweet the [C]sound,
  That saved a wretch like [G]me. [G7]
  I [C]once was lost but [F]now am [C]found,
  Was blind, but [G]now I [F]see. [C]
  {eob}
  """), true);
  });

  test('Section 5', () {
    expect(section.accept(r"""

  [Am] [Am] [Am] [Am]
  
  
  """), true);
  });

  test('Section 6', () {
    expect(section.accept(r"""
    {chorus}
  """), true);
  });

  test('Section 7', () {
    expect(section.accept(r"""
    {chorus: Final}
  """), true);
  });

  test('Section 8', () {
    expect(section.accept(r"""
{soc}
But that's [Dm7]alright, [Am7] [Dm7] [Am7]
I said that [Am7]that's alright. [Dm7] [Am7] [Dm7]
[C]I may not want to admit it,
[Bm]I’m just a fool for your [E7]stockings I [Am7]believe. [Dm7] [Am7] [Dm7]
{eoc}"""), true);
  });

  test('Tab Section 1', () {
    expect(tabSection.accept(r"""
{sot}
 e|--------------------------------|
 B|--------------------------------|
 G|--------------------------------|
 D|----222-22-4-0-2--------0-------|
 A|-----------------444-44---2-4---|
 E|--------------------------------|
{eot}
"""), true);
  });

  test('Tab Section 2', () {
    expect(tabSection.accept(r"""{sot}  
   Bb    C  C                                     Bb    C  C
   Maj7                                           Maj7
E|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
B|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
G|-7-(7)-9--9-------------------------------------7-(7)-9--9----------|
D|-6-(6)10-10-------------------------------------6-(6)10-10----------|
A|-8-(8)10-10-------------------------------------8-(8)10-10----------|
E|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
 |                                                                    |
 |                                                                    |
 |            b      b               b                                |
E|----------13^[1]-13^[1]-11----11-11^[1/4]----------------11---11-11-|
B|-/11-13-13-----------------13-------------13\--/11-13-13---13-------|
G|--------------------------------------------------------------------|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

              C                    F9                     F9

E|-----8-8-8-8-8-8-8-8-8----8(8)8-----8---8------8(8)-8-----8---8-----|
B|-----8-8-8-8-8-8-8-8-8----8(8)8---x-8-x-8------8(8)-8---x-8-x-8-----|
G|-----9-9-9-9-9-9-9-9-9----8(8)8-x-x-8-x-8------8(8)-8-x-x-8-x-8-----|
D|----101010101010101010----7(7)7-x-x-7-x-7------7(7)-7-x-x-7-x-7-----|
A|----101010101010101010----8(8)8-x---8---8------8(8)-8-x---8---8-----|
E|-----8-8-8-8-8-8-8-8-8----------------------------------------------|
 |                                                                    |
 |                                                                    |
 |                          b   r    b             b   r    b      b  |
E|---------11----------11-13^[1]v(13)^[1]-11-----13^[1]v(13)^[1]-11-----|[1]
B|13-11-13----11-13~-13----------------------13~------------------------|
G|----------------------------------------------------------------------|
D|----------------------------------------------------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

                 C9                C9                   G9

E|----------------------------------------------/10---10-10---10---10-|
B|------/8--8-8---x-8---8---/8--8-8---x-8---8---/10---10-10--x10-x-10-|
G|------/7--7-7-x-x-7-x-7---/7--7-7-x-x-7-x-7---/10-10---10-xx10-x-10-|
D|------/8-8--8-x-x-8-x-8---/8-8--8-x-x-8-x-8---/-9--9----9-xx-9-x--9-|
A|------/7-7--7-x---7-x-7---/7-7--7-x---7-x-7---/10-10---10-x-10---10-|
E|--------------------------------------------------------------------|
 |                                                                    |
 |                                                                    |
 |  b                                 b                               |
E|11^[1/4]-8~~----------11--------------------------------------------|
B|--------------------11---13~~---------------------------------------|
G|--------------/12-12---------12\10-8^[1/4]--------------------8-10p8|
D|------------------------------------------10--------------8-10------|
A|--------------------------------------------10~-10-8-10-10----------|
E|--------------------------------------------------------------------|

# this is a comment

          F9                  C9                 C9

E|-/8---8-8-----8---8-------------------------------------------------|
B|-/8---8-8---x-8-x-8---/8--8-8---x-8---8--/8--8-8---x-8---8----------|
G|-/8-8---8-x-x-8-x-8---/7--7-7-x-x-7-x-7--/7--7-7-x-x-7-x-7----------|
D|-/7-7---7-x-x-7-x-7---/8-8--8-x-x-8-x-8--/8-8--8-x-x-8-x-8----------|
A|-/8-8---8-x---8---8---/7-7--7-x---7-x-7--/7-7--7-x---7-x-7----------|
E|--------------------------------------------------------------------|
 |                                                                    |
[Verse 1]
E|--------------8-----------------------------------------|-----------|
B|----8-11p8-11---11/13~~---8h11p8----8-------------------|-----------|
G|-10------------------------------10---10-10p8----8------|-----------|
D|----------------------------------------------10--------|-----------|
A|---------------------------------------------------10~~-|-----------|
E|--------------------------------------------------------|-----------|
{eot}
"""), true);
  });

  test('Grid Line 1', () {
    expect(gridLine.end().accept(r"""
| F#m . C#m7 . | % . . .  | % . . .  | %% . . .  | . . . . |
"""), true);
  });
  test('Grid Line 2', () {
    expect(gridLine.end().accept(r"""
margin note | F#m . C#m7 . | % . . .  | % . . .  | % . . .  | % . . . | margin
"""), true);
  });

  test('Grid Block', () {
    expect(gridBlock.end().accept(r"""
| F#m . C#m7 . | % . . .  | % . . .  | % . . .  | % . . . |
| F#m . C#m7 . | % . . .  | % . . .  | % . . .  | % . . . |
"""), true);
  });

  test('Grid Section', () {
    expect(gridSection.end().accept(r"""
{start_of_grid}
| F#m . C#m7 . | % . . .  | % . . .  | % . . .  | % . . . |
| F#m . C#m7 . | % . . .  | % . . .  | % . . .  | % . . . |
{end_of_grid}
"""), true);
  });

  test('Song 1', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}
"""), true);
  });

  test('Song 2', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}

Is it [Am7]you I've got out[Dm7]side, 
just [Am7]banging on the front [Dm7]door?
You [Am7]said you had e[Dm7]nough, 
now you're [Am7]coming back for [Dm7]more.
"""), true);
  });

  test('Song 3', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}


[Am] [Am] [Am] [Am]

Is it [Am7]you I've got out[Dm7]side, 
just [Am7]banging on the front [Dm7]door?
You [Am7]said you had e[Dm7]nough, 
now you're [Am7]coming back for [Dm7]more.
"""), true);
  });

  test('Song 4', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}

{soc}
But that's [Dm7]alright, [Am7] [Dm7] [Am7]
I said that [Am7]that's alright. [Dm7] [Am7] [Dm7]
[C]I may not want to admit it,
[Bm]I’m just a fool for your [E7]stockings I [Am7]believe. [Dm7] [Am7] [Dm7]
{eoc}

"""), true);
  });

  test('Song 5', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}

{c: Solo}
"""), true);
  });

  test('Song 6', () {
    expect(song.end().accept(r"""
{title: A Fool For Your Stockings}
{subtitle:ZZ Top}
{key: Am}
{time: 4/4}

[Am] [Am] [Am] [Am]

Is it [Am7]you I've got out[Dm7]side, 
just [Am7]banging on the front [Dm7]door?
You [Am7]said you had e[Dm7]nough, 
now you're [Am7]coming back for [Dm7]more.

{soc}
But that's [Dm7]alright, [Am7] [Dm7] [Am7]
I said that [Am7]that's alright. [Dm7] [Am7] [Dm7]
[C]I may not want to admit it,
[Bm]I’m just a fool for your [E7]stockings I [Am7]believe. [Dm7] [Am7] [Dm7]
{eoc}

Now I don't mind when you send money,
And bring your girlfriends with you.
But how could one be so thoughtless, 
to try and handle less than two.

{chorus}

{c: Solo}

Now I'm telling everybody, 
it seems too good to be true.
Sweet things can always get sweeter, 
I know mine did how 'bout you.

{chorus}
"""), true);
  });
}
