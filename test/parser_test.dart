import 'package:sessioncharts/src/song.dart';
import 'package:test/test.dart';
import 'package:petitparser/petitparser.dart';
import 'package:sessioncharts/src/parser/sessioncharts_parser.dart';
import 'package:sessioncharts/src/parser/common.dart';

void main() {
  test('Chord PitchClass', () {
    expect(chordPitchClass.accept('A'), true);
    expect(chordPitchClass.parse('A').value, 'A');
    expect(chordPitchClass.accept('B'), true);
    expect(chordPitchClass.accept('C'), true);
    expect(chordPitchClass.accept('D'), true);
    expect(chordPitchClass.accept('E'), true);
    expect(chordPitchClass.accept('F'), true);
    expect(chordPitchClass.accept('G'), true);
    expect(chordPitchClass.accept('a'), false);
    expect(chordPitchClass.accept('1'), false);
  });

  test('Note PitchClass', () {
    expect(notePitchClass.accept('a'), true);
    expect(notePitchClass.accept('b'), true);
    expect(notePitchClass.accept('c'), true);
    expect(notePitchClass.accept('d'), true);
    expect(notePitchClass.accept('e'), true);
    expect(notePitchClass.accept('f'), true);
    expect(notePitchClass.accept('g'), true);
    expect(notePitchClass.accept('A'), false);
    expect(notePitchClass.accept('1'), false);
  });

  test('Bar Symbols and Bar Lines', () {
    expect(barSymbol.parse("|").value, BarLineSymbol.standard);
    expect(barSymbol.parse("||").value, BarLineSymbol.double);
    expect(barSymbol.parse("|:").value, BarLineSymbol.beginRepeat);
    expect(barSymbol.parse(":|").value, BarLineSymbol.endRepeat);
    expect(barSymbol.parse(":|:").value, BarLineSymbol.endBeginRepeat);
    expect(barSymbol.parse("|]").value, BarLineSymbol.end);
    expect(barLine.parse("[1,2,3").value, isA<BarLine>());
    expect(barLine.parse("[1-3,5").value, isA<BarLine>());
  });

  test('Event', () {
    expect(chordEvent.parse("A").value, isA<ChordEvent>());
    expect(chordEvent.parse("!Am/G_.").value, isA<ChordEvent>());
    expect(chordEvent.parse(">C---").value, isA<ChordEvent>());
    expect(noteEvent.parse("c").value, isA<NoteEvent>());
    expect(noteEvent.parse("1=").value, isA<NoteEvent>());
  });

  test('Chord Line', () {
    expect(chordLine.accept("| A | B | C\n"), true);
    expect(chordLine.accept("|: Am :| Am | Em G | Am |]\n"), true);
    expect(chordLine.accept("|: VIm :| VIm | IIIm V | VIm |]\n"), true);
  });

  test('Code Comment', () {
    expect(codeComment.accept('# This is a comment  \t\n'), true);
    expect(codeComment.accept(" # This is a comment\n"),
        true); // whitespace is allowed before the # symbol
    expect(
        codeComment.accept("# This is a comment"), false); // missing new line
  });

  test('Header', () {
    expect(fieldLine.accept("title: Ännchen von Tharau    \n"), true);
    expect(fieldLine.accept("title: Ain't No Sunshine    \n"), true);
    expect(fieldLine.accept("sorttitle: Foggy Dew, The\n"), true);
    expect(fieldLine.accept("composer: Bill Withers   \n"), true);
    expect(fieldLine.accept("key: Am\n"), true);
    expect(fieldLine.accept("metr: 4/4\n"), false);
    expect(fieldLine.accept("meter: 4/4\n"), true);
    expect(fieldLine.accept("tempo: 120\n"), true);
    expect(fieldLine.accept("unit note length: 1/4\n"), true);
    expect(fieldLine.accept("capo: 5\n"), true);
    expect(fieldLine.accept("album: The White Album\n"), true);
    expect(fieldLine.accept("duration: 3:20\n"), true); // 3 minutes, 20 secs
    expect(fieldLine.accept("unit note length: 1/4\n"), true);
    expect(fieldLine.accept("""# this is a comment
     unit note length: 1/4
     """), true);
  });

  test('Phrase', () {
    expect(phrase.accept("	rock me mama like a,"), true);
  });

  test('Lyric Line', () {
    expect(
        lyricLine.accept(
            "	So []rock me mama like a []wagon wheel, []rock me mama anyway you []feel\n"),
        true);
    expect(
        lyricLine.accept("[E]Let me remember the [E]things I love.\n"), true);
    expect(
        lyricLine.accept("With flat car riders and cross-tie walkers\n"), true);
    expect(
        lyricLine
            .parse(
                "	I was [A]standing [A]all alone against the [F#m]world outside [F#m] \n")
            .value,
        isA<List<LyricElement>>());
  });

  test('Lyric Block', () {
    expect(lyricBlock.parse("""lyric {  
      Wonder _this time where she's _gone,
      _wonder if she's gone to _stay
      Ain't no _sunshine when she's _gone,
      and this _house just ain't no _home 
      Any_time she goes a_way.
    }
    """).value, isA<List<LyricElement>>());
  });

  test('Lyric Block', () {
    expect(lyricBlock.accept("""lyric {  
      A[C]mazing grace, how [F]sweet the [C]sound,
      That saved a wretch like [G]me. [G7]
      I [C]once was lost but [F]now am [C]found,
      Was blind, but [G]now I [F]see. [C]
      ĿŀĲĳŠšŽžŒœŸŐőŰű«»ÀÁ¿ÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛ
      ÝÞßàáâãäåæçéêëìíîïðñòóôõö÷øùúûüþÿ±£¢§©¬®µ¶
   }
   """), true);
  });

  test('Lyric Block Latin 1', () {
    expect(lyricBlock.accept("""lyric {  
      ÄäÜüÖößĿŀĲĳŠšŽžŒœŸŐőŰű«»ÀÁ¿ÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛ
      ÝÞßàáâãäåæçéêëìíîïðñòóôõö÷øùúûüþÿ±£¢§©¬®µ¶
   }
   """), true);
  });

  test('Chord Block', () {
    expect(chordBlock.accept("""chords {  
       C - - - | F | NC | C |
       C | F | G | G |
       # this is a code comment
       C | F | (Em G) | Em |	
       F | C G-. | C | C
       G C | C
   }
   """), true);
  });

  test('Octave', () {
    expect(octave.end().accept("'"), true);
    expect(octave.end().accept('"'), true);
    expect(octave.end().accept(","), true);
    expect(octave.end().accept(":"), true);
    expect(octave.end().accept(";"), true);
  });

  test('Note Events', () {
    expect(noteEvent.accept("5"), true);
    expect(noteEvent.accept("c"), true);
    expect(noteEvent.accept("z"), true);
    expect(noteEvent.accept("0"),
        true); // alternative rest representation, from the numbered musical notation
    expect(noteEvent.accept("x"), true); // rhythm beat
    // expect(noteEvent.end().accept("5#"), true);
    expect(noteEvent.end().accept("#5"), true);
    expect(noteEvent.end().accept("5,"), true);
    expect(noteEvent.end().accept("5:"), true);
    expect(noteEvent.end().accept("5'"), true);
    expect(noteEvent.end().accept('5"'), true);
    expect(noteEvent.end().accept("#5'"), true);
    expect(noteEvent.end().accept('#5"'), true);
    expect(noteEvent.end().accept("#5,"), true);
    expect(noteEvent.end().accept("#5:"), true);
    expect(noteEvent.end().accept("#5,"), true);
    expect(noteEvent.end().accept("#5:"), true);
    expect(noteEvent.end().accept('#5"'), true);
    expect(noteEvent.end().accept("3_"), true);
    expect(noteEvent.end().accept("6,_"), true);
    expect(noteEvent.end().accept("2-."), true);
    expect(noteEvent.end().accept("1---"), true);
    expect(noteEvent.end().accept("1--"), true);
    expect(noteEvent.end().accept("1-"), true);
    expect(noteEvent.end().accept("1 - - -"), true);
    expect(noteEvent.end().accept("1 - -"), true);
    expect(noteEvent.end().accept("1 -"), true);
  });

  test('Tied Notes', () {
    expect(tiedNote.end().accept("~5"), true);
    expect(tiedNote.end().accept("c"), false);
  });

  test('BarLine 1', () {
    expect(barLine.end().accept(r"|"), true);
    expect(barLine.end().accept(r":|"), true);
    expect(barLine.end().accept(r"[1-3"), true);
  });

  test('Measure 1', () {
    expect(measure.end().accept(r"c_e_g_"), true);
    expect(measure.end().accept(r"a-. (a_g_c_) e_ g_g. c_e_g"), true);
    expect(measure.end().accept(r"a-. a_g_c_ d--"), true);
  });

  test('Element 1', () {
    expect(element.end().accept(r"|"), true);
    expect(element.end().accept(r"c_e_g_"), true);
    expect(element.end().accept(r" "), true);
    expect(element.end().accept("\t "), true);
  });
  test('Melody Line 1', () {
    expect(
        melodyLine
            .end()
            .accept("| c_e_g_ :| a-. a_g_c_ e_ g_g. c_e_g| a-. a_g_c_ d--\n"),
        true);
    expect(
        melodyLine.end().accept(
            "1_3_5_  | 6-. 6_5_1_ 3_ 5_5-  1_3_5    | 6-. 6_5_1_  2--\n"),
        true);
  });

  test('Melody Line 2', () {
    expect(
        melodyLine.end().accept(
            """ (1_3_5_)  | 6-. 6_5_1_ 3_5_5-  1_3_5    | 6-. 6_5_1_  2--  \n"""),
        true);
    expect(
        melodyLine.end().accept(
            """ 5,_6,_1 | 2-. 3_2_5, 6,_1_1- 5,_6,_1 | 2-. 3_2_6,_ 1-- |]  \n"""),
        true);
  });

  test('Variant Endings', () {
    expect(melodyLine.end().accept("""[1  1_3_5_ :|[ 2-. 3_2_6,_ 1-- |] \n"""),
        true);
    expect(
        melodyLine.end().accept(""" [1,3 1_3_5_ :|[2 2-. 3_2_6,_ 1-- |]  \n"""),
        true);
  });

  test('Melody Block 1', () {
    expect(melodyBlock.accept(r"""melody {  
        1_3_5_  | 6-. 6_5_1_ 3_(5_5-)  1_3_5    | 6-. 6_5_1_  2-- 
        5,_6,_1 | 2-. 3_2_5, 6,_(1_1-) 5,_6,_1 | 2-. 3_2_6,_ 1-- |]
   }
   """), true);
  });

  test('Melody Block 2', () {
    expect(melodyBlock.accept(r"""melody {  
        1_ 3_ ~5 |: 6-. 6_5_1_ 3_ 5_ 5- 1_3_5    | 6-. 6_5_1_ 2-- 5,_6,_1 | 
     |  2-. 3_2_5, 6,_ 1_ 1- 5,_6,_1 | 2-. 3_3_6,_ 1-- :|
   }
   """), true);
  });

  test('Tab Block', () {
    expect(tabBlock.accept(r"""tab {  
   Bb    C  C                                     Bb    C  C
   Maj7                                           Maj7
E|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
B|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
G|-7-(7)-9--9-------------------------------------7-(7)-9--9----------|
D|-6-(6)10-10-------------------------------------6-(6)10-10----------|
A|-8-(8)10-10-------------------------------------8-(8)10-10----------|
E|-6-(6)-8--8-------------------------------------6-(6)-8--8----------|
 |                                                                    |
 |                                                                    |
 |            b      b               b                                |
E|----------13^[1]-13^[1]-11----11-11^[1/4]----------------11---11-11-|
B|-/11-13-13-----------------13-------------13\--/11-13-13---13-------|
G|--------------------------------------------------------------------|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

              C                    F9                     F9

E|-----8-8-8-8-8-8-8-8-8----8(8)8-----8---8------8(8)-8-----8---8-----|
B|-----8-8-8-8-8-8-8-8-8----8(8)8---x-8-x-8------8(8)-8---x-8-x-8-----|
G|-----9-9-9-9-9-9-9-9-9----8(8)8-x-x-8-x-8------8(8)-8-x-x-8-x-8-----|
D|----101010101010101010----7(7)7-x-x-7-x-7------7(7)-7-x-x-7-x-7-----|
A|----101010101010101010----8(8)8-x---8---8------8(8)-8-x---8---8-----|
E|-----8-8-8-8-8-8-8-8-8----------------------------------------------|
 |                                                                    |
 |                                                                    |
 |                          b   r    b             b   r    b      b  |
E|---------11----------11-13^[1]v(13)^[1]-11-----13^[1]v(13)^[1]-11-----|[1]
B|13-11-13----11-13~-13----------------------13~------------------------|
G|----------------------------------------------------------------------|
D|----------------------------------------------------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

                 C9                C9                   G9

E|----------------------------------------------/10---10-10---10---10-|
B|------/8--8-8---x-8---8---/8--8-8---x-8---8---/10---10-10--x10-x-10-|
G|------/7--7-7-x-x-7-x-7---/7--7-7-x-x-7-x-7---/10-10---10-xx10-x-10-|
D|------/8-8--8-x-x-8-x-8---/8-8--8-x-x-8-x-8---/-9--9----9-xx-9-x--9-|
A|------/7-7--7-x---7-x-7---/7-7--7-x---7-x-7---/10-10---10-x-10---10-|
E|--------------------------------------------------------------------|
 |                                                                    |
 |                                                                    |
 |  b                                 b                               |
E|11^[1/4]-8~~----------11--------------------------------------------|
B|--------------------11---13~~---------------------------------------|
G|--------------/12-12---------12\10-8^[1/4]--------------------8-10p8|
D|------------------------------------------10--------------8-10------|
A|--------------------------------------------10~-10-8-10-10----------|
E|--------------------------------------------------------------------|

# this is a comment

          F9                  C9                 C9

E|-/8---8-8-----8---8-------------------------------------------------|
B|-/8---8-8---x-8-x-8---/8--8-8---x-8---8--/8--8-8---x-8---8----------|
G|-/8-8---8-x-x-8-x-8---/7--7-7-x-x-7-x-7--/7--7-7-x-x-7-x-7----------|
D|-/7-7---7-x-x-7-x-7---/8-8--8-x-x-8-x-8--/8-8--8-x-x-8-x-8----------|
A|-/8-8---8-x---8---8---/7-7--7-x---7-x-7--/7-7--7-x---7-x-7----------|
E|--------------------------------------------------------------------|
 |                                                                    |
[Verse 1]
E|--------------8-----------------------------------------|-----------|
B|----8-11p8-11---11/13~~---8h11p8----8-------------------|-----------|
G|-10------------------------------10---10-10p8----8------|-----------|
D|----------------------------------------------10--------|-----------|
A|---------------------------------------------------10~~-|-----------|
E|--------------------------------------------------------|-----------|
    }
"""), true);
  });

  test('Values', () {
    expect(punctuation.accept(';'), true);
    expect(barSymbol.parse('|').value, BarLineSymbol.standard);
    expect(barSymbol.parse('|]').value, BarLineSymbol.end);
  });

  test('Song 1', () {
    expect(song.end().parse("""
title: Ännchen von Turnau 
key: C major
meter: 4/4

section: V

lyric {
	[C]Abilene [E]Abil[E7]ene, 
	[F]prettiest town, that [C]I've ever seen.
	[D7]Folks down there, don't [G]treat you mean, 
	in [C]Abilene, my [F]Abi[C]lene. [G]
}

""").value, isA<Song>());
  });
}
